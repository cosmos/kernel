//
// Created by antifallobst on 8/17/22.
//

#include <cosmos.h>
#include <cosmos/IPC.h>

void _start() {

    print("<=====[ Test Program 1 ]=====>\n");

    ipc_connection_T ipcc = ipc_connect(0, IPC_CONN_BIDIRECTIONAL);

    ipc_message_T msg = ipc_receive(&ipcc);
    print("[1] Received Message: ");
    print((string_t)msg.buffer);
    print("\n");

    print("[1] Sending Message: <1>Test[1]\n");
    ipc_send_string(&ipcc, "<1>Test[1]");

    msg = ipc_receive(&ipcc);
    print("[1] Received Message: ");
    print((string_t)msg.buffer);
    print("\n");

    exit(0);
}