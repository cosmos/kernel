//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_MATH_H
#define COSMOS_MATH_H

#include <stdint.h>

typedef struct {
    uint64_t x;
    uint64_t y;
} point_T;

uint64_t    octal_to_binary     (uint8_t *str, int size);
int64_t     ceil_to             (uint64_t n, int64_t factor);
int64_t     floor_to            (uint64_t n, int64_t factor);
int64_t     min                 (int64_t val1, int64_t val2);
int64_t     max                 (int64_t val1, int64_t val2);
double      pow                 (int64_t base, int64_t exponent);
int64_t     abs                 (int64_t n);

#endif //COSMOS_MATH_H
