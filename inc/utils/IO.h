//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_IO_H
#define COSMOS_IO_H

#include <stdint.h>

void        io_out_x8       (uint16_t port, uint8_t value);
void        io_out_x16      (uint16_t port, uint16_t value);
void        io_out_x32      (uint16_t port, uint32_t value);

uint8_t     io_in_x8        (uint16_t port);
uint16_t    io_in_x16       (uint16_t port);
uint32_t    io_in_x32       (uint16_t port);

void        io_wait         ();

#endif //COSMOS_IO_H
