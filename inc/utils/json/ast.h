//
// Created by antifallobst on 9/8/22.
//

#ifndef COSMOS_AST_H
#define COSMOS_AST_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <utils/json/tokenizer.h>

typedef enum {
    JSON_AST_NODE_SCOPE,
    JSON_AST_NODE_ARRAY,
    JSON_AST_NODE_STRING,
    JSON_AST_NODE_INTEGER,
    JSON_AST_NODE_BOOL,
    JSON_AST_NODE_NULL
} json_ast_node_type_E;

typedef struct {
    json_ast_node_type_E        type;
    size_t                      length;
    char*                       string;
} json_ast_data_T;

typedef struct {
    struct json_ast_node_T*     parent;
    struct json_ast_node_T*     childs;
    struct json_ast_node_T*     next;
    struct json_ast_node_T*     prev;
    json_ast_data_T             data;
} json_ast_node_T;

json_ast_node_T     json_ast_init                       (json_scope_T* scope);

json_ast_node_T*    json_ast_node_add_child             (json_ast_node_T* node, json_ast_data_T data);
json_ast_node_T*    json_ast_node_get_child_at_index    (json_ast_node_T* node, int index);
uint64_t            json_ast_node_num_childs            (json_ast_node_T* node);
char*               json_ast_node_get_assigned_string   (json_ast_node_T* node);

void                json_ast_clear                      (json_ast_node_T* node);

#endif //COSMOS_AST_H
