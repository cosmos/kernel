//
// Created by antifallobst on 9/8/22.
//

#ifndef COSMOS_TOKENIZER_H
#define COSMOS_TOKENIZER_H

#include <stddef.h>

typedef enum {
    JSON_TOKEN_NULL,
    JSON_TOKEN_INTEGER,
    JSON_TOKEN_STRING,
    JSON_TOKEN_BRACKET_CURLY_OPEN,
    JSON_TOKEN_BRACKET_CURLY_CLOSE,
    JSON_TOKEN_BRACKET_SQUARE_OPEN,
    JSON_TOKEN_BRACKET_SQUARE_CLOSE,
    JSON_TOKEN_COLON,
    JSON_TOKEN_COMMA,
    JSON_TOKEN_BOOL
} json_token_types_E;

typedef struct {
    json_token_types_E  type;
    size_t              length;
    char*               string;
} json_token_T;

typedef struct {
    size_t          size;
    json_token_T*   tokens;
} json_scope_T;


json_scope_T    json_tokenize_json   (char* json_string, size_t json_string_size);

#endif //COSMOS_TOKENIZER_H
