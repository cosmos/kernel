//
// Created by antifallobst on 9/8/22.
//

#ifndef COSMOS_PARSER_H
#define COSMOS_PARSER_H

#include <utils/json/tokenizer.h>
#include <utils/json/ast.h>

json_ast_node_T     json_parse_json_string   (char* json_string, int json_string_size);

#endif //COSMOS_PARSER_H
