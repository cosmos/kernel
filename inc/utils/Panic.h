//
// Created by antifallobst on 15/08/2022.
//

#ifndef COSMOS_PANIC_H
#define COSMOS_PANIC_H

#include <drivers/cpu.h>

void panic(const char* message, cpu_state_T* state);

#endif //COSMOS_PANIC_H
