//
// Created by antifallobst on 9/19/22.
//

#ifndef COSMOS_SYMBOL_H
#define COSMOS_SYMBOL_H

#include <stdint.h>

typedef struct {
    uint64_t    address;
    char*       name;
} symbol_T;

typedef struct {
    uint64_t    num_funcs;
    uint64_t    num_vars;
    symbol_T*   funcs;
    symbol_T*   vars;
} symbols_T;

extern symbols_T g_symbols;

symbol_T* symbol_lookup(symbols_T* symbols, const char* name);

#endif //COSMOS_SYMBOL_H
