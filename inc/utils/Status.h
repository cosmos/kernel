//
// Created by antifallobst on 10/6/22.
//

#ifndef COSMOS_STATUS_H
#define COSMOS_STATUS_H

#include <stdint.h>

typedef int8_t status_t;

#define STATUS_RESOURCE_BUSY            -6
#define STATUS_TIMEOUT                  -5
#define STATUS_RESOURCE_NOT_AVAILABLE   -4
#define STATUS_PERMISSION_DENIED        -3
#define STATUS_ERROR                    -2
#define STATUS_WARNING                  -1
#define STATUS_SUCCESS                   0

#endif //COSMOS_STATUS_H
