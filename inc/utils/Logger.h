//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_LOGGER_H
#define COSMOS_LOGGER_H

#include <stdarg.h>

#define LOGGER_QEMU_PORT        0x3F8

#define LOGGING_PREFIX_SUCCESS  "[ SUCCESS ] "
#define LOGGING_PREFIX_INFO     "[  INFO   ] "
#define LOGGING_PREFIX_DEBUG    "[  DEBUG  ] "
#define LOGGING_PREFIX_WARNING  "[ WARNING ] "
#define LOGGING_PREFIX_ERROR    "[  ERROR  ] "

#define LOGGING_PRINT_SUCCESS   true
#define LOGGING_PRINT_INFO      false
#define LOGGING_PRINT_DEBUG     true
#define LOGGING_PRINT_WARNING   true
#define LOGGING_PRINT_ERROR     true
#define LOGGING_PRINT_NONE      false

typedef enum {
    LOGGING_SUCCESS,
    LOGGING_INFO,
    LOGGING_DEBUG,
    LOGGING_WARNING,
    LOGGING_ERROR,
    LOGGING_NONE,
    LOGGING_NONE_PRINT
} LoggingTypes_E;

void log(LoggingTypes_E type, const char* str, ...);

#endif //COSMOS_LOGGER_H
