//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_STRUTIL_H
#define COSMOS_STRUTIL_H

#include <stdbool.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdint.h>

bool        strcmp              (const char* a, const char* b);
size_t      strlen              (const char* str);

size_t      format              (char* buffer, const char* pattern, ...);
size_t      vformat             (char* buffer, const char* pattern, va_list args);

char*       dec_to_string       (int64_t value, char* buffer);
char*       bin_to_string       (uint64_t value, char* buffer);
char*       bool_to_string      (bool value, char* buffer);
char*       hex_to_string_x8    (uint8_t value, char* buffer);
char*       hex_to_string_x16   (uint16_t value, char* buffer);
char*       hex_to_string_x32   (uint32_t value, char* buffer);
char*       hex_to_string_x64   (uint64_t value, char* buffer);
char*       double_to_string    (double value, uint8_t precision, char* buffer);

uint64_t    string_to_hex_x64   (const char* string);

#endif //COSMOS_STRUTIL_H
