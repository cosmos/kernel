//
// Created by antifallobst on 10/28/22.
//

#ifndef COSMOS_MODULEMANAGER_H
#define COSMOS_MODULEMANAGER_H

#include <module/Module.h>
#include <utils/json/ast.h>

#define NUM_NEEDED_SYMS_LOGGER      1
#define NUM_NEEDED_SYMS_RENDERER    0
#define NUM_NEEDED_SYMS_KEYBOARD    0
#define NUM_NEEDED_SYMS_PCI         0
#define NUM_NEEDED_SYMS_AHCI        0
#define NUM_NEEDED_SYMS_VFAT        0
#define NUM_NEEDED_SYMS_EXT2        0
#define NUM_NEEDED_SYMS_POWER       0
#define NUM_NEEDED_SYMS_USB         0


typedef struct {
    module_T*   modules;
    uint64_t    num_modules;
} module_manager_T;

void        mod_manager_init        (module_manager_T* mod_manager);
void        mod_manager_load_mods   (module_manager_T* mod_manager, json_ast_node_T* json_master);
void        mod_manager_call_mod    (module_manager_T* mod_manager, module_type_E mod, uint16_t call, thread_T* caller, int argc, uint64_t* argv);
uint64_t    mod_manager_get_mod_pid (module_manager_T* mod_manager, module_type_E mod);

extern module_manager_T* g_mod_manager;

#endif //COSMOS_MODULEMANAGER_H
