//
// Created by antifallobst on 12/11/22.
//

#ifndef COSMOS_MODCALLS_H
#define COSMOS_MODCALLS_H

typedef enum {
    MOD_CALL_LOGGER_LOG
} mod_logger_call_E;

typedef enum {
    MOD_CALL_RENDERER_PRINT,
    MOD_CALL_RENDERER_SET_PIXEL,
    MOD_CALL_RENDERER_GET_PIXEL,
    MOD_CALL_RENDERER_SET_BUFFER,
    MOD_CALL_RENDERER_GET_BUFFER,
    MOD_CALL_RENDERER_LOAD_FONT
} mod_renderer_call_E;

typedef enum {
    MOD_CALL_KEYBOARD_CHAR_OUT,
    MOD_CALL_KEYBOARD_CHAR_IN
} mod_keyboard_call_E;

typedef enum {
    MOD_CALL_POWER_SHUTDOWN,
    MOD_CALL_POWER_REBOOT
} mod_power_call_E;

#endif //COSMOS_MODCALLS_H
