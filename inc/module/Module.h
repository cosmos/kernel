//
// Created by antifallobst on 10/28/22.
//

#ifndef COSMOS_MODULE_H
#define COSMOS_MODULE_H

#include <process/Process.h>

#define MODULE_INIT_FUNC "_init"

typedef enum {
    MOD_INVALID,
    MOD_LOGGER,
    MOD_RENDERER,
    MOD_KEYBOARD,
    MOD_PCI,
    MOD_AHCI,
    MOD_VFAT,
    MOD_EXT2,
    MOD_POWER,
    MOD_USB,

    MOD_END_OF_ENUM
} module_type_E;

typedef struct {
    module_type_E   type;
    uint32_t        pid;
} module_T;

const char*     mod_type_to_string  (module_type_E type);
module_type_E   mod_string_to_type  (const char* type_string);

#endif //COSMOS_MODULE_H
