//
// Created by antifallobst on 15/08/2022.
//

#ifndef COSMOS_IDT_H
#define COSMOS_IDT_H

#include <stdint.h>

#define IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE   0b10001110
#define IDT_TYPE_ATTRIBUTE_CALL_GATE        0b10001100
#define IDT_TYPE_ATTRIBUTE_TRAP_GATE        0b10001111


typedef struct {
    uint16_t    limit;
    uint64_t    offset;
} __attribute__((packed)) IDTRegister_T;

typedef struct {
    uint16_t    offset0;
    uint16_t    selector;
    uint8_t     ist;
    uint8_t     type_attribute;
    uint16_t    offset1;
    uint32_t    offset2;
    uint32_t    ignore;
} IDTDescriptorEntry_T;

void        set_idt_descriptor_entry_offset     (IDTDescriptorEntry_T* entry, uint64_t offset);
uint64_t    get_idt_descriptor_entry_offset     (IDTDescriptorEntry_T* entry);

#endif //COSMOS_IDT_H
