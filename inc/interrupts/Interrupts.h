//
// Created by antifallobst on 15/08/2022.
//

#ifndef COSMOS_INTERRUPTS_H
#define COSMOS_INTERRUPTS_H

#include <stdint.h>

void    set_idt_gate        (void* handler, uint8_t entry_offset, uint8_t type_attribute, uint8_t selector);
void    init_idt_register   ();
void    load_idt_register   ();

void    disable_interrupts  ();
void    enable_interrupts   ();



extern void interrupt_stub_0x00();
extern void interrupt_stub_0x01();
extern void interrupt_stub_0x02();
extern void interrupt_stub_0x03();
extern void interrupt_stub_0x04();
extern void interrupt_stub_0x05();
extern void interrupt_stub_0x06();
extern void interrupt_stub_0x07();
extern void interrupt_stub_0x08();
extern void interrupt_stub_0x09();
extern void interrupt_stub_0x0A();
extern void interrupt_stub_0x0B();
extern void interrupt_stub_0x0C();
extern void interrupt_stub_0x0D();
extern void interrupt_stub_0x0E();
extern void interrupt_stub_0x0F();
extern void interrupt_stub_0x10();
extern void interrupt_stub_0x11();
extern void interrupt_stub_0x12();
extern void interrupt_stub_0x13();
extern void interrupt_stub_0x14();
extern void interrupt_stub_0x15();
extern void interrupt_stub_0x16();
extern void interrupt_stub_0x17();
extern void interrupt_stub_0x18();
extern void interrupt_stub_0x19();
extern void interrupt_stub_0x1A();
extern void interrupt_stub_0x1B();
extern void interrupt_stub_0x1C();
extern void interrupt_stub_0x1D();
extern void interrupt_stub_0x1E();
extern void interrupt_stub_0x1F();

extern void interrupt_stub_0x20();
extern void interrupt_stub_0x80();
extern void interrupt_stub_0x81();

#endif //COSMOS_INTERRUPTS_H
