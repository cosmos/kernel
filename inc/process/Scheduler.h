//
// Created by antifallobst on 8/18/22.
//

#ifndef COSMOS_SCHEDULER_H
#define COSMOS_SCHEDULER_H

#include <stdbool.h>
#include <process/Process.h>
#include <process/Thread.h>
#include <memory/Bitmap.h>
#include <drivers/cpu.h>

#define MAX_THREADS   1024
#define MAX_PROCESSES   64

typedef struct {
    process_T**     processes;
    Bitmap_T        processes_bitmap;

    thread_T**      threads;
    Bitmap_T        threads_bitmap;
    Bitmap_T        running_threads_bitmap;

    thread_T**      queue;
    uint64_t        queue_index;
    uint64_t        queue_length;

    uint64_t        current_tid;
    bool            allow_switching;
    bool            enabled;
}scheduler_T;

void            init_scheduler          (scheduler_T* scheduler);
void            enable_scheduler        (scheduler_T* scheduler);

int             register_thread         (scheduler_T* scheduler, thread_T* thread);
void            pause_thread            (scheduler_T* scheduler, int tid);
void            start_thread            (scheduler_T* scheduler, int tid);
void            kill_thread             (scheduler_T* scheduler, int tid);

int             register_process        (scheduler_T* scheduler, process_T* process);
void            start_process           (scheduler_T* scheduler, int pid);
void            kill_process            (scheduler_T* scheduler, int pid);

bool            verify_tid              (scheduler_T* scheduler, int tid);
bool            verify_pid              (scheduler_T* scheduler, int pid);
void            calculate_queue         (scheduler_T* scheduler);
cpu_state_T*    switch_thread           (scheduler_T* scheduler, cpu_state_T* state);

extern scheduler_T* g_scheduler;

#endif //COSMOS_SCHEDULER_H
