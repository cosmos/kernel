//
// Created by antifallobst on 12/13/22.
//

#ifndef COSMOS_THREAD_H
#define COSMOS_THREAD_H

#include <stdint.h>
#include <stdbool.h>
#include <drivers/cpu.h>
#include <utils/Symbol.h>

extern char* thread_type_strings[3];

typedef enum {
    THREAD_PRIORITY_HIGH,
    THREAD_PRIORITY_LOW
} thread_priority_E;

typedef enum {
    THREAD_MASTER,
    THREAD_NORMAL,
    THREAD_MODULE_CALL
} thread_type_E;

typedef struct {
    uint32_t            pid;
    uint32_t            g_tid;    // Global Thread ID (Scheduler Intern)
    uint32_t            l_tid;    // Local Thread ID (Process Intern)

    thread_priority_E   priority;
    thread_type_E       type;
    uint64_t            caller_tid;
    uint64_t*           ret_val_ptr;

    cpu_state_T         cpu_state;
    double              cpu_time;
    uint64_t            stack;

} thread_T;

void    thread_end_reached  ();
bool    init_thread         (thread_T* thread, uint32_t pid, symbol_T* start_symbol, thread_type_E type, uint32_t argc, uint64_t* argv);
void    log_threads         ();

#endif //COSMOS_THREAD_H
