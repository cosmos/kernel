//
// Created by antifallobst on 8/17/22.
//

#ifndef COSMOS_PROCESS_H
#define COSMOS_PROCESS_H

#include <stdint.h>
#include <stddef.h>
#include <memory/paging/Paging.h>
#include <memory/Bitmap.h>
#include <process/elf/ELF.h>
#include <process/elf/MemoryImage.h>
#include <process/IPC.h>
#include <process/ProcessFileManager.h>
#include <process/Thread.h>
#include <drivers/cpu.h>
#include <drivers/File.h>

#define STACK_SIZE               5
#define MAX_THREADS_PER_PROC    64

extern char* process_type_strings      [3];
extern char* process_priorities_strings[2];


typedef enum {
    PROCESS_MODULE,
    PROCESS_APPLICATION
} process_type_E;

typedef struct {
    process_filemgr_T   file_manager;
    ipc_manager_T       ipc_manager;
} process_descriptor_manager_T;

typedef struct {
    process_type_E                  type;
    uint64_t                        pid;
    uint64_t                        base_address;
    uint64_t                        reserved;       // used to set caller process for mod_calls
    double                          cpu_time;
    char                            name                [129];
    elf_executable_T                executable;
    paging_page_table_T*            page_map;
    process_descriptor_manager_T    descriptors;

    thread_T**                      threads;
    Bitmap_T                        threads_bitmap;
    thread_T*                       master_thread;
} process_T;

int         load_process_from_file          (const char* path, process_type_E type, const char* name);
int         load_process                    (void* exec_buffer, uint64_t exec_buffer_len, process_type_E type, const char* name);
int         load_process_from_executable    (elf_executable_T executable, process_type_E type, paging_page_table_T* page_map, const char* name);

uint32_t    process_new_thread              (process_T* process, thread_type_E type, symbol_T* function, int argc, uint64_t* argv);
uint32_t    process_block_thread_lid        (process_T* process);
void        process_free_thread_lid         (process_T* process, uint32_t lid);
bool        process_check_thread_lid        (process_T* process, uint32_t lid);
thread_T*   process_get_thread              (process_T* process, uint32_t lid);

#endif //COSMOS_PROCESS_H
