//
// Created by antifallobst on 10/4/22.
//

#ifndef COSMOS_IPC_H
#define COSMOS_IPC_H

#include <utils/Status.h>
#include <stdint.h>
#include <memory/Bitmap.h>

#define IPC_CHUNK_SIZE 0x20
#define IPC_CACHE_SIZE 0x20
#define IPC_STACK_SIZE 0x1000

typedef enum {
    IPC_MSG_STATUS,
    IPC_MSG_SIGNAL,
    IPC_MSG_MESSAGE,
    IPC_MSG_MODCOM
} ipc_message_type_E;

typedef enum {
    IPC_CONN_RECV,
    IPC_CONN_SEND,
    IPC_CONN_BIDIRECTIONAL
} ipc_connection_mode_E;

typedef enum {
    IPC_CONN_NOT_WAITING,
    IPC_CONN_CONNECTION_AVAILABLE,
    IPC_CONN_WAITING
} ipc_connection_wait_status_E;

typedef enum {
    IPC_STD_DESC_NONE,
    IPC_STD_DESC_BROADCAST,

    IPC_STD_DESC_END
} ipc_standard_descriptors_E;

typedef struct {
    int                 sender_tid;
    ipc_message_type_E  type;
    uint16_t            special;        // used for module calls to set a type of modcall
    uint8_t*            buffer;
    uint64_t            buffer_len;
} ipc_message_T;

typedef struct {
    uint64_t            num_messages;
    uint64_t            top;
    uint64_t            bottom;
    ipc_message_T*      top_message;
    ipc_message_T*      bottom_message;
} ipc_message_stack_T;

typedef struct {
    uint64_t                        descriptor;
    uint64_t                        server_pid;
    ipc_connection_mode_E           mode;
    ipc_message_stack_T             stack;
    struct ipc_connection_T*        link;
} ipc_connection_T;

typedef struct {
    uint64_t                        size;
    ipc_connection_T*               connections;
    Bitmap_T                        connections_bmp;
    uint64_t                        free_slots;
    struct ipc_manager_chunk_T*     next;
} ipc_manager_chunk_T;

typedef struct {
    uint64_t                        size;
    uint64_t                        position;
    uint64_t*                       descriptors;
    ipc_connection_T**              connections;
} ipc_manager_cache_T;

typedef struct {
    ipc_manager_cache_T             cache;
    ipc_manager_chunk_T*            chunk;
    uint64_t                        num_files;
    ipc_connection_wait_status_E    waiting_for_connection;
    ipc_connection_mode_E           waiting_for_mode;
} ipc_manager_T;

void                ipc_link_connections            (ipc_connection_T* a, ipc_connection_T* b);
void                ipc_manager_init                (ipc_manager_T* ipc_manager);
uint64_t            ipc_manager_add_connection      (ipc_manager_T* ipc_manager, uint64_t server_pid, ipc_connection_mode_E mode);
ipc_connection_T*   ipc_manager_get_connection      (ipc_manager_T* ipc_manager, uint64_t connection_descriptor);
bool                ipc_manager_remove_connection   (ipc_manager_T* ipc_manager, uint64_t connection_descriptor);

status_t            ipc_handle_send                 (uint64_t connection_descriptor, ipc_message_T* msg);
status_t            ipc_handle_receive              (uint64_t connection_descriptor, ipc_message_T* msg);

status_t            ipc_push_message                (ipc_message_stack_T* stack, ipc_message_T* msg);
ipc_message_T*      ipc_pop_message                 (ipc_message_stack_T* stack);

#endif //COSMOS_IPC_H
