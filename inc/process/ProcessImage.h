//
// Created by antifallobst on 9/8/22.
//
// !!! ATTENTION !!!
// DO NOT USE THIS FILE - IT WILL BE DELETED IN A FEW COMMITS
//

#ifndef COSMOS_PROCESSIMAGE_H
#define COSMOS_PROCESSIMAGE_H

#include <stdint.h>
#include <stddef.h>

typedef struct {
    uint64_t        virtual_offset;
    uint64_t        length;
    void*           specific; // Specific to the format that the executable is in (e.g. ELF)
} process_mapping_T;

typedef struct {
    size_t              num_mappings;
    process_mapping_T*  mappings;
    uint64_t            entry_point;
} process_image_T;

#endif //COSMOS_PROCESSIMAGE_H
