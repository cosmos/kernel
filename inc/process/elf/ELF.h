//
// Created by antifallobst on 8/16/22.
//

#ifndef COSMOS_ELF_H
#define COSMOS_ELF_H

#include <stdint.h>
#include <stdbool.h>
#include <process/elf/MemoryImage.h>
#include <process/elf/Header.h>
#include <process/elf/Section.h>
#include <process/elf/Program.h>
#include <process/elf/Symbol.h>
#include <process/elf/Library.h>
#include <memory/paging/PageMap.h>

#define ELF_START_SYMBOL        "_start"

typedef struct {
    elf_header_x64_T            header;
    elf_section_entry_x64_T     symbol_table;
    elf_section_entry_x64_T     got_plt;
    elf_section_entry_x64_T*    dynamic;
    elf_section_entry_x64_T     string_table;
    elf_section_entry_x64_T     dynamic_string_table;
    elf_section_entry_x64_T     dynamic_symbol_table;
    elf_section_entry_x64_T     section_header_string_table;
    elf_section_entry_x64_T*    section_table;
    elf_program_entry_x64_T*    program_table;
    elf_memory_image_T          image;
    uint64_t                    num_libraries;
    shared_library_T**          libraries;

    size_t                      buffer_size;
    uint8_t*                    buffer;
    uint64_t                    load_address;
    uint64_t                    mappings_end;
} elf_executable_T;

elf_executable_T    elf_load_executable             (void* buffer, size_t buffer_size, void* load_address, paging_page_table_T* page_map);
bool                elf_validate_executable         (elf_executable_T* executable);
void                elf_parse_symbol_table          (elf_executable_T* executable);
uint64_t            elf_lookup_section_index        (elf_executable_T* executable, const char* name);
elf_symbol_T        elf_lookup_symbol_in_table      (elf_executable_T* executable, elf_section_entry_x64_T symbol_table, char* name);
void                elf_resolve_libraries           (elf_executable_T* executable);
void                elf_set_got                     (elf_executable_T* executable);

#endif //COSMOS_ELF_H
