//
// Created by antifallobst on 8/16/22.
//

#ifndef COSMOS_HEADER_H
#define COSMOS_HEADER_H

#include <stdint.h>
#include <stddef.h>

typedef enum {
    ELF_BITS_32             = 1,
    ELF_BITS_64             = 2,
    ELF_BITS_UNKNOWN        = 0xff
} elf_bit_count_E;

typedef enum {
    ELF_ENDIAN_LITTLE       = 1,
    ELF_ENDIAN_BIG          = 2,
    ELF_ENDIAN_UNKNOWN      = 0xff
} elf_endianness_E;

typedef enum {
    SYS_ABI_SYSTEM_V        = 0x00,
    SYS_ABI_HP_UX           = 0x01,
    SYS_ABI_NETBSD          = 0x02,
    SYS_ABI_LINUX           = 0x03,
    SYS_ABI_GNU_HURD        = 0x04,
    SYS_ABI_SOLARIS         = 0x06,
    SYS_ABI_AIX             = 0x07,
    SYS_ABI_IRIX            = 0x08,
    SYS_ABI_FREEBSD         = 0x09,
    SYS_ABI_TRU64           = 0x0a,
    SYS_ABI_NOVELL_MODESTO  = 0x0b,
    SYS_ABI_OPENBSD         = 0x0c,
    SYS_ABI_OPENVMS         = 0x0d,
    SYS_ABI_NONSTOP_KERNEL  = 0x0e,
    SYS_ABI_AROS            = 0x0f,
    SYS_ABI_FENIX_OS        = 0x10,
    SYS_ABI_NUXI_CLOUD_ABI  = 0x11,
    SYS_ABI_STRATUS_OPENVOS = 0x12,

    SYS_ABI_COSMOS          = 0xc0,

    SYS_ABI_UNKNOWN         = 0xff
} elf_system_ABI_E;

typedef enum {
    ELF_OBJECT_NONE                 = 0x00,
    ELF_OBJECT_RELOCATABLE          = 0x01,
    ELF_OBJECT_EXECUTABLE           = 0x02,
    ELF_OBJECT_DYNAMIC_LIB          = 0x03,
    ELF_OBJECT_CORE                 = 0x04
} elf_object_type_E;

typedef enum {
    ISA_UNDEFINED               = 0x0000,
    ISA_WE_32100                = 0x0001,
    ISA_SPARC                   = 0x0002,
    ISA_X86_32                  = 0x0003,
    ISA_MOTOROLA_68000          = 0x0004,
    ISA_MOTOROLA_88000          = 0x0005,
    ISA_INTEL_MCU               = 0x0006,
    ISA_INTEL_80860             = 0x0007,
    ISA_MIPS                    = 0x0008,
    ISA_IBM_SYSTEM_370          = 0x0009,
    ISA_MIPS_RS3000_LE          = 0x000A,
    ISA_HP_PA_RISC              = 0x000E,
    ISA_INTEL_80960             = 0x0013,
    ISA_POWERPC                 = 0x0014,
    ISA_POWERPC_64              = 0x0015,
    ISA_S390                    = 0x0016,
    ISA_IBM_SPU_SPC             = 0x0017,
    ISA_NEC_V800                = 0x0024,
    ISA_FUJITSU_FR20            = 0x0025,
    ISA_TRW_RH32                = 0x0026,
    ISA_MOTOROLA_RCE            = 0x0027,
    ISA_ARM32                   = 0x0028,
    ISA_DIGITAL_ALPHA           = 0x0029,
    ISA_SUPER_H                 = 0x002A,
    ISA_SPARC_V9                = 0x002B,
    ISA_SIEMENS_TRICORE         = 0x002C,
    ISA_ARGONAUT_RISC           = 0x002D,
    ISA_HITACHI_H8_300          = 0x002E,
    ISA_HITACHI_H8_300H         = 0x002F,
    ISA_HITACHI_H8S             = 0x0030,
    ISA_HITACHI_H8_500          = 0x0031,
    ISA_IA_64                   = 0x0032,
    ISA_STANFORD_MIPS_X         = 0x0033,
    ISA_MOTOROLA_COLDFIRE       = 0x0034,
    ISA_MOTOROLA_68HC12         = 0x0035,
    ISA_FUJITSU_MMA_ACCEL       = 0x0036,
    ISA_SIEMENS_PCP             = 0x0037,
    ISA_SONY_NCPU_RISC          = 0x0038,
    ISA_DENSO_NDR1              = 0x0039,
    ISA_MOTOROLA_STARCORE       = 0x003A,
    ISA_TOYOTA_ME16             = 0x003B,
    ISA_ST100                   = 0x003C,
    ISA_TINY_J                  = 0x003D,
    ISA_X86_64                  = 0x003E,
    ISA_SONY_DSP                = 0x003F,
    ISA_PDP_10                  = 0x0040,
    ISA_PDP_11                  = 0x0041,
    ISA_SIEMENS_FX66            = 0x0042,
    ISA_STM_ST9_PLUS            = 0x0043,
    ISA_STM_ST7                 = 0x0044,
    ISA_MOTOROLA_MC68HC16       = 0x0045,
    ISA_MOTOROLA_MC68HC11       = 0x0046,
    ISA_MOTOROLA_MC68HC08       = 0x0047,
    ISA_MOTOROLA_MC68HC05       = 0x0048,
    ISA_SGI_SVX                 = 0x0049,
    ISA_STM_ST19                = 0x004A,
    ISA_DIGITAL_VAX             = 0x004B,
    ISA_AXIS_EMBEDDED_32        = 0x004C,
    ISA_INFINEON_EMBEDDED_32    = 0x004D,
    ISA_ELEMENT_14_DSP          = 0x004E,
    ISA_LSI_LOGIC_16_DSP        = 0x004F,
    ISA_TMS_S320_C6000          = 0x008C,
    ISA_MCST_ELBRUS_E2K         = 0x00AF,
    ISA_ARM64                   = 0x00B7,
    ISA_ZILOG_Z80               = 0x00DC,
    ISA_RISC_V                  = 0x00F3,
    ISA_BERKELEY_PACKET_FILTER  = 0x00F7,
    ISA_WDC65C816               = 0x0101,

    ISA_UNKNOWN                 = 0xFFFF
} cpu_instruction_set_E;


typedef struct {
    uint8_t         identity[16];
    uint16_t	    elf_type;
    uint16_t	    instruction_set;
    uint32_t	    version;
    uint64_t	    address_entry_point;
    uint64_t	    offset_program_header;
    uint64_t	    offset_section_header;
    uint32_t	    flags;
    uint16_t	    len_header;
    uint16_t	    len_program_header_entry;
    uint16_t	    num_program_header_entries;
    uint16_t	    len_section_header_entry;
    uint16_t	    num_section_header_entries;
    uint16_t	    string_section_index;
} elf_header_x64_T;

extern char*    elf_header_str_num_bits[3];
extern char*    elf_header_str_endianness[3];
extern char*    elf_header_str_system_ABI[18];
extern char*    elf_header_str_object_type[5];
extern char*    elf_header_str_instruction_set[79];

#endif //COSMOS_HEADER_H
