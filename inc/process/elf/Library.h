//
// Created by antifallobst on 10/6/22.
//

#ifndef COSMOS_LIBRARY_H
#define COSMOS_LIBRARY_H

#include <stdint.h>

typedef struct {
    char*       name;
    uint64_t    start;
    uint64_t    end;
    uint64_t    executable;
} shared_library_T;

#endif //COSMOS_LIBRARY_H
