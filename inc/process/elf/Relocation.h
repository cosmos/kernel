//
// Created by antifallobst on 9/21/22.
//

#ifndef COSMOS_RELOCATION_H
#define COSMOS_RELOCATION_H

#include <stdint.h>
#include <process/elf/ELF.h>


#define ELF_RELOCATION_SYMBOL(i)            ((i) >> 32)
#define ELF_RELOCATION_TYPE(i)              ((i) & 0xffffffffL)
#define ELF_RELOCATION_INFO(s,t)            (((s) << 32) + ((t) & 0xffffffffL))

#define ELF_RELOCATION_X86_64_PC32(s,a,p)   (s + a - p)
#define ELF_RELOCATION_X86_64_PLT32(l,a,p)  (l + a - p)

extern char* elf_relocation_types[27];

typedef enum {
    ELF_REL_NONE,
    ELF_REL_64,
    ELF_REL_PC32,
    ELF_REL_GOT32,
    ELF_REL_PLT32,
    ELF_REL_COPY,
    ELF_REL_GLOB_DAT,
    ELF_REL_JUMP_SLOT,
    ELF_REL_RELATIVE
} elf_relocation_type_E;

typedef struct {
    uint64_t    offset;
    uint64_t    info;
} elf_relocation_T;

typedef struct {
    uint64_t    offset;
    uint64_t    info;
    int64_t     addend;
} elf_relocation_addend_T;

void elf_relocate(elf_executable_T* executable, uint64_t load_address);

#endif //COSMOS_RELOCATION_H
