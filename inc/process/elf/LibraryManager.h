//
// Created by antifallobst on 10/5/22.
//

#ifndef COSMOS_LIBRARYMANAGER_H
#define COSMOS_LIBRARYMANAGER_H

#include <process/elf/ELF.h>
#include <process/elf/Library.h>

#define MAX_LIBRARIES 16

typedef struct {
    shared_library_T*   libraries;
    uint64_t            num_libraries;
    uint64_t            base_address;
} shared_library_manager_T;

void                library_manager_init                    (shared_library_manager_T* lib_manager);
void                library_manager_load_library            (shared_library_manager_T* lib_manager, void* buffer, uint64_t buffer_len, const char* name);
void                library_manager_load_library_from_file  (shared_library_manager_T* lib_manager, const char* path, const char* name);
shared_library_T*   library_manager_get_lib_by_name         (shared_library_manager_T* lib_manager, const char* name);

extern shared_library_manager_T* g_lib_manager;

#endif //COSMOS_LIBRARYMANAGER_H
