//
// Created by antifallobst on 10/6/22.
//

#ifndef COSMOS_DYNAMIC_H
#define COSMOS_DYNAMIC_H

#include <stdint.h>

typedef enum {
    ELF_DYN_NULL,
    ELF_DYN_NEEDED
} elf_dynamic_tag_E;

typedef struct {
    elf_dynamic_tag_E   tag;
    uint64_t            value;
} elf_dynamic_T;

void dynamic_linker_resolve_symbol();

#endif //COSMOS_DYNAMIC_H
