//
// Created by antifallobst on 10/22/22.
//

#ifndef COSMOS_PROCESSFILEMANAGER_H
#define COSMOS_PROCESSFILEMANAGER_H

#include <stdint.h>
#include <drivers/File.h>
#include <memory/Bitmap.h>

#define PROCESS_FILEMGR_CHUNK_SIZE 32
#define PROCESS_FILEMGR_CACHE_SIZE 32

typedef struct {
    uint64_t                        size;
    file_T*                         files;
    Bitmap_T                        files_bmp;
    uint64_t                        free_slots;
    struct process_filemgr_chunk_T* next;
} process_filemgr_chunk_T;

typedef struct {
    uint64_t                        size;
    uint64_t                        position;
    uint64_t*                       descriptors;
    file_T**                        files;
} process_filemgr_cache_T;

typedef struct {
    process_filemgr_cache_T         cache;
    process_filemgr_chunk_T*        chunk;
    uint64_t                        num_files;
} process_filemgr_T;

void        process_filemgr_init           (process_filemgr_T* pfmgr);
uint64_t    process_filemgr_add_file       (process_filemgr_T* pfmgr, const char* path, FileModes_E mode);
file_T*     process_filemgr_get_file       (process_filemgr_T* pfmgr, uint64_t file_descriptor);
bool        process_filemgr_remove_file    (process_filemgr_T* pfmgr, uint64_t file_descriptor);

#endif //COSMOS_PROCESSFILEMANAGER_H
