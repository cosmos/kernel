//
// Created by antifallobst on 10/22/22.
//

#ifndef COSMOS_PERMISSION_H
#define COSMOS_PERMISSION_H

#include <stdbool.h>
#include <process/Process.h>
#include <drivers/File.h>

bool permission_path_access (thread_T* thread, const char* path, FileModes_E mode);
bool permission_ipc_connect (thread_T* server, thread_T* client, ipc_connection_mode_E mode);

#endif //COSMOS_PERMISSION_H
