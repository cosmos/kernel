//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_GDT_H
#define COSMOS_GDT_H

#include <stdint.h>

typedef struct {
    uint16_t    size;
    uint64_t    offset;
}__attribute__((packed)) GDTDescriptor_T;

typedef struct {
    uint16_t    limit0;
    uint16_t    base0;
    uint8_t     base1;
    uint8_t     access_byte;
    uint8_t     limit1_flags;
    uint8_t     base2;
}__attribute__((packed)) GDTEntry_T;

typedef struct {
    GDTEntry_T  Null;           // 0x00
    GDTEntry_T  KernelCode;     // 0x08
    GDTEntry_T  KernelData;     // 0x10
    GDTEntry_T  UserNull;       // 0x18
    GDTEntry_T  UserCode;       // 0x20
    GDTEntry_T  UserData;       // 0x28
}__attribute__((packed)) __attribute((aligned(0x1000))) GDT_T;

extern GDT_T default_GDT;

extern void load_GDT(GDTDescriptor_T* gdtDescriptor);

#endif //COSMOS_GDT_H
