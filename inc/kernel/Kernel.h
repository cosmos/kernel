//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_KERNEL_H
#define COSMOS_KERNEL_H

#include <stdbool.h>
#include <graphics/Framebuffer.h>
#include <graphics/Font.h>
#include <memory/EFIMemory.h>
#include <drivers/ACPI.h>

typedef struct {
    framebuffer_T*              framebuffer;
    psf1_font_T*                psf1_font;
    EFI_MEMORY_DESCRIPTOR_T*    memory_map;
    unsigned long long          memory_map_size;
    unsigned long long          memory_map_descriptor_size;
    acpi_rsdp_2_T*              rsdp;
    void*                       ramdisk;
} boot_info_T;

extern uint64_t _kernel_start;
extern uint64_t _kernel_end;

void    initialize_kernel   (boot_info_T* boot_info);
void    initialize_modules  ();
bool    kernel_initialized  ();

#endif //COSMOS_KERNEL_H
