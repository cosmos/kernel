//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_COLOR_H
#define COSMOS_COLOR_H

#include <stdint.h>

typedef uint32_t color_t;


// Alpha Red Green Blue
//                          A R G B
enum ColorStandard_E {
    COLOR_STD_RED       = 0xffff0000,
    COLOR_STD_YELLOW    = 0xffffff00,
    COLOR_STD_CYAN      = 0xff00ffff,
    COLOR_STD_MAGENTA   = 0xffff00ff,
    COLOR_STD_BLACK     = 0xff000000,
    COLOR_STD_WHITE     = 0xffffffff
};

// Colors from https://gogh-co.github.io/Gogh/
enum ColorPalette_E {
    COLOR_PAL_GREY_DARK     = 0xff363636,
    COLOR_PAL_PINK          = 0xffff0883,
    COLOR_PAL_GREEN_SIGNAL  = 0xff83ff08,
    COLOR_PAL_ORANGE        = 0xffff8308,
    COLOR_PAL_BLUE          = 0xff0883ff,
    COLOR_PAL_PURPLE        = 0xff8308ff,
    COLOR_PAL_GREEN         = 0xff08ff83,
    COLOR_PAL_GREY_LIGHT    = 0xffb6b6b6
};

#endif //COSMOS_COLOR_H
