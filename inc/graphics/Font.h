//
// Created by antifallobst on 24/05/2022.
//

#ifndef COSMOS_FONT_H
#define COSMOS_FONT_H

typedef struct {
    unsigned char   magic[2];
    unsigned char   mode;
    unsigned char   char_size;
} psf1_header_T;

typedef struct {
    psf1_header_T*  psf1_header;
    void*           glyph_buffer;
} psf1_font_T;

extern psf1_font_T* g_font;

#endif //COSMOS_FONT_H
