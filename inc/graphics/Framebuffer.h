//
// Created by antifallobst on 24/05/2022.
//

#ifndef COSMOS_FRAMEBUFFER_H
#define COSMOS_FRAMEBUFFER_H

typedef struct {
    void*               base_address;
    unsigned long long  buffer_size;
    unsigned int        width;
    unsigned int        height;
    unsigned int        pixels_per_scan_line;
} framebuffer_T;

extern framebuffer_T* g_framebuffer;

#endif //COSMOS_FRAMEBUFFER_H
