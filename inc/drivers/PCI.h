//
// Created by antifallobst on 8/26/22.
//

#ifndef COSMOS_PCI_H
#define COSMOS_PCI_H

#include <stdint.h>
#include <drivers/ACPI.h>

typedef enum {
    PCI_C_NONE,
    PCI_C_MASS_STORAGE_CTRLR,
    PCI_C_NETWORK_CTRLR,
    PCI_C_DISPLAY_CTRLR,
    PCI_C_MULTIMEDIA_CTRLR,
    PCI_C_MEMORY_CTRLR,
    PCI_C_BRIDGE,
    PCI_C_SIMPLE_COM_CTRLR,
    PCI_C_BASE_SYSTEM_PERIPHERAL,
    PCI_C_INPUT_DEVICE_CTRLR,
    PCI_C_DOCKING_STATION,
    PCI_C_PROCESSOR,
    PCI_C_SERIAL_BUS_CTRLR,
    PCI_C_WIRELESS_CTRLR,
    PCI_C_INTELLIGENT_CTRLR,
    PCI_C_SATELLITE_COM_CTRLR,
    PCI_C_ENCRYPTION_CTRLR,
    PCI_C_SIGNAL_PROCESSING_CTRLR,
    PCI_C_PROCESSING_ACCELLERATOR,
    PCI_C_NON_ESSENTIAL,
    PCI_C_CO_PROCESSOR              = 0x40,
    PCI_C_VENDOR_SPECIFIC           = 0xFF
} pci_class_types_E;    // All PCI Class codes


typedef enum {
    PCI_C_NONE_SC_VGA,
    PCI_C_NONE_SC_NO_VGA
} pci_class_none_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_NONE

typedef enum {
    PCI_C_MASS_SC_SCSI_BUS,
    PCI_C_MASS_SC_IDE,
    PCI_C_MASS_SC_FLOPPY,
    PCI_C_MASS_SC_IPI_BUS,
    PCI_C_MASS_SC_RAID,
    PCI_C_MASS_SC_ATA,
    PCI_C_MASS_SC_SATA,
    PCI_C_MASS_SC_SA_SCSI,
    PCI_C_MASS_SC_NON_VOLATILE_MEM,
    PCI_C_MASS_SC_OTHER                 = 0x80
} pci_class_mass_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_MASS_STORAGE_CTRLR

typedef enum {
    PCI_C_NET_SC_ETHERNET,
    PCI_C_NET_SC_TOKEN_RING,
    PCI_C_NET_SC_FDDI,
    PCI_C_NET_SC_WORLD_FLIP,
    PCI_C_NET_SC_PICMG_2_14_MULTI_COMP,
    PCI_C_NET_SC_INFINIBAND,
    PCI_C_NET_SC_FABRIC,
    PCI_C_NET_SC_OTHER                  = 0x80
} pci_class_net_subclass_types_E;   // All known PCI subclass codes for Class PCI_C_NETWORK_CTRLR

typedef enum {
    PCI_C_DISP_SC_VGA,
    PCI_C_DISP_SC_XGA,
    PCI_C_DISP_SC_3D,
    PCI_C_DISP_SC_OTHER                 = 0x80
} pci_class_disp_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_DISPLAY_CTRLR

typedef enum {
    PCI_C_MULT_SC_VIDEO,
    PCI_C_MULT_SC_AUDIO,
    PCI_C_MULT_SC_TEL,
    PCI_C_MULT_SC_AUDIO_DEV,
    PCI_C_MULT_SC_OTHER                 = 0x80
} pci_class_mult_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_MULTIMEDIA_CTRLR

typedef enum {
    PCI_C_MEM_SC_RAM,
    PCI_C_MEM_SC_FLASH,
    PCI_C_MEM_SC_OTHER                  = 0x80
} pci_class_mem_subclass_types_E;   // All known PCI subclass codes for Class PCI_C_MEMORY_CTRLR

typedef enum {
    PCI_C_BRDG_SC_HOST,
    PCI_C_BRDG_SC_ISA,
    PCI_C_BRDG_SC_EISA,
    PCI_C_BRDG_SC_MCA,
    PCI_C_BRDG_SC_PCI_PCI_V0,
    PCI_C_BRDG_SC_PCMCIA,
    PCI_C_BRDG_SC_NU_BUS,
    PCI_C_BRDG_SC_CARD_BUS,
    PCI_C_BRDG_SC_RACEWAY,
    PCI_C_BRDG_SC_PCI_PCI_V1,
    PCI_C_BRDG_SC_INFINIBAND_PCI_HOST,
    PCI_C_BRDG_SC_OTHER                 = 0x80
} pci_class_brdg_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_BRIDGE

typedef enum {
    PCI_C_SIMCOM_SC_SERIAL,
    PCI_C_SIMCOM_SC_PARALLEL,
    PCI_C_SIMCOM_SC_MPORT_SERIAL,
    PCI_C_SIMCOM_SC_MODEM,
    PCI_C_SIMCOM_SC_IEEE,
    PCI_C_SIMCOM_SC_SMARTCARD,
    PCI_C_SIMCOM_SC_OTHER                = 0x80
} pci_class_simcom_subclass_types_E;   // All known PCI subclass codes for Class PCI_C_SIMPLE_COM_CTRLR

typedef enum {
    PCI_C_BSP_SC_PIC,
    PCI_C_BSP_SC_DMA,
    PCI_C_BSP_SC_TIMER,
    PCI_C_BSP_SC_RTC,
    PCI_C_BSP_SC_HOT_PLUG,
    PCI_C_BSP_SC_SD,
    PCI_C_BSP_SC_IOMMU,
    PCI_C_BSP_SC_OTHER                  = 0x80
} pci_class_bsp_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_BASE_SYSTEM_PERIPHERAL

typedef enum {
    PCI_C_IDEV_SC_KEYBOARD,
    PCI_C_IDEV_SC_DIGITIZER_PEN,
    PCI_C_IDEV_SC_MOUSE,
    PCI_C_IDEV_SC_SCANNER,
    PCI_C_IDEV_SC_GAMEPORT,
    PCI_C_IDEV_SC_OTHER                 = 0x80
} pci_class_idev_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_INPUT_DEVICE_CTRLR

typedef enum {
    PCI_C_DOCK_SC_GENERIC,
    PCI_C_DOCK_SC_OTHER                 = 0x80
} pci_class_dock_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_DOCKING_STATION

typedef enum {
    PCI_C_PROC_SC_386,
    PCI_C_PROC_SC_486,
    PCI_C_PROC_SC_PENTIUM,
    PCI_C_PROC_SC_PENTIUM_PRO,
    PCI_C_PROC_SC_ALPHA,
    PCI_C_PROC_SC_POWERPC,
    PCI_C_PROC_SC_MIPS,
    PCI_C_PROC_SC_CO_PROC,
    PCI_C_PROC_SC_OTHER                 = 0x80
} pci_class_proc_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_PROCESSOR

typedef enum {
    PCI_C_SBUS_SC_FIREWIRE,
    PCI_C_SBUS_SC_ACCESS_BUS,
    PCI_C_SBUS_SC_SSA,
    PCI_C_SBUS_SC_USB,
    PCI_C_SBUS_SC_FIBRE,
    PCI_C_SBUS_SC_SM_BUS,
    PCI_C_SBUS_SC_INFINIBAND,
    PCI_C_SBUS_SC_IPMI,
    PCI_C_SBUS_SC_SERCOS,
    PCI_C_SBUS_SC_CAN_BUS,
    PCI_C_SBUS_SC_OTHER                 = 0x80
} pci_class_sbus_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_SERIAL_BUS_CTRLR

typedef enum {
    PCI_C_WLRS_SC_IRDA,
    PCI_C_WLRS_SC_CONSUMER_IR,
    PCI_C_WLRS_SC_RF,
    PCI_C_WLRS_SC_BLUETOOTH,
    PCI_C_WLRS_SC_BROADBAND,
    PCI_C_WLRS_SC_ETHERNET_801,
    PCI_C_WLRS_SC_ETHERNET_802,
    PCI_C_WLRS_SC_OTHER                 = 0x80
} pci_class_wlrs_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_WIRELESS_CTRLR

typedef enum {
    PCI_C_INTI_SC_I20
} pci_class_inti_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_INTELLIGENT_CTRLR

typedef enum {
    PCI_C_SATCOM_SC_TV,
    PCI_C_SATCOM_SC_AUDIO,
    PCI_C_SATCOM_SC_VIDEO,
    PCI_C_SATCOM_SC_VOICE,
    PCI_C_SATCOM_SC_DATA
} pci_class_satcom_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_SATELLITE_COM_CTRLR

typedef enum {
    PCI_C_ENCR_SC_NET_COMP,
    PCI_C_ENCR_SC_ENTERTAINMENT,
    PCI_C_ENCR_SC_OTHER                 = 0x80
} pci_class_encr_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_ENCRYPTION_CTRLR

typedef enum {
    PCI_C_SIGP_SC_DPIO_MODS,
    PCI_C_SIGP_SC_PERFROMANCE_COUNTERS,
    PCI_C_SIGP_SC_COM_SYNC,
    PCI_C_SIGP_SC_SIGNAL_PROCESSING_M,
    PCI_C_SIGP_SC_OTHER                 = 0x80
} pci_class_sigp_subclass_types_E;  // All known PCI subclass codes for Class PCI_C_SIGNAL_PROCESSING_CTRLR


typedef struct {
    uint16_t                vendor_id;
    uint16_t                device_id;
    uint16_t                command;
    uint16_t                status;
    uint8_t                 revision_id;
    uint8_t                 prog_if;
    uint8_t                 subclass;
    uint8_t                 class;
    uint8_t                 cache_line_size;
    uint8_t                 latency_timer;
    uint8_t                 header_type;
    uint8_t                 bist;
} pci_device_header_T;

typedef struct {
    pci_device_header_T     header;
    uint32_t                bar_0;
    uint32_t                bar_1;
    uint32_t                bar_2;
    uint32_t                bar_3;
    uint32_t                bar_4;
    uint32_t                bar_5;
    uint32_t                card_bus_cis_ptr;
    uint16_t                subsystem_vendor_id;
    uint16_t                subsystem_id;
    uint32_t                expansion_rom_base_address;
    uint8_t                 capabilities_ptr;
    uint8_t                 reserved_0;
    uint16_t                reserved_1;
    uint32_t                reserved_2;
    uint8_t                 interrupt_line;
    uint8_t                 interrupt_pin;
    uint8_t                 min_grant;
    uint8_t                 max_latency;
} pci_header_0_T;

void        pci_enumerate_pci       (acpi_mcfg_header_T* mcfg);
void        pci_enumerate_bus       (uint64_t base_address,   uint64_t bus);
void        pci_enumerate_device    (uint64_t bus_address,    uint64_t device);
void        pci_enumerate_function  (uint64_t device_address, uint64_t function);

#endif //COSMOS_PCI_H
