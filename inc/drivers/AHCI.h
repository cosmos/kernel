//
// Created by antifallobst on 8/26/22.
//

#ifndef COSMOS_AHCI_H
#define COSMOS_AHCI_H

#include <stdint.h>
#include <stdbool.h>
#include <drivers/PCI.h>

#define ATA_DEVICE_BUSY             0x80
#define ATA_DEVICE_DRQ              0x08
#define ATA_COMMAND_READ_DMA_EX     0x25
#define ATA_COMMAND_WRITE_DMA_EX    0x35

#define HBA_PX_IS_TFES              (1 << 30)

#define HBA_PORT_DEVICE_PRESENT     0x3
#define HBA_PORT_IPM_ACTIVE         0x1

typedef enum {
    HBA_PX_COMMAND_CR       = 0x8000,
    HBA_PX_COMMAND_FRE      = 0x0010,
    HBA_PX_COMMAND_ST       = 0x0001,
    HBA_PX_COMMAND_FR       = 0x4000
} ahci_hba_px_commands_E;

typedef enum {
    SATA_SIGNATURE_ATAPI    = 0xEB140101,
    SATA_SIGNATURE_ATA      = 0x00000101,
    SATA_SIGNATURE_SEMB     = 0xC33C0101,
    SATA_SIGNATURE_PM       = 0x96690101
} ahci_sata_signatures_E;

typedef enum {
    AHCI_NONE               = 0,
    AHCI_SATA               = 1,
    AHCI_SEMB               = 2,
    AHCI_PM                 = 3,
    AHCI_SATAPI             = 4
} ahci_port_type_E;

typedef enum {
    FIS_TYPE_REG_H2D        = 0x27,
    FIS_TYPE_REG_D2H        = 0x34,
    FIS_TYPE_DMA_ACT        = 0x39,
    FIS_TYPE_DMA_SETUP      = 0x41,
    FIS_TYPE_DATA           = 0x46,
    FIS_TYPE_BIST           = 0x58,
    FIS_TYPE_PIO_SETUP      = 0x5F,
    FIS_TYPE_DEV_BITS       = 0xA1,
} ahci_fis_type_E;

typedef struct {
    uint32_t                command_list_base;
    uint32_t                command_list_base_upper;
    uint32_t                fis_base_address;
    uint32_t                fis_base_address_upper;
    uint32_t                interrupt_status;
    uint32_t                interrupt_enable;
    uint32_t                command_status;
    uint32_t                reserved_0;
    uint32_t                task_file_data;
    uint32_t                signature;
    uint32_t                sata_status;
    uint32_t                sata_control;
    uint32_t                sata_error;
    uint32_t                sata_active;
    uint32_t                command_issue;
    uint32_t                sata_notification;
    uint32_t                fis_switch_control;
    uint32_t                reserved_1                          [11];
    uint32_t                vendor                              [4];
} ahci_hba_port_T;

typedef struct {
    uint32_t                host_capability;
    uint32_t                global_host_control;
    uint32_t                interrupt_status;
    uint32_t                ports_implemented;
    uint32_t                version;
    uint32_t                ccc_control;
    uint32_t                ccc_ports;
    uint32_t                enclosure_management_location;
    uint32_t                enclosure_management_ontrol;
    uint32_t                host_capabilities_extended;
    uint32_t                bios_handoff_control_status;
    uint8_t                 reserved_0                          [0x74];
    uint8_t                 vendor                              [0x60];
    ahci_hba_port_T         ports                               [1];
} ahci_hba_memory_T;

typedef struct {
    uint8_t                 command_fis_length                  :5;
    uint8_t                 atapi                               :1;
    uint8_t                 write                               :1;
    uint8_t                 prefetchable                        :1;

    uint8_t                 reset                               :1;
    uint8_t                 bist                                :1;
    uint8_t                 clear_busy                          :1;
    uint8_t                 reserved_0                          :1;
    uint8_t                 port_multiplier                     :4;

    uint16_t                prdt_length;
    uint32_t                prdb_count;
    uint32_t                command_table_base_address;
    uint32_t                command_table_base_address_upper;
    uint32_t                reserved_1                          [4];
} ahci_hba_command_header_T;

typedef struct {
    uint32_t                data_base_address;
    uint32_t                data_base_address_upper;
    uint32_t                reserved_0;

    uint32_t                byte_count                          :22;
    uint32_t                reserved_1                          :9;
    uint32_t                interrupt_on_completion             :1;
} ahci_hba_prdt_entry_T;

typedef struct{
    uint8_t                 command_fis                         [64];
    uint8_t                 atapi_command                       [16];
    uint8_t                 reserved                            [48];
    ahci_hba_prdt_entry_T   prdt_entry                          [];
} ahci_hba_command_table_T;

typedef struct {
    uint8_t                 fis_type;

    uint8_t                 port_multiplier                     :4;
    uint8_t                 reserved_0                          :3;
    uint8_t                 command_control                     :1;

    uint8_t                 command;
    uint8_t                 feature_low;

    uint8_t                 lba_0;
    uint8_t                 lba_1;
    uint8_t                 lba_2;
    uint8_t                 device_register;

    uint8_t                 lba_3;
    uint8_t                 lba_4;
    uint8_t                 lba_5;
    uint8_t                 feature_high;

    uint8_t                 count_low;
    uint8_t                 count_high;
    uint8_t                 iso_command_completion;
    uint8_t                 control;

    uint8_t                 reserved_1                          [4];
} ahci_fis_register_hardware_to_device_T;

typedef struct {
    uint8_t                 fis_type;

    uint8_t                 port_multiplier                     :4;
    uint8_t                 reserved_0                          :2;
    uint8_t                 interrupt                           :1;
    uint8_t                 reserved_1                          :1;

    uint8_t                 status_register;
    uint8_t                 error_register;

    uint8_t                 lba_0;
    uint8_t                 lba_1;
    uint8_t                 lba_2;
    uint8_t                 device_register;

    uint8_t                 lba_3;
    uint8_t                 lba_4;
    uint8_t                 lba_5;
    uint8_t                 reserved_2;

    uint8_t                 count_low;
    uint8_t                 count_high;
    uint8_t                 reserved_3                          [2];

    uint8_t                 reserved_4                          [4];
} ahci_fis_register_device_to_hardware_T;


typedef struct {
    ahci_hba_port_T*        hba_port;
    ahci_port_type_E        port_type;
    uint8_t*                buffer;
    uint8_t                 port_number;
} ahci_port_T;

typedef struct {
    pci_device_header_T*    pci_base_address;
    ahci_hba_memory_T*      abar;
    ahci_port_T*            ports                               [32];
    uint8_t                 port_count;
} ahci_driver_T;

void                ahci_port_configure         (ahci_port_T* port);
void                ahci_port_stop_command      (ahci_port_T* port);
void                ahci_port_start_command     (ahci_port_T* port);
bool                ahci_port_read              (ahci_port_T* port, uint64_t sector, uint32_t sector_count, void* buffer);
bool                ahci_port_write             (ahci_port_T* port, uint64_t sector, uint32_t sector_count, void* buffer);

ahci_port_type_E    ahci_hba_port_check_type    (ahci_hba_port_T* hba_port);

void                ahci_driver_init            (ahci_driver_T* driver, pci_device_header_T* pci_base_address);
void                ahci_driver_probe_ports     (ahci_driver_T* driver);


extern ahci_driver_T* g_ahci_driver;

#endif //COSMOS_AHCI_H