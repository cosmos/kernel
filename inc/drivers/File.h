//
// Created by antifallobst on 8/16/22.
//

#ifndef COSMOS_FILE_H
#define COSMOS_FILE_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define FILE_FILENAME_LENGTH_MAX    128

typedef enum {
    FILE_WRITE,
    FILE_READ
} FileModes_E;

typedef enum {
    FS_USTAR,
    FS_EXT4,
    FS_FAT32
} FileSystems_E;

typedef struct {
    char            name[FILE_FILENAME_LENGTH_MAX];
    uint64_t        size;
    FileModes_E     mode;
} file_header_T;

typedef struct {
    file_header_T   header;
    void*           handler;
} file_T;

file_T  open_file   (const char* path, FileModes_E mode);
bool    check_path  (const char* path);
bool    read_file   (file_T* file, uint64_t pos, void* buffer, size_t n);
bool    write_file  (file_T* file, uint64_t pos, void* buffer, size_t n);

extern FileSystems_E g_file_system;

#endif //COSMOS_FILE_H
