//
// Created by antifallobst on 8/26/22.
//

#ifndef COSMOS_ACPI_H
#define COSMOS_ACPI_H

#include <stdint.h>

typedef struct {
    uint8_t             signature           [8];
    uint8_t             checksum;
    uint8_t             oem_id              [6];
    uint8_t             revision;
    uint32_t            rsdt_address;
    uint32_t            length;
    uint64_t            xsdt_address;
    uint8_t             extended_checksum;
    uint8_t             reserved            [3];
} __attribute__((packed)) acpi_rsdp_2_T;

typedef struct {
    uint8_t             signature           [4];
    uint32_t            length;
    uint8_t             revision;
    uint8_t             checksum;
    uint8_t             oem_id              [6];
    uint8_t             oem_table_id        [8];
    uint32_t            oem_revision;
    uint32_t            creator_id;
    uint32_t            creator_revision;
} __attribute__((packed)) acpi_sdt_header_T;

typedef struct {
    acpi_sdt_header_T   header;
    uint64_t            Reserved;
} __attribute__((packed)) acpi_mcfg_header_T;

typedef struct {
    uint64_t            base_address;
    uint16_t            pci_segment_group;
    uint8_t             start_bus;
    uint8_t             end_bus;
    uint32_t            reserved;
} __attribute__((packed)) acpi_device_config_T;


void* acpi_find_table(acpi_sdt_header_T* sdt_header, char* signature);

#endif //COSMOS_ACPI_H