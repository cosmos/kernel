//
// Created by antifallobst on 9/9/22.
//

#ifndef COSMOS_CPU_H
#define COSMOS_CPU_H

#include <stdint.h>

typedef enum {
    EFLAGS_CARRY                        = 0,
    EFLAGS_PARITY                       = 2,
    EFLAGS_AUXILIARY                    = 4,
    EFLAGS_ZERO                         = 6,
    EFLAGS_SIGN                         = 7,
    EFLAGS_TRAP                         = 8,
    EFLAGS_INTERRUPT_ENABLE             = 9,
    EFLAGS_DIRECTION                    = 10,
    EFLAGS_OVERFLOW                     = 11,
    EFLAGS_IO_PRIVILEGE_0               = 12,
    EFLAGS_IO_PRIVILEGE_1               = 13,
    EFLAGS_NESTED_TASK                  = 14,
    EFLAGS_RESUME                       = 16,
    EFLAGS_VIRTUAL_8086                 = 17,
    EFLAGS_ALIGNMENT_CHECK              = 18,
    EFLAGS_VIRTUAL_INTERRUPT            = 19,
    EFLAGS_VIRTUAL_INTERRUPT_PENDING    = 20,
    EFLAGS_CPUID                        = 21
} cpu_eflags_E;

typedef struct {
    // Registers saved by assembly function
    uint64_t    cr3;
    uint64_t    rax;
    uint64_t    rbx;
    uint64_t    rcx;
    uint64_t    rdx;
    uint64_t    rsi;
    uint64_t    rdi;
    uint64_t    rbp;

    // Manually pushed
    uint64_t    interrupt_id;
    uint64_t    error_code;

    // Saved by CPU
    uint64_t    rip;
    uint64_t    crs;
    uint64_t    eflags;
    uint64_t    rsp;
    uint64_t    ss;
} cpu_state_T;

extern cpu_state_T g_kernel_state;

#endif //COSMOS_CPU_H
