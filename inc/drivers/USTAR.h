//
// Created by antifallobst on 8/16/22.
//

#ifndef COSMOS_USTAR_H
#define COSMOS_USTAR_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#define USTAR_BLOCK_SIZE    512

enum USTARTypeFlags_E {
    USTAR_FILE,
    USTAR_HARD_LINK,
    USTAR_SYM_LINK,
    USTAR_CHAR_DEVICE,
    USTAR_BLOCK_DEVICE,
    USTAR_DIRECTORY,
    USTAR_PIPE
};

typedef struct {
    uint8_t filename            [100];
    uint8_t mode                [8];
    uint8_t ownerID             [8];
    uint8_t groupID             [8];
    uint8_t size                [12];
    uint8_t last_modification   [12];
    uint8_t checksum            [8];
    uint8_t type;
    uint8_t filename_linked     [100];
    uint8_t indicator           [6];
    uint8_t version             [2];
    uint8_t user                [32];
    uint8_t group               [32];
    uint8_t device0             [8];
    uint8_t device1             [8];
    uint8_t prefix              [155];
    uint8_t reserved            [12];
} ustar_entry_header_T;

void                    ustar_analyse_archive   (void* archive);
ustar_entry_header_T*   ustar_lookup_path       (void* archive, const char* path);
bool                    ustar_verify_header     (ustar_entry_header_T* header);
uint64_t                ustar_get_entry_size    (ustar_entry_header_T* header);
bool                    ustar_read_file         (ustar_entry_header_T* header, uint64_t offset, void* buffer, size_t n);

#endif //COSMOS_USTAR_H
