//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_MEMORY_H
#define COSMOS_MEMORY_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <memory/EFIMemory.h>

uint64_t    get_memory_size     (EFI_MEMORY_DESCRIPTOR_T* memory_map, uint64_t memory_map_entries, uint64_t memory_map_descriptor_size);
void        memset              (void* start, uint8_t value, uint64_t num);
void        memcpy              (void* dest, void* src, uint64_t len);
void        rmemcpy             (void* dest, void* src, uint64_t len);
bool        memcmp              (void* a, void* b, uint64_t num);
void        hexdump             (void* buffer, uint64_t num);

#endif //COSMOS_MEMORY_H
