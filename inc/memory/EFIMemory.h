//
// Created by antifallobst on 24/05/2022.
//

#ifndef COSMOS_EFIMEMORY_H
#define COSMOS_EFIMEMORY_H

#include <stdint.h>

typedef struct {
    uint32_t    type;
    void*       physical_address;
    void*       virtual_address;
    uint64_t    num_pages;
    uint64_t    attributes;
} EFI_MEMORY_DESCRIPTOR_T;

typedef enum {
    EMT_RESERVED,
    EMT_LOADER_CODE,
    EMT_LOADER_DATA,
    EMT_BOOTSERVICES_CODE,
    EMT_BOOTSERVICES_DATA,
    EMT_RUNTIMESERVICES_CODE,
    EMT_RUNTIMESERVICES_DATA,
    EMT_CONVENTIONAL,
    EMT_UNUSABLE,
    EMT_ACPI_RECLAIM,
    EMT_ACPI_NVS,
    EMT_MAPPED_IO,
    EMT_MAPPED_IO_PORT_SPACE,
    EMT_PAL_CODE
} EFIMemoryTypes_E;

extern const char* EFI_MEMORY_TYPE_STRINGS[];

#endif //COSMOS_EFIMEMORY_H
