//
// Created by antifallobst on 9/19/22.
//

#ifndef COSMOS_STACK_H
#define COSMOS_STACK_H

#include <drivers/cpu.h>

typedef struct {
    uint64_t    rbp;
    uint64_t    rip;
} stack_frame_T;

void stack_backtrace(cpu_state_T* state);

#endif //COSMOS_STACK_H
