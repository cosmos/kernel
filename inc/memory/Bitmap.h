//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_BITMAP_H
#define COSMOS_BITMAP_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct {
    size_t      size;
    uint8_t*    buffer;
} Bitmap_T;

void    init_bitmap             (Bitmap_T* bitmap, void* buffer, size_t size);
bool    get_bitmap_at_index     (Bitmap_T* bitmap, uint64_t index);
bool    set_bitmap_at_index     (Bitmap_T* bitmap, uint64_t index, bool value);

#endif //COSMOS_BITMAP_H
