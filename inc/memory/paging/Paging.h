//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_PAGING_H
#define COSMOS_PAGING_H

#include <stdint.h>
#include <stdbool.h>

#define PAGE_SIZE   4096    // one page is has a size of 4096 bytes

typedef enum {
    PTF_PRESENT         = 0b0000000000000001,   // 1 <<  0
    PTF_READ_WRITE      = 0b0000000000000010,   // 1 <<  1
    PTF_USER_SUPER      = 0b0000000000000100,   // 1 <<  2
    PTF_WRITE_THROUGH   = 0b0000000000001000,   // 1 <<  3
    PTF_CACHE_DISABLED  = 0b0000000000010000,   // 1 <<  4
    PTF_ACCESSED        = 0b0000000000100000,   // 1 <<  5
    PTF_LARGER_PAGES    = 0b0000000010000000,   // 1 <<  7
    PTF_CUSTOM_0        = 0b0000001000000000,   // 1 <<  9
    PTF_CUSTOM_1        = 0b0000010000000000,   // 1 << 10
    PTF_CUSTOM_2        = 0b0000100000000000,   // 1 << 11
    PTF_NO_EXECUTE      = 0x8000000000000000    // 1 << 63  - only if supported
} page_table_flags_E;

typedef struct {
    uint64_t entries [512];
}__attribute__((aligned(0x1000))) paging_page_table_T;

bool            GetFlag             (uint64_t* value, page_table_flags_E flag);
void            SetAddress          (uint64_t* value, uint64_t address);
uint64_t        GetAddress          (uint64_t* value);

extern paging_page_table_T* g_page_map;

#endif //COSMOS_PAGING_H
