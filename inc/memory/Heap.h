//
// Created by antifallobst on 29/07/2022.
//

#ifndef COSMOS_HEAP_H
#define COSMOS_HEAP_H

#include <stddef.h>
#include <stdbool.h>

struct HeapSegmentHeader_T{
    size_t                          length;
    bool                            free;
    struct HeapSegmentHeader_T*     next;
    struct HeapSegmentHeader_T*     prev;
};

void    initialize_heap     (void* heap_address, size_t page_count);
void*   malloc              (size_t size);
void    free                (void* address);
void    expand_heap         (size_t length);

#endif //COSMOS_HEAP_H
