# cosmos - kernel

The kernel for the small x86_64 **cosmos** operating system.

My goal of writing this OS is mostly learning how stuff works.

And I think it would be cool to write an OS that has a Text Editor and a C compiler,
because at this point i could say, that I wrote a fully functional OS.

Contribution welcome :D</br>
What you could do:
* Test - test cosmos and report (or even fix) bugs
* Design - design stuff like syscalls to make the system fit together
* Code
  * kernel - kind of a nightmare
  * libcosmos / modules -  pony Farm

(I would even try to document the code)

## Features
 * **Detailed Panic Screen**
   * Stacktracing
   * CPU Register dump
   * Page Fault information
 * **Ramdisk**
 * **ELF Loading / Executing**
 * **Multithreading**
   * Preemptive Scheduling
   * Dynamic Linking (Shared Libraries)
   * Inter Process Communication (currently disabled, because of the multiprocessing to multithreading refactor)
   * Syscalls
 * **JSON Parsing** (json will probably be replaced by something like *.conf* files)

## In Work
 * **Modules**
 * **Refactor IPC**
 * **Standard Library (libcosmos)**
 * **Paging System (Improvements / Rework)**
 * **Memory leak fixes**
 * **minor improvements**

## Planed
 * **Keyboard input** (not yet implemented, because I wanna have this modular, so the standard PS/2 interrupt and later the USB-Keyboard will talk to a **KEYBOARD module** that then talks to the Applications)
 * **Shell**
 * **Transparent FDE** (Full Disk Encryption)
 * **AHCI (module)**
 * **FAT (module)**
 * **EXT2 (module)**
 * **USB (module)** - I'm scared of this
 * **Text Editor**
 * **C Compiler** - not sure if I will port TCC or try to roll my own