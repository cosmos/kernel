//
// Created by antifallobst on 10/28/22.
//

#include <module/ModuleManager.h>
#include <utils/Logger.h>
#include <utils/StrUtil.h>
#include <utils/Panic.h>
#include <memory/Heap.h>
#include <memory/Memory.h>
#include <process/Scheduler.h>

module_manager_T                   G_mod_manager;
module_manager_T* g_mod_manager = &G_mod_manager;


int mod_num_needed_symbols[MOD_END_OF_ENUM] = {
        0,  /* MOD_INVALID */
        NUM_NEEDED_SYMS_LOGGER,
        NUM_NEEDED_SYMS_RENDERER,
        NUM_NEEDED_SYMS_KEYBOARD,
        NUM_NEEDED_SYMS_PCI,
        NUM_NEEDED_SYMS_AHCI,
        NUM_NEEDED_SYMS_VFAT,
        NUM_NEEDED_SYMS_EXT2,
        NUM_NEEDED_SYMS_POWER,
        NUM_NEEDED_SYMS_USB
};

char* mod_needed_symbols_logger[NUM_NEEDED_SYMS_LOGGER] = {
        "_mod_log"
};

char** mod_needed_symbols[MOD_END_OF_ENUM] = {
        NULL,
        mod_needed_symbols_logger
};


bool mod_has_needed_symbols(module_type_E type, symbols_T* symbols) {
    char**  symbol_lookup_table = mod_needed_symbols[type];
    int     num_needed_symbols  = mod_num_needed_symbols[type];

    for (int i = 0; i < num_needed_symbols; i++) {
        if (symbol_lookup(symbols, symbol_lookup_table[i]) == NULL) {
            log(LOGGING_ERROR, "Module Manager -> Missing symbol '%s' in module %s", symbol_lookup_table[i], mod_type_to_string(type));
            return false;
        }
    }

    return true;
}


void mod_manager_init(module_manager_T* mod_manager) {
    mod_manager->num_modules    = MOD_END_OF_ENUM;
    mod_manager->modules        = malloc(mod_manager->num_modules * sizeof(module_T));
}

void mod_manager_load_mods(module_manager_T* mod_manager, json_ast_node_T* json_master) {
    mod_manager->num_modules = json_ast_node_num_childs(json_master);
    for (int i = 0; i < mod_manager->num_modules; i++) {
        json_ast_node_T*    node    = json_ast_node_get_child_at_index(json_master, i);
        module_type_E       type    = mod_string_to_type(node->data.string);
        if (type == MOD_INVALID) {
            log(LOGGING_WARNING, "Module Manager -> '%s' is not a valid module type", node->data.string);
//            return;
            mod_manager->num_modules -= 1;
            continue;
        }

        uint64_t            size    = format(NULL, "Module <%s>", node->data.string);
        char                buffer   [size];
        format(buffer, "Module <%s>", node->data.string);
        buffer[size]                = '\0';

        uint64_t pid = load_process_from_file(json_ast_node_get_assigned_string(node), PROCESS_MODULE, buffer);

        if (!mod_has_needed_symbols(type, &g_scheduler->processes[pid]->executable.image.symbols)) {
            panic("Missing symbol in module", NULL);
        }

        module_T* mod   = &mod_manager->modules[type];
        mod->type       =  type;
        mod->pid        =  pid;

        log(LOGGING_SUCCESS, "Module Manager -> Registered Module: %s  PID[%d]", node->data.string, pid);
    }

    log(LOGGING_SUCCESS, "Module Manager -> Registered all modules");

    for (int i = 1; i < mod_manager->num_modules + 1; i++) {
        module_T* mod = &mod_manager->modules[i];
        log(LOGGING_DEBUG, "Starting Module %s process...", mod_type_to_string(i));
        start_process(g_scheduler, (int)mod->pid);
    }
}

void mod_manager_call_mod(module_manager_T* mod_manager, module_type_E mod, uint16_t call, thread_T* caller, int argc, uint64_t* argv) {
    process_T* mother_process   = g_scheduler->processes[mod_manager->modules[mod].pid];
    uint32_t   tid              = process_new_thread(mother_process, THREAD_MODULE_CALL,
                                                     symbol_lookup(&mother_process->executable.image.symbols, mod_needed_symbols[mod][call]),
                                                     argc, argv);

    thread_T*  call_thread      = g_scheduler->threads[tid];

    call_thread->caller_tid     =  caller->g_tid;
    call_thread->ret_val_ptr    = &caller->cpu_state.rax;

    start_thread(g_scheduler, tid);
    pause_thread(g_scheduler, caller->g_tid);
}

uint64_t mod_manager_get_mod_pid(module_manager_T* mod_manager, module_type_E mod) {
    return mod_manager->modules[mod].pid;
}