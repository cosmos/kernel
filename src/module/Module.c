//
// Created by antifallobst on 10/28/22.
//

#include <module/Module.h>
#include <utils/StrUtil.h>

char* module_type_ID_strings[MOD_END_OF_ENUM] = {
        "INVALID",
        "LOGGER",
        "RENDERER",
        "KEYBOARD",
        "PCI",
        "AHCI",
        "VFAT",
        "EXT2",
        "POWER",
        "USB"
};

const char* mod_type_to_string(module_type_E type) {
    return module_type_ID_strings[type];
}

module_type_E mod_string_to_type(const char* type_string) {
    for (int i = 1; i < MOD_END_OF_ENUM; i++) {
        if (strcmp(type_string, module_type_ID_strings[i])) {
            return i;
        }
    }
    return MOD_INVALID;
}