//
// Created by antifallobst on 8/26/22.
//

#include <drivers/ACPI.h>
#include <utils/Logger.h>

void* acpi_find_table(acpi_sdt_header_T* sdt_header, char* signature) {
    int entries = (int)(sdt_header->length - sizeof(acpi_sdt_header_T)) / 8;

    for (int i = 0; i < entries; i++) {
        acpi_sdt_header_T* new_sdt_header = (acpi_sdt_header_T*)*(uint64_t*)((uint64_t)sdt_header + sizeof(acpi_sdt_header_T) + (i * 8));
        for (int j = 0; j < 4; j++) {
            if (new_sdt_header->signature[j] != signature[j]) break;
            if (j == 3) return new_sdt_header;
        }
    }
    log(LOGGING_WARNING, "ACPI -> Table with signature '%s' not found!", signature);
    return 0;
}