//
// Created by antifallobst on 8/16/22.
//

#include <drivers/File.h>
#include <drivers/USTAR.h>
#include <drivers/Ramdisk.h>
#include <utils/Logger.h>
#include <memory/Memory.h>

bool check_path_ustar(const char* path) {
    return (ustar_lookup_path(g_ramdisk, path) != NULL);
}

file_T open_file_ustar(const char* path, FileModes_E mode) {
    file_T file;

    file.handler    = ustar_lookup_path(g_ramdisk, path);
    if (file.handler == NULL) {
        return file;
    }

    memcpy(file.header.name, ((ustar_entry_header_T*)file.handler)->filename, 100);
    file.header.size = ustar_get_entry_size(file.handler);
    file.header.mode = mode;

    return file;
}

bool read_file_ustar(file_T* file, uint64_t pos, void* buffer, size_t n) {
    bool status = ustar_read_file(file->handler, pos, buffer, n);

    return status;
}

bool check_path(const char* path) {
    switch(g_file_system) {
        case FS_USTAR: {
            return check_path_ustar(path);
        }
        case FS_EXT4: {
            log(LOGGING_WARNING, "File -> filesystem EXT4 is set but not implemented yet.");
            break;
        }
        case FS_FAT32: {
            log(LOGGING_WARNING, "File -> filesystem FAT32 is set but not implemented yet.");
            break;
        }
    }

    return false;
}

file_T open_file(const char* path, FileModes_E mode) {
    switch(g_file_system) {
        case FS_USTAR: {
            if (mode == FILE_WRITE) {
                log(LOGGING_WARNING, "File -> cannot open file \"%s\" in WRITE mode because it is in a readonly filesystem");
                break;
            }
            return open_file_ustar(path, mode);
        }
        case FS_EXT4: {
            log(LOGGING_WARNING, "File -> filesystem EXT4 is set but not implemented yet.");
            break;
        }
        case FS_FAT32: {
            log(LOGGING_WARNING, "File -> filesystem FAT32 is set but not implemented yet.");
            break;
        }
    }

    file_T puppet;
    return puppet;
}

bool read_file(file_T* file, uint64_t pos, void* buffer, size_t n) {
    switch(g_file_system) {
        case FS_USTAR: {
            return read_file_ustar(file, pos, buffer, n);
        }
        case FS_EXT4: {
            log(LOGGING_WARNING, "File -> filesystem EXT4 is set but not implemented yet.");
            break;
        }
        case FS_FAT32: {
            log(LOGGING_WARNING, "File -> filesystem FAT32 is set but not implemented yet.");
            break;
        }
    }
    return false;
}

bool write_file(file_T* file, uint64_t pos, void* buffer, size_t n) {
    if (file->header.mode == FILE_WRITE) {
        switch(g_file_system) {
            case FS_USTAR: {
                log(LOGGING_WARNING, "File -> If you see this message something went really wrong - but its not that important");
                break;
            }
            case FS_EXT4: {
                log(LOGGING_WARNING, "File -> filesystem EXT4 is set but not implemented yet.");
                break;
            }
            case FS_FAT32: {
                log(LOGGING_WARNING, "File -> filesystem FAT32 is set but not implemented yet.");
                break;
            }
        }
    }
    return false;
}