//
// Created by antifallobst on 8/18/22.
//

#include <drivers/time/PIT.h>
#include <utils/IO.h>
#include <utils/Logger.h>

double          pit_time_since_boot     = 0;
const uint64_t  pit_base_frequency      = 1193182;
uint16_t        G_divisor               = PIT_DIVISOR;

void pit_sleep_seconds(double seconds) {
    double start_time = pit_time_since_boot;
    while(pit_time_since_boot < start_time + seconds) {
        asm("hlt");
    }
}

void pit_sleep_milliseconds(uint64_t milliseconds) {
    pit_sleep_seconds((double)milliseconds / 1000);
}

void pit_set_divisor(uint16_t divisor) {
    if (divisor < 100) divisor = 100;
    G_divisor = divisor;
    io_out_x8(0x40, (uint8_t) (divisor & 0x00ff));
    io_wait();
    io_out_x8(0x40, (uint8_t) ((divisor & 0xff00) >> 8));
    log(LOGGING_INFO, "PIT -> Divisor was set to: %d", divisor);
}

uint64_t pit_get_frequency() {
    return pit_base_frequency / G_divisor;
}

void pit_set_frequency(uint64_t frequency) {
    pit_set_divisor(pit_base_frequency / frequency);
}

void pit_tick(cpu_state_T* state) {
    pit_time_since_boot += 1 / (double)pit_get_frequency();
}