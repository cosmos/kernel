//
// Created by antifallobst on 8/26/22.
//

#include <drivers/AHCI.h>
#include <memory/paging/PageMap.h>
#include <memory/paging/PageFrame.h>
#include <memory/Heap.h>
#include <memory/Memory.h>
#include <utils/Logger.h>

ahci_driver_T   ahci_driver;
ahci_driver_T*  g_ahci_driver     = &ahci_driver;

void ahci_port_configure(ahci_port_T* port) {
    ahci_port_stop_command(port);

    void* new_base                              = request_page();
    port->hba_port->command_list_base           = (uint32_t)(uint64_t)new_base;
    port->hba_port->command_list_base_upper     = (uint32_t)((uint64_t)new_base >> 32);
    memset((void*)(port->hba_port->command_list_base), 0, 1024);

    void* fis_base                              = request_page();
    port->hba_port->fis_base_address            = (uint32_t)(uint64_t)fis_base;
    port->hba_port->fis_base_address_upper      = (uint32_t)((uint64_t)fis_base >> 32);
    memset(fis_base, 0, 256);

    ahci_hba_command_header_T* command_header   = (ahci_hba_command_header_T*)((uint64_t)port->hba_port->command_list_base + ((uint64_t)port->hba_port->command_list_base_upper << 32));

    for (int i = 0; i < 32; i++) {
        command_header[i].prdt_length = 8;

        void* command_table_address                         = request_page();
        uint64_t address                                    = (uint64_t)command_table_address + (i << 8);
        command_header[i].command_table_base_address        = (uint32_t)(uint64_t)address;
        command_header[i].command_table_base_address_upper  = (uint32_t)((uint64_t)address >> 32);
        memset((void*)address, 0, 256);
    }

    ahci_port_start_command(port);

}

void ahci_port_stop_command(ahci_port_T* port) {
    port->hba_port->command_status &= ~HBA_PX_COMMAND_ST;
    port->hba_port->command_status &= ~HBA_PX_COMMAND_FRE;

    while (true) {
        if (port->hba_port->command_status & HBA_PX_COMMAND_FR) continue;
        if (port->hba_port->command_status & HBA_PX_COMMAND_CR) continue;
        break;
    }
}

void ahci_port_start_command(ahci_port_T* port) {
    while (port->hba_port->command_status & HBA_PX_COMMAND_CR);

    port->hba_port->command_status |= HBA_PX_COMMAND_FRE;
    port->hba_port->command_status |= HBA_PX_COMMAND_ST;
}

bool ahci_port_read(ahci_port_T* port, uint64_t sector, uint32_t sector_count, void* buffer) {
    uint64_t spin = 0;

    while ((port->hba_port->task_file_data & (ATA_DEVICE_BUSY | ATA_DEVICE_DRQ)) && spin < 1000000) {
        spin++;
    }
    if (spin == 1000000) return false;

    uint32_t sector_low                 = (uint32_t) sector;
    uint32_t sector_high                = (uint32_t) (sector >> 32);
    port->hba_port->interrupt_status    = (uint32_t) -1;


    ahci_hba_command_header_T* command_header   = (ahci_hba_command_header_T*)port->hba_port->command_list_base;
    command_header->command_fis_length          = sizeof(FIS_TYPE_REG_H2D) / sizeof(uint32_t);
    command_header->write                       = 0;
    command_header->prdt_length                 = 1;


    ahci_hba_command_table_T* command_table     = (ahci_hba_command_table_T*)(command_header->command_table_base_address);
    memset(command_table, 0, sizeof(ahci_hba_command_table_T) + (command_header->prdt_length - 1) * sizeof(ahci_hba_prdt_entry_T));

    command_table->prdt_entry[0].data_base_address          = (uint32_t)(uint64_t)buffer;
    command_table->prdt_entry[0].data_base_address_upper    = (uint32_t)((uint64_t)buffer >> 32);
    command_table->prdt_entry[0].byte_count                 = (sector_count << 9)-1;
    command_table->prdt_entry[0].interrupt_on_completion    = 1;


    ahci_fis_register_hardware_to_device_T* command_fis = (ahci_fis_register_hardware_to_device_T*)(&command_table->command_fis);

    command_fis->fis_type           = FIS_TYPE_REG_H2D;
    command_fis->command_control    = 1;
    command_fis->command            = ATA_COMMAND_READ_DMA_EX;

    command_fis->lba_0              = (uint8_t)sector_low;
    command_fis->lba_1              = (uint8_t)(sector_low >> 8);
    command_fis->lba_2              = (uint8_t)(sector_low >> 16);
    command_fis->lba_3              = (uint8_t)sector_high;
    command_fis->lba_4              = (uint8_t)(sector_high >> 8);
    command_fis->lba_5              = (uint8_t)(sector_high >> 16);

    command_fis->device_register    = 1 << 6;

    command_fis->count_low          = sector_count & 0xFF;
    command_fis->count_high         = (sector_count >> 8) & 0xFF;

    port->hba_port->command_issue   = 1;

    while (true) {
        if (port->hba_port->command_issue == 0) break;
        if (port->hba_port->interrupt_status & HBA_PX_IS_TFES) return false;
    }

    return true;
}

bool ahci_port_write(ahci_port_T* port, uint64_t sector, uint32_t sector_count, void* buffer) {
    // TODO: implement
    log(LOGGING_WARNING, "AHCI Write -> Not implemented yet!");
    return false;
}

ahci_port_type_E ahci_hba_port_check_type(ahci_hba_port_T* hba_port) {
    uint32_t    sata_status                 = hba_port->sata_status;
    uint8_t     interface_power_management  = (sata_status >> 8) & 0b111;
    uint8_t     device_detection            = sata_status & 0b111;

    if (device_detection != HBA_PORT_DEVICE_PRESENT || interface_power_management != HBA_PORT_IPM_ACTIVE) {
        return AHCI_NONE;
    }

    switch (hba_port->signature) {
        case SATA_SIGNATURE_ATAPI: {
            return AHCI_SATAPI;
        }
        case SATA_SIGNATURE_ATA: {
            return AHCI_SATA;
        }
        case SATA_SIGNATURE_SEMB: {
            return AHCI_SEMB;
        }
        case SATA_SIGNATURE_PM: {
            return AHCI_PM;
        }
        default: {
            return AHCI_NONE;
        }
    }
}

void ahci_driver_init(ahci_driver_T* driver, pci_device_header_T* pci_base_address) {
    driver->pci_base_address    = pci_base_address;
    driver->abar                = (ahci_hba_memory_T*)((pci_header_0_T*)pci_base_address)->bar_5;

    map_memory(g_page_map, PTF_READ_WRITE, driver->abar, driver->abar);
    ahci_driver_probe_ports(driver);

    for (int i = 0; i < driver->port_count; i++) {
        ahci_port_T* port = driver->ports[i];

        ahci_port_configure(port);
    }
}

void ahci_driver_probe_ports(ahci_driver_T* driver) {
    uint32_t ports_implemented = driver->abar->ports_implemented;

    for (int i = 0; i < 32; i++) {
        if (ports_implemented & (1 << i)) {
            ahci_port_type_E port_type = ahci_hba_port_check_type(&driver->abar->ports[i]);

            if (port_type == AHCI_SATA || port_type == AHCI_SATAPI) {
                driver->ports[driver->port_count]               = (ahci_port_T*)malloc(sizeof(ahci_port_T));
                driver->ports[driver->port_count]->port_type    = port_type;
                driver->ports[driver->port_count]->hba_port     = &driver->abar->ports[i];
                driver->ports[driver->port_count]->port_number  = driver->port_count;
                driver->port_count += 1;
            }
        }
    }
}