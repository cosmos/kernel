//
// Created by antifallobst on 23/05/2022.
//

#include <kernel/Kernel.h>
#include <utils/Logger.h>
#include <memory/Heap.h>
#include <process/Process.h>
#include <process/Scheduler.h>
#include <module/ModuleManager.h>

extern void _start (boot_info_T* boot_info) {
    log(LOGGING_INFO, "!===== KERNEL STARTED =====!");

    initialize_kernel(boot_info);
//    initialize_modules();

    log(LOGGING_NONE_PRINT, "_______  _____  _______ _______  _____  _______");
    log(LOGGING_NONE_PRINT, "|       |     | |______ |  |  | |     | |______");
    log(LOGGING_NONE_PRINT, "|_____  |_____| ______| |  |  | |_____| _,_____|\n");

    int                 proc    = load_process_from_file("./test.elf", PROCESS_APPLICATION, "Test 0");

    start_process       (g_scheduler, proc);
    enable_scheduler    (g_scheduler);


//    paging_page_table_T* testmap = new_page_map();


//    for (int i = 0; i < 100000; i++) {
//        map_memory(testmap, PTF_READ_WRITE, 0xFFFFFF, 0xACAB);
//    }
//    clone_page_map(new_page_map(), testmap);

    log(LOGGING_DEBUG, "Reached end of Kernel");
    while(true) {
        asm("hlt");
    }
}