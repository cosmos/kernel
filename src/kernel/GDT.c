//
// Created by antifallobst on 28/07/2022.
//

#include <kernel/GDT.h>

__attribute__((aligned(0x1000)))
GDT_T default_GDT = {
        {0, 0, 0, 0x00, 0x00, 0},
        {0, 0, 0, 0x9A, 0xA0, 0},
        {0, 0, 0, 0x92, 0xA0, 0},
        {0, 0, 0, 0x00, 0x00, 0},
        {0, 0, 0, 0x9A, 0xA0, 0},
        {0, 0, 0, 0x92, 0xA0, 0}
};