//
// Created by antifallobst on 10/22/22.
//

#include <kernel/Permission.h>

// check if a thread is allowed to access a path (in the given mode)
bool permission_path_access(thread_T* thread, const char* path, FileModes_E mode) {
    return true;
}

bool permission_ipc_connect(thread_T* server, thread_T* client, ipc_connection_mode_E mode){
    // check if procs exist and if the client has the permission to talk to the server
    return true;
}