//
// Created by antifallobst on 28/07/2022.
//

#include <kernel/Kernel.h>
#include <kernel/GDT.h>

#include <memory/Memory.h>
#include <memory/Heap.h>
#include <memory/paging/Paging.h>
#include <memory/paging/PageFrame.h>
#include <memory/paging/PageMap.h>

#include <interrupts/Interrupts.h>
#include <interrupts/IDT.h>
#include <interrupts/PIC.h>

#include <drivers/info/cpu.h>
#include <drivers/time/PIT.h>
#include <drivers/PCI.h>
#include <drivers/File.h>

#include <process/Scheduler.h>
#include <process/elf/LibraryManager.h>

#include <utils/Logger.h>
#include <utils/Symbol.h>
#include <utils/StrUtil.h>
#include <utils/json/parser.h>
#include <utils/Panic.h>

#include <module/ModuleManager.h>

framebuffer_T*          g_framebuffer;
psf1_font_T*            g_font;
paging_page_table_T*    g_page_map;
FileSystems_E           g_file_system;
void*                   g_ramdisk;
symbols_T               g_symbols;
bool                    kernel_init     = false;


void initialize_kernel(boot_info_T* boot_info) {
    disable_interrupts();

    g_font          = boot_info->psf1_font;
    g_framebuffer   = boot_info->framebuffer;
    g_file_system   = FS_USTAR;
    g_ramdisk       = boot_info->ramdisk;

    // Clear the Screen
    memset(g_framebuffer->base_address, 0, g_framebuffer->buffer_size);


    log(LOGGING_SUCCESS, "!===== Initializing Kernel =====!");

    // <================================[ General Descriptor Table ]================================>
    GDTDescriptor_T         gdtDescriptor;
    gdtDescriptor.size      = sizeof(GDT_T) - 1;
    gdtDescriptor.offset    = (uint64_t)&default_GDT;

    load_GDT(&gdtDescriptor);
    log(LOGGING_SUCCESS, "Loaded GDT");


    // <=========================================[ Paging ]=========================================>
    read_EFI_memory_map(boot_info->memory_map, boot_info->memory_map_size, boot_info->memory_map_descriptor_size);
    uint64_t    memory_map_entries  = boot_info->memory_map_size / boot_info->memory_map_descriptor_size;
    uint64_t    memory_size         = get_memory_size(boot_info->memory_map, memory_map_entries, boot_info->memory_map_descriptor_size);

    uint64_t    kernel_size         = (uint64_t)&_kernel_end - (uint64_t)&_kernel_start;
    uint64_t    kernel_pages        = (uint64_t)kernel_size / PAGE_SIZE + 1;
    lock_pages(&_kernel_start, kernel_pages);
    log(LOGGING_SUCCESS, "Locked Kernel Pages");

    g_page_map = new_page_map();

    for (uint64_t i = 0; i < memory_size; i += PAGE_SIZE) {
        map_memory(g_page_map, PTF_READ_WRITE, (void*)i, (void*)i);
    }

    uint64_t framebuffer_base = (uint64_t)boot_info->framebuffer->base_address;
    uint64_t framebuffer_size = (uint64_t)boot_info->framebuffer->buffer_size + PAGE_SIZE;
    lock_pages((void*)framebuffer_base, framebuffer_size / PAGE_SIZE + 1);

    for (uint64_t i = framebuffer_base; i < framebuffer_base + framebuffer_size; i += PAGE_SIZE) {
        map_memory(g_page_map, PTF_READ_WRITE, (void*)i, (void*)i);
    }

    load_page_map(g_page_map);
    log(LOGGING_SUCCESS, "Loaded PML4 into CR3 register");


    // <==========================================[ Heap ]==========================================>
    initialize_heap((void*)0x00000000100000000000, 0x10);
    log(LOGGING_SUCCESS, "Initialized Heap");

    // <========================================[ Symbols ]=========================================>
    file_T      f           = open_file("./kernel_symbols.map", FILE_READ);
    if (f.handler == NULL) {
        panic("Cannot open Kernel Symbol map", NULL);
    }

    uint8_t*    buffer      = malloc(f.header.size);
    read_file(&f, 0, buffer, f.header.size);

    size_t      size        = string_to_hex_x64((char*)buffer);
    g_symbols.funcs         = malloc(size * sizeof(symbol_T));
    g_symbols.vars          = NULL;
    g_symbols.num_vars      = 0;
    int         index       = 0;

    for (int i = 0; i < size; i++) {
        g_symbols.funcs[i].address  = string_to_hex_x64((char*)&buffer[index]);
        index                      += (int)strlen((char*)&buffer[index]) + 5;
        g_symbols.funcs[i].name     = malloc(strlen((char*)&buffer[index]) + 1);
        memcpy(g_symbols.funcs[i].name, (char*)&buffer[index], strlen((char*)&buffer[index]));
        g_symbols.funcs[i].name[(int)strlen((char*)&buffer[index])] = 0;
        index                      += (int)strlen((char*)&buffer[index]) + 2;
        g_symbols.num_funcs++;
    }


    // <=======================================[ Interrupts ]=======================================>
    init_idt_register();

    set_idt_gate((void*)interrupt_stub_0x00, 0x00, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x01, 0x01, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x02, 0x02, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x03, 0x03, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x04, 0x04, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x05, 0x05, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x06, 0x06, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x07, 0x07, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x08, 0x08, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x09, 0x09, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0A, 0x0A, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0B, 0x0B, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0C, 0x0C, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0D, 0x0D, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0E, 0x0E, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x0F, 0x0F, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x10, 0x10, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x11, 0x11, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x12, 0x12, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x13, 0x13, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x14, 0x14, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x15, 0x15, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x16, 0x16, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x17, 0x17, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x18, 0x18, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x19, 0x19, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1A, 0x1A, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1B, 0x1B, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1C, 0x1C, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1D, 0x1D, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1E, 0x1E, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x1F, 0x1F, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);

    set_idt_gate((void*)interrupt_stub_0x20, 0x20, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x80, 0x80, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);
    set_idt_gate((void*)interrupt_stub_0x81, 0x81, IDT_TYPE_ATTRIBUTE_INTERRUPT_GATE, 0x08);

    log(LOGGING_SUCCESS, "Interrupt gates set");

    load_idt_register();
    log(LOGGING_SUCCESS, "IDT register loaded");

    // <=======================================[ ACPI ]=======================================>
    acpi_sdt_header_T*      xsdt    = (acpi_sdt_header_T*)  boot_info->rsdp->xsdt_address;
    acpi_mcfg_header_T*     mcfg    = (acpi_mcfg_header_T*) acpi_find_table(xsdt, "MCFG");

    pci_enumerate_pci(mcfg);

    log(LOGGING_SUCCESS, "ACPI prepared");

    // <=======================================[ Scheduler ]=======================================>
    init_scheduler(g_scheduler);
    log(LOGGING_SUCCESS, "Scheduler initialized");

    // <=======================================[ PIC ]=======================================>
    pic_unmask_IRQ  (PIC_IRQ_PIT);
    pic_mask_IRQ    (PIC_IRQ_CASCADE);

    // <=======================================[ Programmable Interval Timer ]=======================================>
    pit_set_divisor(PIT_DIVISOR);
    log(LOGGING_SUCCESS, "PIT divisor set");

    // <=======================================[ CPU Info ]=======================================>
    cpu_get_information(g_cpu_info);

    // <=======================================[ Shared Libraries ]=======================================>
    library_manager_init(g_lib_manager);
    library_manager_load_library_from_file(g_lib_manager, "./libcosmos.so", "libcosmos.so");


    log(LOGGING_SUCCESS, "!===== Kernel  Initialized =====!\n");
    kernel_init = true;

    enable_interrupts();
}

void initialize_modules() {
    log(LOGGING_SUCCESS, "!===== Initializing Modules =====!");

    g_mod_manager->num_modules = MOD_END_OF_ENUM;
    mod_manager_init(g_mod_manager);

    file_T  f_bootmods  = open_file("./mods/bootmods.json", FILE_READ);
    size_t  f_size      = f_bootmods.header.size;
    void*   buffer      = malloc(f_size);
    read_file(&f_bootmods, 0, buffer, f_size);

    json_ast_node_T master_node = json_parse_json_string(buffer, f_size);

    mod_manager_load_mods(g_mod_manager, &master_node);

    json_ast_clear(&master_node);
    free(buffer);

    log(LOGGING_SUCCESS, "!===== Modules  Initialized =====!");
}

bool kernel_initialized() {
    return kernel_init;
}