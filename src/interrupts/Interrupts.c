//
// Created by antifallobst on 15/08/2022.
//

#include <interrupts/Interrupts.h>
#include <interrupts/IDT.h>
#include <interrupts/PIC.h>
#include <utils/Panic.h>
#include <utils/Logger.h>
#include <memory/paging/PageFrame.h>
#include <memory/Memory.h>
#include <drivers/time/PIT.h>
#include <drivers/cpu.h>
#include <process/Scheduler.h>
#include <syscalls/syscall.h>
#include <kernel/Kernel.h>

IDTRegister_T   idt_register;
int             interrupts_disabled_status  = 0;

void exception_handler(cpu_state_T* state) {
    switch(state->interrupt_id) {
        case 0x00: {
            panic("Divide by Zero Error", state);
        }
        case 0x01: {
            log(LOGGING_DEBUG, "Interrupt Handler -> Debug interrupt 0x01 thrown");
            break;
        }
        case 0x02: {
            panic("Non maskable Interrupt", state);
        }
        case 0x03: {
            panic("Breakpoint interrupt handlers are not implemented", state);
        }
        case 0x04: {
            panic("Overflow interrupt handlers are not implemented", state);
        }
        case 0x05: {
            panic("Bound Range exceeded", state);
        }
        case 0x06: {
            panic("Invalid Opcode", state);
        }
        case 0x07: {
            panic("Device not available", state);
        }
        case 0x08: {
            panic("Double Fault", state);
        }
        case 0x09: {
            panic("Outdated Fault (was 'Coprocessor Segment Overrun')", state);
        }
        case 0x0A: {
            panic("Invalid TSS", state);
        }
        case 0x0B: {
            panic("Segment not present", state);
        }
        case 0x0C: {
            panic("Stack Segment Fault", state);
        }
        case 0x0D: {
            panic("General Protection Fault", state);
        }
        case 0x0E: {
            panic("Page Fault", state);
        }
        case 0x10: {
            panic("x87 Floating Point Exception", state);
        }
        case 0x11: {
            panic("Alignment check", state);
        }
        case 0x12: {
            panic("Machine Check", state);
        }
        case 0x13: {
            panic("SIMD Floating Point Exception", state);
        }
        case 0x14: {
            panic(" Virtualization Exception", state);
        }
        case 0x15: {
            panic("Control Protection Fault", state);
        }
        case 0x1C: {
            panic("Hypervisor Injection Exception", state);
        }
        case 0x1D: {
            panic("VMM Communication Exception", state);
        }
        case 0x1E: {
            panic("Security Exception", state);
        }
        default: {
            panic("Reserved / Unknown Fault", state);
        }
    }
}

cpu_state_T* interrupt_handler(cpu_state_T* state) {
    cpu_state_T* ret = state;

    if (state->interrupt_id < 32) {
        exception_handler(state);
    } else {
        switch (state->interrupt_id) {
            case 0x20: {
                pit_tick(state);

                cpu_state_T* new_state = switch_thread(g_scheduler, state);

                if (new_state != NULL) {
                    ret = new_state;
                }
                pic_end_master();
                break;
            }
            case 0x80: {
                ret = syscall_handle(state);
                pic_end_master();
                break;
            }
            case 0x81: {
                if (state->rip > (uint64_t)&_kernel_start && state->rip < (uint64_t)&_kernel_end) {
                    memcpy(&g_kernel_state, state, sizeof(cpu_state_T));
                    g_scheduler->enabled = true;
                    cpu_state_T* new_state = switch_thread(g_scheduler, state);

                    if (new_state != NULL) {
                        ret = new_state;
                    }
                }
                break;
            }
        }
    }

    return ret;
}

void set_idt_gate(void* handler, uint8_t entry_offset, uint8_t type_attribute, uint8_t selector) {
    IDTDescriptorEntry_T* interrupt = (IDTDescriptorEntry_T*)(idt_register.offset + entry_offset * sizeof(IDTDescriptorEntry_T));

    set_idt_descriptor_entry_offset(interrupt, (uint64_t)handler);
    interrupt->type_attribute   = type_attribute;
    interrupt->selector         = selector;
}

void init_idt_register () {
    idt_register.limit      = 0x0FFF;
    idt_register.offset     = (uint64_t)request_page();
}

void load_idt_register () {
    asm("lidt %0" : : "m" (idt_register));

    pic_remap_pic();
}


void disable_interrupts () {
    if (interrupts_disabled_status > 0) {
        interrupts_disabled_status++;
        return;
    } else {
        asm("cli");
    }
}

void enable_interrupts () {
    if (interrupts_disabled_status > 0) {
        interrupts_disabled_status--;
    } else {
        asm("sti");
    }
}