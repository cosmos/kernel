//
// Created by antifallobst on 9/11/22.
//

#include <stdint.h>
#include <interrupts/PIC.h>
#include <utils/IO.h>

void pic_mask_IRQ(pic_irq_type_E irq) {
    uint16_t    port;
    uint8_t     value;

    if(irq < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        irq -= 8;
    }
    value = io_in_x8(port) | (1 << irq);
    io_out_x8(port, value);
}

void pic_unmask_IRQ(pic_irq_type_E irq) {
    uint16_t    port;
    uint8_t     value;

    if(irq < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        irq -= 8;
    }
    value = io_in_x8(port) & ~(1 << irq);
    io_out_x8(port, value);
}


void pic_end_master() {
    io_out_x8(PIC1_COMMAND, PIC_EOI);
}

void pic_end_slave() {
    io_out_x8(PIC2_COMMAND, PIC_EOI);
    io_out_x8(PIC1_COMMAND, PIC_EOI);
}

void pic_remap_pic() {
    uint8_t a1;
    uint8_t a2;

    a1 = io_in_x8(PIC1_DATA);
    io_wait();
    a2 = io_in_x8(PIC2_DATA);
    io_wait();

    io_out_x8(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
    io_wait();
    io_out_x8(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
    io_wait();

    io_out_x8(PIC1_DATA, 0x20);
    io_wait();
    io_out_x8(PIC2_DATA, 0x28);
    io_wait();

    io_out_x8(PIC1_DATA, 4);
    io_wait();
    io_out_x8(PIC2_DATA, 2);
    io_wait();

    io_out_x8(PIC1_DATA, ICW4_8086);
    io_wait();
    io_out_x8(PIC2_DATA, ICW4_8086);
    io_wait();

    io_out_x8(PIC1_DATA, a1);
    io_out_x8(PIC2_DATA, a2);
}