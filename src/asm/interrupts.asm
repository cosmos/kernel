extern interrupt_handler

interrupt_common_handler:
    ; save CPU state
    push    rbp
    push    rdi
    push    rsi
    push    rdx
    push    rcx
    push    rbx
    push    rax

    ; save current page map
    mov     rax, cr3
    push    rax

    ; load kernel page map
;    mov     rax, 0x0000000000100000
;    mov     cr3, rax

    ; call handler
    mov     rdi, rsp
    call    interrupt_handler
    mov     rsp, rax

    ; load cpu_state
    pop     rax
    mov     cr3, rax

    ; restore CPU state
    pop     rax
    pop     rbx
    pop     rcx
    pop     rdx
    pop     rsi
    pop     rdi
    pop     rbp

    ; remove error_code and interrupt number from stack
    add     rsp, 16

    iretq
GLOBAL interrupt_common_handler


%macro interrupt_stub 1
    interrupt_stub_%1:
        push qword    0
        push qword   %1
        jmp interrupt_common_handler
    GLOBAL interrupt_stub_%1
    %endmacro

%macro interrupt_stub_error_code 1
    interrupt_stub_%1:
        push qword   %1
        jmp interrupt_common_handler
    GLOBAL interrupt_stub_%1
    %endmacro

; Faults
interrupt_stub              0x00
interrupt_stub              0x01
interrupt_stub              0x02
interrupt_stub              0x03
interrupt_stub              0x04
interrupt_stub              0x05
interrupt_stub              0x06
interrupt_stub              0x07
interrupt_stub_error_code   0x08
interrupt_stub              0x09
interrupt_stub_error_code   0x0A
interrupt_stub_error_code   0x0B
interrupt_stub_error_code   0x0C
interrupt_stub_error_code   0x0D
interrupt_stub_error_code   0x0E
interrupt_stub              0x0F
interrupt_stub              0x10
interrupt_stub_error_code   0x11
interrupt_stub              0x12
interrupt_stub              0x13
interrupt_stub              0x14
interrupt_stub              0x15
interrupt_stub              0x16
interrupt_stub              0x17
interrupt_stub              0x18
interrupt_stub              0x19
interrupt_stub              0x1A
interrupt_stub              0x1B
interrupt_stub              0x1C
interrupt_stub              0x1D
interrupt_stub              0x1E
interrupt_stub              0x1F

; IRQs
interrupt_stub              0x20
;interrupt_stub              0x21
;interrupt_stub              0x22
;interrupt_stub              0x23
;interrupt_stub              0x24
;interrupt_stub              0x25
;interrupt_stub              0x26
;interrupt_stub              0x27
;interrupt_stub              0x28
;interrupt_stub              0x29
;interrupt_stub              0x2A
;interrupt_stub              0x2B
;interrupt_stub              0x2C
;interrupt_stub              0x2D
;interrupt_stub              0x2E
;interrupt_stub              0x2F
;interrupt_stub              0x30
;interrupt_stub              0x31
;interrupt_stub              0x32
;interrupt_stub              0x33
;interrupt_stub              0x34
;interrupt_stub              0x35
;interrupt_stub              0x36
;interrupt_stub              0x37
;interrupt_stub              0x38
;interrupt_stub              0x39
;interrupt_stub              0x3A
;interrupt_stub              0x3B
;interrupt_stub              0x3C
;interrupt_stub              0x3D
;interrupt_stub              0x3E
;interrupt_stub              0x3F
;interrupt_stub              0x40
;interrupt_stub              0x41
;interrupt_stub              0x42
;interrupt_stub              0x43
;interrupt_stub              0x44
;interrupt_stub              0x45
;interrupt_stub              0x46
;interrupt_stub              0x47
;interrupt_stub              0x48
;interrupt_stub              0x49
;interrupt_stub              0x4A
;interrupt_stub              0x4B
;interrupt_stub              0x4C
;interrupt_stub              0x4D
;interrupt_stub              0x4E
;interrupt_stub              0x4F
;interrupt_stub              0x50
;interrupt_stub              0x51
;interrupt_stub              0x52
;interrupt_stub              0x53
;interrupt_stub              0x54
;interrupt_stub              0x55
;interrupt_stub              0x56
;interrupt_stub              0x57
;interrupt_stub              0x58
;interrupt_stub              0x59
;interrupt_stub              0x5A
;interrupt_stub              0x5B
;interrupt_stub              0x5C
;interrupt_stub              0x5D
;interrupt_stub              0x5E
;interrupt_stub              0x5F
;interrupt_stub              0x60
;interrupt_stub              0x61
;interrupt_stub              0x62
;interrupt_stub              0x63
;interrupt_stub              0x64
;interrupt_stub              0x65
;interrupt_stub              0x66
;interrupt_stub              0x67
;interrupt_stub              0x68
;interrupt_stub              0x69
;interrupt_stub              0x6A
;interrupt_stub              0x6B
;interrupt_stub              0x6C
;interrupt_stub              0x6D
;interrupt_stub              0x6E
;interrupt_stub              0x6F
;interrupt_stub              0x70
;interrupt_stub              0x71
;interrupt_stub              0x72
;interrupt_stub              0x73
;interrupt_stub              0x74
;interrupt_stub              0x75
;interrupt_stub              0x76
;interrupt_stub              0x77
;interrupt_stub              0x78
;interrupt_stub              0x79
;interrupt_stub              0x7A
;interrupt_stub              0x7B
;interrupt_stub              0x7C
;interrupt_stub              0x7D
;interrupt_stub              0x7E
;interrupt_stub              0x7F
interrupt_stub              0x80
interrupt_stub              0x81
;interrupt_stub              0x82
;interrupt_stub              0x83
;interrupt_stub              0x84
;interrupt_stub              0x85
;interrupt_stub              0x86
;interrupt_stub              0x87
;interrupt_stub              0x88
;interrupt_stub              0x89
;interrupt_stub              0x8A
;interrupt_stub              0x8B
;interrupt_stub              0x8C
;interrupt_stub              0x8D
;interrupt_stub              0x8E
;interrupt_stub              0x8F
;interrupt_stub              0x90
;interrupt_stub              0x91
;interrupt_stub              0x92
;interrupt_stub              0x93
;interrupt_stub              0x94
;interrupt_stub              0x95
;interrupt_stub              0x96
;interrupt_stub              0x97
;interrupt_stub              0x98
;interrupt_stub              0x99
;interrupt_stub              0x9A
;interrupt_stub              0x9B
;interrupt_stub              0x9C
;interrupt_stub              0x9D
;interrupt_stub              0x9E
;interrupt_stub              0x9F
;interrupt_stub              0xA0
;interrupt_stub              0xA1
;interrupt_stub              0xA2
;interrupt_stub              0xA3
;interrupt_stub              0xA4
;interrupt_stub              0xA5
;interrupt_stub              0xA6
;interrupt_stub              0xA7
;interrupt_stub              0xA8
;interrupt_stub              0xA9
;interrupt_stub              0xAA
;interrupt_stub              0xAB
;interrupt_stub              0xAC
;interrupt_stub              0xAD
;interrupt_stub              0xAE
;interrupt_stub              0xAF
;interrupt_stub              0xB0
;interrupt_stub              0xB1
;interrupt_stub              0xB2
;interrupt_stub              0xB3
;interrupt_stub              0xB4
;interrupt_stub              0xB5
;interrupt_stub              0xB6
;interrupt_stub              0xB7
;interrupt_stub              0xB8
;interrupt_stub              0xB9
;interrupt_stub              0xBA
;interrupt_stub              0xBB
;interrupt_stub              0xBC
;interrupt_stub              0xBD
;interrupt_stub              0xBE
;interrupt_stub              0xBF
;interrupt_stub              0xC0
;interrupt_stub              0xC1
;interrupt_stub              0xC2
;interrupt_stub              0xC3
;interrupt_stub              0xC4
;interrupt_stub              0xC5
;interrupt_stub              0xC6
;interrupt_stub              0xC7
;interrupt_stub              0xC8
;interrupt_stub              0xC9
;interrupt_stub              0xCA
;interrupt_stub              0xCB
;interrupt_stub              0xCC
;interrupt_stub              0xCD
;interrupt_stub              0xCE
;interrupt_stub              0xCF
;interrupt_stub              0xD0
;interrupt_stub              0xD1
;interrupt_stub              0xD2
;interrupt_stub              0xD3
;interrupt_stub              0xD4
;interrupt_stub              0xD5
;interrupt_stub              0xD6
;interrupt_stub              0xD7
;interrupt_stub              0xD8
;interrupt_stub              0xD9
;interrupt_stub              0xDA
;interrupt_stub              0xDB
;interrupt_stub              0xDC
;interrupt_stub              0xDD
;interrupt_stub              0xDE
;interrupt_stub              0xDF
;interrupt_stub              0xE0
;interrupt_stub              0xE1
;interrupt_stub              0xE2
;interrupt_stub              0xE3
;interrupt_stub              0xE4
;interrupt_stub              0xE5
;interrupt_stub              0xE6
;interrupt_stub              0xE7
;interrupt_stub              0xE8
;interrupt_stub              0xE9
;interrupt_stub              0xEA
;interrupt_stub              0xEB
;interrupt_stub              0xEC
;interrupt_stub              0xED
;interrupt_stub              0xEE
;interrupt_stub              0xEF
;interrupt_stub              0xF0
;interrupt_stub              0xF1
;interrupt_stub              0xF2
;interrupt_stub              0xF3
;interrupt_stub              0xF4
;interrupt_stub              0xF5
;interrupt_stub              0xF6
;interrupt_stub              0xF7
;interrupt_stub              0xF8
;interrupt_stub              0xF9
;interrupt_stub              0xFA
;interrupt_stub              0xFB
;interrupt_stub              0xFC
;interrupt_stub              0xFD
;interrupt_stub              0xFE
;interrupt_stub              0xFF