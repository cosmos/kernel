//
// Created by antifallobst on 10/22/22.
//

#include <process/ProcessFileManager.h>
#include <memory/Heap.h>
#include <memory/Memory.h>
#include <utils/Math.h>
#include <syscalls/syscall.h>

void process_filemgr_init_chunk(process_filemgr_chunk_T* chunk, uint64_t size) {
    chunk->size         = size;
    chunk->free_slots   = size;
    chunk->files        = malloc(size * sizeof(file_T));
    chunk->next         = NULL;
    init_bitmap(&chunk->files_bmp, malloc((ceil_to(size, 8) / 8)), size);
}

void process_filemgr_init_cache(process_filemgr_cache_T* cache, uint64_t size) {
    cache->size         = size;
    cache->position     = 0;
    cache->files        = malloc(size * sizeof(file_T*));
    cache->descriptors  = malloc(size * sizeof(uint64_t));
}


void process_filemgr_init(process_filemgr_T* pfmgr) {
    pfmgr->num_files    = 0;
    pfmgr->chunk        = NULL;

    process_filemgr_init_cache(&pfmgr->cache, PROCESS_FILEMGR_CACHE_SIZE);
}

void process_filemgr_cache_add_file(process_filemgr_cache_T* cache, file_T* file, uint64_t file_descriptor) {
    if (cache->position >= cache->size) {
        cache->position = 0;
    }

    cache->files[cache->position]           = file;
    cache->descriptors[cache->position]     = file_descriptor;
    cache->position                        += 1;
}

void process_filemgr_cache_remove_file(process_filemgr_cache_T* cache, uint64_t file_descriptor) {
    for (int i = 0; i < cache->size; i++) {
        if (cache->descriptors[i] == file_descriptor) {
            cache->files[cache->position]       =  NULL;
            cache->descriptors[cache->position] = -1;
        }
    }
}

file_T* process_filemgr_cache_get_file(process_filemgr_cache_T* cache, uint64_t file_descriptor) {
    for (int i = 0; i < cache->size; i++) {
        if (cache->descriptors[i] == file_descriptor) {
            return cache->files[i];
        }
    }
    return NULL;
}

uint64_t process_filemgr_add_file(process_filemgr_T* pfmgr, const char* path, FileModes_E mode) {
    process_filemgr_chunk_T**   chunk = &pfmgr->chunk;
    uint64_t                    index = 0;

    while (*chunk != NULL) {
        if ((*chunk)->free_slots > 0) {
            for (int i = 0; i < (*chunk)->size; i++) {
                if (!get_bitmap_at_index(&(*chunk)->files_bmp, i)) {
                    (*chunk)->files[i]    = open_file(path, mode);
                    (*chunk)->free_slots -= 1;
                    set_bitmap_at_index(&(*chunk)->files_bmp, i, true);
                    process_filemgr_cache_add_file(&pfmgr->cache, &(*chunk)->files[i], index + i);
                    return index + i + FILE_DESC_END;
                }
            }
        }
        index +=                             (*chunk)->size;
        chunk  = (process_filemgr_chunk_T**)&(*chunk)->next;
    }

    (*chunk) = malloc(sizeof(process_filemgr_chunk_T));
    process_filemgr_init_chunk((*chunk), PROCESS_FILEMGR_CHUNK_SIZE);

    (*chunk)->files[0] = open_file(path, mode);
    set_bitmap_at_index(&(*chunk)->files_bmp, 0, true);
    process_filemgr_cache_add_file(&pfmgr->cache, &(*chunk)->files[0], index);

    return index + FILE_DESC_END;
}

file_T* process_filemgr_get_file(process_filemgr_T* pfmgr, uint64_t file_descriptor) {
    process_filemgr_chunk_T*    chunk   = pfmgr->chunk;
    uint64_t                    index   = 0;
    file_descriptor                    -= FILE_DESC_END;

    file_T* cached_file = process_filemgr_cache_get_file(&pfmgr->cache, file_descriptor);
    if (cached_file != NULL) {
        return cached_file;
    }

    while ((index + chunk->size) < file_descriptor) {
        if (chunk->next == NULL) {
            return NULL;
        }
        index +=                           chunk->size;
        chunk  = (process_filemgr_chunk_T*)chunk->next;
    }
    uint64_t file_selector = file_descriptor - index;

    if (!get_bitmap_at_index(&chunk->files_bmp, file_selector)) {
        return NULL;
    }

    process_filemgr_cache_add_file(&pfmgr->cache, &chunk->files[file_selector], file_descriptor);

    return &chunk->files[file_selector];
}

bool process_filemgr_remove_file(process_filemgr_T* pfmgr, uint64_t file_descriptor) {
    process_filemgr_chunk_T*    chunk   = pfmgr->chunk;
    uint64_t                    index   = 0;
    file_descriptor                    -= FILE_DESC_END;

    while (index + chunk->size < file_descriptor) {
        if (chunk->next == NULL) {
            return false;
        }
        index +=                           chunk->size;
        chunk  = (process_filemgr_chunk_T*)chunk->next;
    }
    uint64_t file_selector = file_descriptor - index;

    if (get_bitmap_at_index(&chunk->files_bmp, file_selector)) {
        set_bitmap_at_index(&chunk->files_bmp, file_selector, false);
        memset(&chunk->files[file_selector], 0, sizeof(file_T));
        process_filemgr_cache_remove_file(&pfmgr->cache, file_descriptor);
        chunk->free_slots += 1;
        return true;
    }

    return false;
}