//
// Created by antifallobst on 12/13/22.
//

#include <process/Thread.h>
#include <process/Scheduler.h>
#include <memory/Heap.h>
#include <utils/Logger.h>
#include <syscalls/syscall.h>

char* thread_type_strings[3] = {
        "Master ",
        "Normal ",
        "ModCall"
};

void thread_end_reached() {
    g_scheduler->allow_switching = false;
    thread_T* thread = g_scheduler->threads[g_scheduler->current_tid];

//    log(LOGGING_INFO, "Thread[%d] Reached its end", thread->g_tid);

    switch (thread->type) {
        case THREAD_MODULE_CALL: {
            start_thread(g_scheduler, thread->caller_tid);
        }
        case THREAD_NORMAL: {
            if (thread->ret_val_ptr != NULL) {
                *thread->ret_val_ptr = thread->cpu_state.rax;
            }
            kill_thread(g_scheduler, thread->g_tid);
            g_scheduler->allow_switching = true;
            asm("int $0x20");
            break;
        }
        case THREAD_MASTER: {
            switch (g_scheduler->processes[thread->pid]->type) {
                case PROCESS_APPLICATION: {
                    perform_syscall(SYSCALL_MISC_EXIT, thread->cpu_state.rax, 0, 0, 0);
                    break;
                }
                case PROCESS_MODULE: {
                    kill_thread(g_scheduler, thread->g_tid);
                    g_scheduler->allow_switching = true;
                    asm("int $0x20");
                    break;
                }
            }
            break;
        }
    }
}

// A thread can have a maximum of 4 arguments, all others will be ignored
bool init_thread(thread_T* thread, uint32_t pid, symbol_T* start_symbol, thread_type_E type, uint32_t argc, uint64_t* argv) {
    process_T* process  = g_scheduler->processes[pid];
    thread->pid         = pid;
    thread->type        = type;
    thread->ret_val_ptr = NULL;
    thread->l_tid       = process_block_thread_lid(process);
    if (thread->l_tid == 0) {
        return false;
    }
    thread->stack       = (uint64_t)malloc(STACK_SIZE * PAGE_SIZE) + (STACK_SIZE * PAGE_SIZE);    // TODO: change     <- no need to thank me

    *(uint64_t*)(thread->stack-8) = (uint64_t)&thread_end_reached;

    thread->cpu_time    = 0;
    thread->cpu_state   = (cpu_state_T){
        (uint64_t)process->page_map,
        0,
        0,
        (argc > 3 ? argv[3] : 0),
        (argc > 2 ? argv[2] : 0),
        (argc > 1 ? argv[1] : 0),
        (argc > 0 ? argv[0] : 0),
        thread->stack - 8,

        0,
        0,

        process->executable.load_address + start_symbol->address,
        0x08,
        0,
        thread->stack - 8,
        0
    };

    return true;
}

void log_threads() {
    log(LOGGING_NONE_PRINT, "System Threads:");
    log(LOGGING_NONE_PRINT, "   <ID> <Status>   <Type>     <PID> <Process Name>");

    for (int i = 0; i < MAX_THREADS; i++) {
        if (get_bitmap_at_index(&g_scheduler->threads_bitmap, i)) {
            thread_T* thread = g_scheduler->threads[i];
            log(LOGGING_NONE_PRINT, "    %d    %s    %s    %d     %s",
                i, get_bitmap_at_index(&g_scheduler->running_threads_bitmap, i) ? "Running" : "Paused ",
                thread_type_strings[thread->type], thread->pid, g_scheduler->processes[thread->pid]->name);
        }
    }

//    log(LOGGING_NONE_PRINT, "Scheduling queue:");
//    if (g_scheduler->queue_length == 0) {
//        log(LOGGING_NONE_PRINT, "    Queue is empty");
//    }
//    for (int i = 0; i < g_scheduler->queue_length; i++) {
//        log(LOGGING_NONE_PRINT, "  %c %d\n",
//            ((i == g_scheduler->queue_index-1) ? '>' : ' '), g_scheduler->queue[i]->g_tid);
//    }
    log(LOGGING_NONE_PRINT, "");
}