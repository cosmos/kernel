//
// Created by antifallobst on 10/4/22.
//

#include <process/IPC.h>
#include <process/Process.h>
#include <process/Scheduler.h>
#include <utils/Logger.h>
#include <utils/Math.h>
#include <memory/Memory.h>
#include <memory/Heap.h>

void ipc_init_connection(ipc_connection_T* connection, uint64_t descriptor, uint64_t pid, ipc_connection_mode_E mode) {
//    log(LOGGING_DEBUG, "IPC -> Initializing connection  PID[%d]  MODE[%d]", pid, mode);
    uint64_t address                    = (uint64_t)malloc(IPC_STACK_SIZE);
    connection->stack.bottom            = address;
    connection->stack.top               = address + IPC_STACK_SIZE;
    connection->stack.top_message       = (ipc_message_T*)connection->stack.top - sizeof(ipc_message_T);
    connection->stack.bottom_message    = connection->stack.top_message;
    connection->stack.num_messages      = 0;

    connection->descriptor              = descriptor;
    connection->server_pid              = pid;
    connection->mode                    = mode;
    connection->link                    = NULL;
}

void ipc_manager_init_chunk(ipc_manager_chunk_T* chunk, uint64_t size) {
    chunk->size         = size;
    chunk->free_slots   = size;
    chunk->connections  = malloc(size * sizeof(file_T));
    chunk->next         = NULL;
    init_bitmap(&chunk->connections_bmp, malloc((ceil_to(size, 8) / 8)), size);
}

void ipc_manager_init_cache(ipc_manager_cache_T* cache, uint64_t size) {
    cache->size         = size;
    cache->position     = 0;
    cache->connections  = malloc(size * sizeof(file_T*));
    cache->descriptors  = malloc(size * sizeof(uint64_t));
}

void ipc_manager_init(ipc_manager_T* ipc_manager) {
    ipc_manager->num_files              = 0;
    ipc_manager->chunk                  = NULL;
    ipc_manager->waiting_for_connection = IPC_CONN_NOT_WAITING;
    ipc_manager_init_cache(&ipc_manager->cache, IPC_CACHE_SIZE);
}

void ipc_link_connections(ipc_connection_T* a, ipc_connection_T* b) {
    a->link = (struct ipc_connection_T*)b;
    b->link = (struct ipc_connection_T*)a;
}

void ipc_manager_cache_add_connection(ipc_manager_cache_T* cache, ipc_connection_T* connection, uint64_t connection_descriptor) {
    if (cache->position >= cache->size) {
        cache->position = 0;
    }

    cache->connections[cache->position]     = connection;
    cache->descriptors[cache->position]     = connection_descriptor;
    cache->position                        += 1;
}

void ipc_manager_cache_remove_connection(ipc_manager_cache_T* cache, uint64_t connection_descriptor) {
    for (int i = 0; i < cache->size; i++) {
        if (cache->descriptors[i] == connection_descriptor) {
            cache->connections[cache->position] =  NULL;
            cache->descriptors[cache->position] = -1;
        }
    }
}

ipc_connection_T* ipc_manager_cache_get_connection(ipc_manager_cache_T* cache, uint64_t connection_descriptor) {
    for (int i = 0; i < cache->size; i++) {
        if (cache->descriptors[i] == connection_descriptor) {
            return cache->connections[i];
        }
    }
    return NULL;
}

uint64_t ipc_manager_add_connection(ipc_manager_T* ipc_manager, uint64_t server_pid, ipc_connection_mode_E mode) {
    ipc_manager_chunk_T**   chunk = &ipc_manager->chunk;
    uint64_t                index = 0;

    while (*chunk != NULL) {
        if ((*chunk)->free_slots > 0) {
            for (int i = 0; i < (*chunk)->size; i++) {
                if (!get_bitmap_at_index(&(*chunk)->connections_bmp, i)) {
                    ipc_init_connection(&(*chunk)->connections[i], index + i + IPC_STD_DESC_END, server_pid, mode);
                    (*chunk)->free_slots       -= 1;
                    set_bitmap_at_index(&(*chunk)->connections_bmp, i, true);
                    ipc_manager_cache_add_connection(&ipc_manager->cache, &(*chunk)->connections[i], index + i + IPC_STD_DESC_END);
                    return index + i + IPC_STD_DESC_END;
                }
            }
        }
        index +=                         (*chunk)->size;
        chunk  = (ipc_manager_chunk_T**)&(*chunk)->next;
    }

    (*chunk) = malloc(sizeof(process_filemgr_chunk_T));
    ipc_manager_init_chunk((*chunk), IPC_CHUNK_SIZE);

    ipc_init_connection(&(*chunk)->connections[0], index + IPC_STD_DESC_END, server_pid, mode);
    set_bitmap_at_index(&(*chunk)->connections_bmp, 0, true);
    ipc_manager_cache_add_connection(&ipc_manager->cache, &(*chunk)->connections[0], index + IPC_STD_DESC_END);

    return index + IPC_STD_DESC_END;
}

ipc_connection_T* ipc_manager_get_connection(ipc_manager_T* ipc_manager, uint64_t connection_descriptor) {
    ipc_manager_chunk_T*    chunk   = ipc_manager->chunk;
    uint64_t                index   = 0;
    connection_descriptor          -= IPC_STD_DESC_END;

    ipc_connection_T* cached_connection = ipc_manager_cache_get_connection(&ipc_manager->cache, connection_descriptor);
    if (cached_connection != NULL) {
        return cached_connection;
    }

    while ((index + chunk->size) < connection_descriptor) {
        if (chunk->next == NULL) {
            return NULL;
        }
        index +=                       chunk->size;
        chunk  = (ipc_manager_chunk_T*)chunk->next;
    }
    uint64_t connection_selector = connection_descriptor - index;

    if (!get_bitmap_at_index(&chunk->connections_bmp, connection_selector)) {
        return NULL;
    }

    ipc_manager_cache_add_connection(&ipc_manager->cache, &chunk->connections[connection_selector],
                                     connection_descriptor);

    return &chunk->connections[connection_selector];
}

bool ipc_manager_remove_connection(ipc_manager_T* ipc_manager, uint64_t connection_descriptor) {
    ipc_manager_chunk_T*    chunk   = ipc_manager->chunk;
    uint64_t                index   = 0;
    connection_descriptor          -= IPC_STD_DESC_END;

    while ((index + chunk->size) < connection_descriptor) {
        if (chunk->next == NULL) {
            return NULL;
        }
        index +=                       chunk->size;
        chunk  = (ipc_manager_chunk_T*)chunk->next;
    }
    uint64_t connection_selector = connection_descriptor - index;

    if (get_bitmap_at_index(&chunk->connections_bmp, connection_selector)) {
        set_bitmap_at_index(&chunk->connections_bmp, connection_selector, false);
        memset(&chunk->connections[connection_selector], 0, sizeof(file_T));
        ipc_manager_cache_remove_connection(&ipc_manager->cache, connection_descriptor);
        chunk->free_slots += 1;

        ipc_connection_T* link = (ipc_connection_T*)&chunk->connections[connection_selector].link;
        ipc_manager_remove_connection(&g_scheduler->processes[chunk->connections[connection_selector].server_pid]->descriptors.ipc_manager, link->descriptor);

        return true;
    }

    return false;
}



status_t ipc_handle_send(uint64_t connection_descriptor, ipc_message_T* msg) {
    thread_T*           thread      = g_scheduler->threads[g_scheduler->current_tid];
    ipc_connection_T*   connection  = ipc_manager_get_connection(&g_scheduler->processes[thread->pid]->descriptors.ipc_manager, connection_descriptor);
    ipc_connection_T*   link        = (ipc_connection_T*)connection->link;
    if (connection->mode != IPC_CONN_SEND && connection->mode != IPC_CONN_BIDIRECTIONAL) {
        return STATUS_PERMISSION_DENIED;
    }
    switch (msg->type) {
        case IPC_MSG_MODCOM:
        case IPC_MSG_STATUS:
        case IPC_MSG_MESSAGE: {
//            log(LOGGING_DEBUG, "IPC -> Sending: %d >--[%s]--> %d", g_scheduler->current_pid, msg->buffer, connection->server_pid);

            msg->sender_tid = (int)g_scheduler->current_tid;

            if (ipc_push_message(&link->stack, msg) < 0) {
                return STATUS_ERROR;
            }
            if (!g_scheduler->processes[connection->server_pid]->descriptors.ipc_manager.waiting_for_connection) {
//                start_thread(g_scheduler, (int) connection->server_tid);
            }
            return STATUS_SUCCESS;
        }
        case IPC_MSG_SIGNAL: {
            log(LOGGING_WARNING, "IPC -> Sending Signal[%d] to Process[%d]", msg->buffer, connection->server_pid);
            log(LOGGING_WARNING, "IPC -> Sending Signals is not implemented yet");
            return STATUS_ERROR;
        }
    }
}

status_t ipc_handle_receive(uint64_t connection_descriptor, ipc_message_T* msg) {
//    process_T*          process     =  &g_scheduler->processes[g_scheduler->current_pid];
//    ipc_connection_T*   connection  =   ipc_manager_get_connection(&process->descriptors.ipc_manager, connection_descriptor);

//    log(LOGGING_DEBUG, "IPC -> Receive  DESC[%d]  MODE[%d]", connection_descriptor, connection->mode);

//    if (connection->mode != IPC_CONN_RECV && connection->mode != IPC_CONN_BIDIRECTIONAL) {
//        return STATUS_PERMISSION_DENIED;
//    }

//    process_T*          client      =  &g_scheduler->processes[g_scheduler->current_pid];
//    msg->sender_tid     = (int)g_scheduler->current_tid;

//    log(LOGGING_DEBUG, "IPC -> Popping PID %d message stack", g_scheduler->current_pid);
//    ipc_message_T* popped_msg = ipc_pop_message (&connection->stack);
//    if (popped_msg == NULL) {
//        pause_process(g_scheduler, (int)client->pid);
//        return STATUS_RESOURCE_NOT_AVAILABLE;
//    }

//    memcpy(msg, popped_msg, sizeof(ipc_message_T));

//    log(LOGGING_DEBUG, "IPC -> Received: %d <--[%s]--< %d", client->pid, msg->buffer, msg->sender_pid);

    return STATUS_SUCCESS;
}

status_t ipc_push_message(ipc_message_stack_T* stack, ipc_message_T* msg) {
    if ((uint64_t)stack->bottom_message - sizeof(ipc_message_T) < stack->bottom) {
        if ((uint64_t)stack->top_message + sizeof(ipc_message_T) >= stack->top) {
            log(LOGGING_WARNING, "IPC -> Failed to push message to stack, stack is full");
            return STATUS_ERROR;
        }
        stack->bottom_message  = (ipc_message_T*)(stack->top - sizeof(ipc_message_T));
    }

    memcpy(stack->bottom_message, msg, sizeof(ipc_message_T));
    stack->num_messages   ++;
    stack->bottom_message -= sizeof(ipc_message_T);
    return STATUS_SUCCESS;
}

ipc_message_T* ipc_pop_message(ipc_message_stack_T* stack) {
    if (stack->num_messages == 0) {
//        log(LOGGING_WARNING, "IPC -> No messages to pop!");
        return NULL;
    }
    if ((uint64_t)stack->top_message - sizeof(ipc_message_T) < stack->bottom) {
        stack->top_message = (ipc_message_T*)(stack->top - sizeof(ipc_message_T));
    }

    ipc_message_T* msg  = stack->top_message;
    stack->top_message -= sizeof(ipc_message_T);
    stack->num_messages--;

    return msg;
}