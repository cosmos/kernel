//
// Created by antifallobst on 8/17/22.
//

#include <process/Process.h>
#include <process/Scheduler.h>
#include <memory/paging/PageMap.h>
#include <memory/Memory.h>
#include <memory/Heap.h>
#include <utils/Math.h>
#include <utils/StrUtil.h>
#include <utils/Logger.h>
#include <drivers/File.h>

uint64_t g_process_space = 0x1000000000;

char* process_type_strings[3] = {
        "Module",
        "Module Call",
        "Application"
};

char* process_priorities_strings[2] = {
        "High",
        "Low"
};

int load_process_from_file(const char* path, process_type_E type, const char* name) {
    file_T file = open_file(path, FILE_READ);

    void* buffer = malloc(file.header.size);
    read_file(&file, 0, buffer, file.header.size);

    return load_process(buffer, file.header.size, type, name);
}

int load_process(void* exec_buffer, uint64_t exec_buffer_len, process_type_E type, const char* name) {
    paging_page_table_T* page_map = new_page_map();


    log(LOGGING_INFO, "Process -> Page Map initialized");
    return load_process_from_executable(elf_load_executable(exec_buffer, exec_buffer_len, (void*)g_process_space, page_map), type, page_map, name);
}

int load_process_from_executable(elf_executable_T executable, process_type_E type, paging_page_table_T* page_map, const char* name) {
    process_T proc;
    log(LOGGING_NONE, "");
    log(LOGGING_INFO,             "Process -> Loading Process");
    log(LOGGING_NONE, "                     > Name: '%s'", name);

    proc.cpu_time           = 0;
    proc.base_address       = g_process_space;
    proc.executable         = executable;
    proc.page_map           = page_map;
    proc.type               = type;

    memcpy(&proc.name, (void*)name, min(strlen(name), 128));
    proc.name[min(strlen(name), 128)] = '\0';

    init_bitmap(&proc.threads_bitmap, malloc(8), 8);    // 64 / 8
    proc.threads = malloc(MAX_THREADS_PER_PROC * sizeof(thread_T*));

    process_filemgr_init    (&proc.descriptors.file_manager);
    ipc_manager_init        (&proc.descriptors.ipc_manager);

    uint64_t mappings_end = proc.executable.mappings_end;

    mappings_end += PAGE_SIZE;
    log(LOGGING_INFO, "Process -> Mappings end at 0x%x64", mappings_end);

    uint32_t pid = register_process(g_scheduler, &proc);
    uint32_t master = process_new_thread(g_scheduler->processes[pid], THREAD_MASTER, proc.executable.image.entry_point, 0, NULL);
    g_scheduler->processes[pid]->master_thread = g_scheduler->threads[master];

    g_process_space += 0x1000000000;

    log(LOGGING_INFO, "Process -> Successfully loaded");

    return pid;
}

uint32_t process_new_thread(process_T* process, thread_type_E type, symbol_T* function, int argc, uint64_t* argv) {
    thread_T thread;
    init_thread(&thread, process->pid, function, type, argc, argv);
    uint32_t g_tid = register_thread(g_scheduler, &thread);
    thread_T* g_thread = g_scheduler->threads[g_tid];
    process->threads[g_thread->l_tid-1] = g_thread;
    return g_tid;
}

uint32_t process_block_thread_lid(process_T* process) {
    for (int i = 0; i < MAX_THREADS_PER_PROC; i++) {
        if (!get_bitmap_at_index(&process->threads_bitmap, i)) {
            set_bitmap_at_index(&process->threads_bitmap, i, true);
            return i+1;     // because thread_id 0 is blocked, but using just 63 thread_ids would be a waste
        }
    }
    return 0;
}

void process_free_thread_lid(process_T* process, uint32_t lid) {
    set_bitmap_at_index(&process->threads_bitmap, lid-1, false);
}

bool process_check_thread_lid(process_T* process, uint32_t lid) {
    return get_bitmap_at_index(&process->threads_bitmap, lid-1);
}

thread_T* process_get_thread(process_T* process, uint32_t lid) {
    if (!process_check_thread_lid(process, lid)) {
        return NULL;
    }
    return process->threads[lid-1];
}