//
// Created by antifallobst on 9/21/22.
//

#include <process/elf/Symbol.h>

char* elf_symbol_types[7] = {
        "None",
        "Object",
        "Function",
        "Section",
        "File",
        "Common",
        "TLS"
};