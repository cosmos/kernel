//
// Created by antifallobst on 10/6/22.
//

#include <process/elf/Dynamic.h>
#include <utils/Logger.h>

void dynamic_linker_resolve_symbol() {
    log(LOGGING_DEBUG, "Dynamic Linker -> Runtime symbol resolver");
}