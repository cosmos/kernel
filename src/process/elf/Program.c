//
// Created by antifallobst on 8/17/22.
//

#include <process/elf/Program.h>
#include <utils/Logger.h>
#include <utils/Math.h>

char* elf_program_str_types[9] = {
        "Null",
        "Loadable",
        "Dynamic Linker Info",
        "Interpreter Info",
        "Note",
        "Unknown 0x05",
        "Program Header Table",
        "Thread Local Storage",
        "Unknown 0x07+"
};

int elf_program_get_loadable_segments(elf_program_entry_x64_T* segment_table, size_t num_entries) {
    int counter = 0;

    for (int i = 0; i < num_entries; i++) {
        if (segment_table[i].type == ELF_PROGRAM_ENTRY_LOADABLE) counter++;
    }
    return counter;
}

void elf_program_parse_table_x64(void *buffer, size_t buffer_size, elf_program_entry_x64_T *entries, size_t num_entries) {
    log(LOGGING_NONE, "");
    for (int i = 0; i < num_entries; i++) {
        if((i * sizeof(elf_program_entry_x64_T)) >= (int)buffer_size) return; // If this entry doesn't fit in anymore

        elf_program_entry_x64_T* entry = (elf_program_entry_x64_T*)&buffer[i * sizeof(elf_program_entry_x64_T)];

        log(LOGGING_INFO, "ELF -> Program Header %d", i);
        log(LOGGING_NONE, "                 > Type: %s", elf_program_str_types[min(entry->type, 8)]);
        log(LOGGING_NONE, "                 > Offset: 0x%x64", entry->offset_segment);
        log(LOGGING_NONE, "                 > Virtual Address: 0x%x64", entry->virtual_address);
        log(LOGGING_NONE, "                 > Physical Address: 0x%x64", entry->physical_address);
        log(LOGGING_NONE, "                 > Length On Disk: %d", entry->length_on_disk);
        log(LOGGING_NONE, "                 > Length In Memory: %d", entry->length_in_mem);
        log(LOGGING_NONE, "                 > Alignment: 0x%x64", entry->alignment);
        log(LOGGING_NONE, "                 > Flags:");
        if ((entry->flags & ELF_PROGRAM_FLAG_READABLE) > 0) log(LOGGING_NONE,   "                   * Readable");
        if ((entry->flags & ELF_PROGRAM_FLAG_WRITABLE) > 0) log(LOGGING_NONE,   "                   * Writable");
        if ((entry->flags & ELF_PROGRAM_FLAG_EXECUTABLE) > 0) log(LOGGING_NONE, "                   * Executable");

        entries[i] = *entry;
    }
}