//
// Created by antifallobst on 10/5/22.
//

#include <process/elf/LibraryManager.h>
#include <memory/paging/PageMap.h>
#include <memory/Memory.h>
#include <memory/Heap.h>
#include <utils/Logger.h>
#include <utils/Math.h>
#include <utils/StrUtil.h>
#include <drivers/File.h>

uint64_t g_shared_object_space = 0x10000000;

shared_library_manager_T  G_lib_manager;
shared_library_manager_T* g_lib_manager = &G_lib_manager;

void library_manager_init(shared_library_manager_T* lib_manager) {
    lib_manager->base_address   = g_shared_object_space;
    lib_manager->libraries      = malloc(MAX_LIBRARIES * sizeof(shared_library_T));
    lib_manager->num_libraries  = 0;
}

void library_manager_load_library_from_file(shared_library_manager_T* lib_manager, const char* path, const char* name) {
    file_T file = open_file(path, FILE_READ);

    void* buffer = malloc(file.header.size);
    read_file(&file, 0, buffer, file.header.size);

    library_manager_load_library(lib_manager, buffer, file.header.size, name);
}

void library_manager_load_library(shared_library_manager_T* lib_manager, void* buffer, uint64_t buffer_len, const char* name) {
    log(LOGGING_NONE, "");
    log(LOGGING_INFO, "Library Manager -> Loading '%s'", name);

    shared_library_T* lib   = &lib_manager->libraries[lib_manager->num_libraries];
    lib->executable         =  malloc(sizeof(elf_executable_T));
    lib->name               =  (char*)name;
    lib->start              =  lib_manager->base_address;

    if (lib_manager->num_libraries > 0) {
        lib->start = lib_manager->libraries[lib_manager->num_libraries - 1].end;
    }

    elf_executable_T exec = elf_load_executable(buffer, buffer_len, (void*)lib->start, g_page_map);

    memcpy((void*)lib->executable, &exec, sizeof(elf_executable_T));

    lib->end = exec.mappings_end;
    lib_manager->num_libraries++;

    log(LOGGING_INFO, "Library Manager -> Library '%s' loaded successfully", name);
}

shared_library_T* library_manager_get_lib_by_name(shared_library_manager_T* lib_manager, const char* name) {
    for (int i = 0; i < lib_manager->num_libraries; i++) {
        if (memcmp(name, lib_manager->libraries[i].name, strlen(name))) {
            return &lib_manager->libraries[i];
        }
    }
    return NULL;
}