//
// Created by antifallobst on 8/17/22.
//

#include <process/elf/Section.h>

void elf_section_parse_table_x64(uint8_t* buffer, size_t buffer_size, elf_section_entry_x64_T* sections, size_t num_sections) {
    for (int i = 0; i < num_sections; i++) {
        if((i * sizeof(elf_section_entry_x64_T)) >= (int)buffer_size) return;
        sections[i] = *(elf_section_entry_x64_T*)&buffer[i * sizeof(elf_section_entry_x64_T)];
    }
}