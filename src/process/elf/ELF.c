//
// Created by antifallobst on 8/16/22.
//

#include <process/elf/ELF.h>
#include <process/elf/Relocation.h>
#include <process/elf/LibraryManager.h>
#include <process/elf/Dynamic.h>
#include <memory/paging/Paging.h>
#include <memory/paging/PageFrame.h>
#include <memory/Memory.h>
#include <memory/Heap.h>
#include <utils/StrUtil.h>
#include <utils/Logger.h>
#include <utils/Math.h>

uint8_t elf_magic_number[4] = { 0x7f, 'E', 'L', 'F' };

elf_executable_T elf_load_executable(void* buffer, size_t buffer_size, void* load_address, paging_page_table_T* page_map) {
    log(LOGGING_NONE, "");
    elf_executable_T executable;
    log(LOGGING_INFO, "ELF -> Loading Executable");

    executable.buffer_size  = buffer_size;
    executable.buffer       = (uint8_t*)buffer;
    executable.load_address = (uint64_t)load_address;
    memcpy(&executable.header, executable.buffer, sizeof(elf_header_x64_T));

//    hexdump(buffer, buffer_size);

    log(LOGGING_INFO,             "ELF -> Header");
    log(LOGGING_NONE, "                 > Bits: %s", elf_header_str_num_bits[executable.header.identity[4]]);
    log(LOGGING_NONE, "                 > Endian: %s", elf_header_str_endianness[executable.header.identity[5]]);
    log(LOGGING_NONE, "                 > ABI: %s", elf_header_str_system_ABI[executable.header.identity[7]]);
    log(LOGGING_NONE, "                 > Type: %s", elf_header_str_object_type[executable.header.elf_type]);
    log(LOGGING_NONE, "                 > Instruction Set: %s", elf_header_str_instruction_set[executable.header.instruction_set]);
    log(LOGGING_NONE, "                 > Entry: 0x%x64", executable.header.address_entry_point);
    log(LOGGING_NONE, "                 > Offset Program Header: 0x%x64", executable.header.offset_program_header);
    log(LOGGING_NONE, "                 > Program Headers: %d", executable.header.num_program_header_entries);
    log(LOGGING_NONE, "                 > Offset Section Header: 0x%x64", executable.header.offset_section_header);
    log(LOGGING_NONE, "                 > Section Headers: %d", executable.header.num_section_header_entries);

    elf_validate_executable(&executable);

    executable.program_table = malloc(executable.header.num_program_header_entries * sizeof(elf_program_entry_x64_T));
    elf_program_parse_table_x64(&executable.buffer[executable.header.offset_program_header], buffer_size,
                                executable.program_table, executable.header.num_program_header_entries);
    log(LOGGING_INFO, "ELF [SEGMENTS] -> Segments read");

    executable.section_table = malloc(executable.header.num_section_header_entries * sizeof(elf_section_entry_x64_T));
    elf_section_parse_table_x64(&executable.buffer[executable.header.offset_section_header], buffer_size,
                                executable.section_table, executable.header.num_section_header_entries);
    log(LOGGING_INFO, "ELF [SECTIONS] -> Sections read");

    executable.section_header_string_table = executable.section_table[executable.header.string_section_index];
    log(LOGGING_INFO, "ELF [STRTAB] -> Looked up section header string table (index: %d)", executable.header.string_section_index);
    uint8_t strtab_section          =  elf_lookup_section_index(&executable, ".strtab");
    executable.string_table         =  executable.section_table[strtab_section];
    log(LOGGING_INFO, "ELF [STRTAB] -> Looked up string table (index: %d)", strtab_section);

    uint8_t dynstr_section          =  elf_lookup_section_index(&executable, ".dynstr");
    executable.dynamic_string_table =  executable.section_table[dynstr_section];
    log(LOGGING_INFO, "ELF [STRTAB] -> Looked up dynamic string table (index: %d)", dynstr_section);

    uint8_t symtab_section          =  elf_lookup_section_index(&executable, ".symtab");
    executable.symbol_table         =  executable.section_table[symtab_section];
    log(LOGGING_INFO, "ELF [SYMTAB] -> Looked up symbol table (index: %d)", symtab_section);

    uint8_t dynsym_section          =  elf_lookup_section_index(&executable, ".dynsym");
    executable.dynamic_symbol_table =  executable.section_table[dynsym_section];
    log(LOGGING_INFO, "ELF [SYMTAB] -> Looked up dynamic symbol table (index: %d)", dynsym_section);

    uint8_t got_plt_section         =  elf_lookup_section_index(&executable, ".got.plt");
    executable.got_plt              =  executable.section_table[got_plt_section];
    log(LOGGING_INFO, "ELF [GOT.PLT] -> Looked up global offset table (index: %d)", got_plt_section);


    uint8_t dynamic_section         =  elf_lookup_section_index(&executable, ".dynamic");
    executable.dynamic              = &executable.section_table[dynamic_section];
    log(LOGGING_INFO, "ELF [DYNAMIC] -> Looked up dynamic section (index: %d)", dynamic_section);


    uint64_t    address         = 0;
    executable.mappings_end     = 0;
    for(int i = 0; i < executable.header.num_program_header_entries; i++) {
        elf_program_entry_x64_T* segment = &executable.program_table[i];
        switch (segment->type) {
            case ELF_PROGRAM_ENTRY_LOADABLE: {

                uint64_t destination = (uint64_t)load_address + segment->virtual_address;

                log(LOGGING_NONE, "                     > Mapping %d: [ Physical: 0x%x |  Offset: 0x%x  |  Size: 0x%x ]", i,
                    segment->physical_address, segment->offset_segment, segment->length_on_disk);

                // Map the exact number of pages that are needed in RAM.
                size_t num_pages = ceil_to(segment->length_in_mem + (segment->virtual_address - floor_to((uint64_t) segment->virtual_address, PAGE_SIZE)), PAGE_SIZE) / PAGE_SIZE;

                for (int j = 0; j < (int) num_pages; j++) {
                    address = floor_to((uint64_t) destination + (j * PAGE_SIZE), PAGE_SIZE);
                    if (address > executable.mappings_end) executable.mappings_end = address;
                    void* phys_address = request_page();
                    uint64_t map_flags = 0;
                    if ((segment->flags & ELF_PROGRAM_FLAG_WRITABLE) > 0) map_flags |= PTF_READ_WRITE;
                    if ((segment->flags & ELF_PROGRAM_FLAG_EXECUTABLE) == 0) map_flags |= PTF_NO_EXECUTE;
                    map_memory(g_page_map, PTF_READ_WRITE, (void *) address, phys_address);
                    map_memory(page_map, map_flags, (void *) address, phys_address);
                    log(LOGGING_INFO, "0x%x mapped", address);
                }

                log(LOGGING_NONE, "                         > copying data  -  0x%x  |  0x%x", destination, segment->offset_segment);
                memcpy((void *) destination,
                       &executable.buffer[segment->offset_segment],
                       segment->length_in_mem);
                log(LOGGING_NONE, "                         > done  -  0x%x", destination + segment->length_in_mem);
                break;
            }
        }
    }

    for (int i = 0; i < executable.header.num_section_header_entries; i++) {
        elf_section_entry_x64_T section = executable.section_table[i];
        if (section.type == SECTION_NOBITS) {
            log(LOGGING_NONE, "                         > zeroing data out  -  0x%x", load_address + section.virtual_address);
            memset((void *) load_address + section.virtual_address,
                   0,
                   section.length);
            log(LOGGING_NONE, "                         > done  -  0x%x", load_address + section.virtual_address + section.length);
        }
    }

    elf_resolve_libraries(&executable);

    elf_parse_symbol_table(&executable);

    elf_relocate(&executable, (uint64_t)load_address);

    for(int i = 0; i < executable.header.num_program_header_entries; i++) {
        elf_program_entry_x64_T* segment = &executable.program_table[i];
        switch (segment->type) {
            case ELF_PROGRAM_ENTRY_LOADABLE: {

                uint64_t destination = (uint64_t)load_address + segment->virtual_address;

                log(LOGGING_NONE, "                     > Mapping %d: [ Physical: 0x%x |  Offset: 0x%x  |  Size: 0x%x ]", i,
                    segment->physical_address, segment->offset_segment, segment->length_on_disk);

                // Map the exact number of pages that are needed in RAM.
                size_t num_pages = ceil_to(segment->length_in_mem + (segment->virtual_address - floor_to((uint64_t) segment->virtual_address, PAGE_SIZE)), PAGE_SIZE) / PAGE_SIZE;

                for (int j = 0; j < (int) num_pages; j++) {
                    unmap_memory(g_page_map, (void *)floor_to((uint64_t) destination + (j * PAGE_SIZE), PAGE_SIZE));
                    log(LOGGING_INFO, "0x%x unmapped", address);
                }
                break;
            }
        }
    }

    if (got_plt_section != 0) {
        elf_set_got(&executable);
    }

    executable.image.entry_point = symbol_lookup(&executable.image.symbols, ELF_START_SYMBOL);

    log(LOGGING_INFO, "ELF [IMAGE] -> start symbol Offset: 0x%x", executable.image.entry_point->address);

    return executable;
}


bool elf_validate_executable(elf_executable_T* executable) {
    if (!memcmp(executable->header.identity, elf_magic_number, 4)) {
        log(LOGGING_ERROR, "ELF -> Executable has a wrong magic number");
        return false;
    }
    if (executable->header.elf_type != ELF_OBJECT_DYNAMIC_LIB) {
        log(LOGGING_ERROR, "ELF -> Executable is not a DYN object");
        return false;
    }
    if (executable->header.identity[6] != ELF_ENDIAN_LITTLE) {
        log(LOGGING_ERROR, "ELF -> Executable is not Little Endian");
        return false;
    }
    log(LOGGING_INFO, "ELF -> Executable is valid");
    return true;
}

uint64_t elf_lookup_section_index(elf_executable_T* executable, const char* name) {
    for (int i = 0; i < executable->header.num_section_header_entries; i++) {
        elf_section_entry_x64_T section = executable->section_table[i];

        if(strcmp(name, (const char *)&executable->buffer[executable->section_header_string_table.offset + section.name_offset])) {
            return i;
        }
    }
    return 0;
}

void elf_parse_symbol_table(elf_executable_T* executable) {
    for (int i = 0; i < executable->header.num_section_header_entries; i++) {
        elf_section_entry_x64_T section = executable->section_table[i];

        if (section.type == SECTION_SYMBOL_TABLE) {
            log(LOGGING_INFO, "ELF -> Found Symbol table at index %d", i);

            elf_symbol_T    symbol;
            size_t          num_symbols         = section.length / section.entry_size;
            executable->image.symbols.num_funcs = 2;
            executable->image.symbols.num_vars  = 2;

            for (int j = 0; j < num_symbols; j++) {
                symbol    = ((elf_symbol_T *)&executable->buffer[section.offset])[j];
                switch (ELF_SYMBOL_TYPE(symbol.info)) {
                    case ELF_SYM_FUNC: {
                        executable->image.symbols.num_funcs++;
                        break;
                    }
                    case ELF_SYM_OBJECT: {
                        executable->image.symbols.num_vars++;
                        break;
                    }
                }
            }

            executable->image.symbols.funcs     = malloc((executable->image.symbols.num_funcs + 1) * sizeof(symbol_T));
            executable->image.symbols.vars      = malloc((executable->image.symbols.num_vars + 1) * sizeof(symbol_T));

            uint64_t func_index = 1;
            uint64_t var_index  = 1;
            for (int j = 0; j < num_symbols; j++) {
                symbol    = ((elf_symbol_T *)&executable->buffer[section.offset])[j];
                switch (ELF_SYMBOL_TYPE(symbol.info)) {
                    case ELF_SYM_FUNC: {
                        executable->image.symbols.funcs[func_index].name    = (char *) &executable->buffer[executable->string_table.offset + symbol.name];
                        executable->image.symbols.funcs[func_index].address = symbol.value;
                        func_index++;
                        break;
                    }
                    case ELF_SYM_OBJECT: {
                        executable->image.symbols.vars[var_index].name    = (char *) &executable->buffer[executable->string_table.offset + symbol.name];
                        executable->image.symbols.vars[var_index].address = symbol.value;
                        var_index++;
                        break;
                    }
                }
            }

            executable->image.symbols.funcs[0].name             = "BEGIN";
            executable->image.symbols.funcs[0].address          = 0;
            executable->image.symbols.funcs[func_index].name    = "END";
            executable->image.symbols.funcs[func_index].address = 0xFFFFFFF0;
            executable->image.symbols.vars[0].name              = "BEGIN";
            executable->image.symbols.vars[0].address           = 0;
            executable->image.symbols.vars[var_index].name      = "END";
            executable->image.symbols.vars[var_index].address   = 0xFFFFFFF0;

        }
    }
}

elf_symbol_T elf_lookup_symbol_in_table(elf_executable_T* executable, elf_section_entry_x64_T symbol_table, char* name) {
    elf_symbol_T    symbol;
    symbol.name                     = 0;
    size_t          num_symbols     = symbol_table.length / symbol_table.entry_size;
    uint64_t        string_table    = executable->string_table.offset;

    if (memcmp(&executable->buffer[executable->section_header_string_table.offset + symbol_table.name_offset], ".dyn", 4)) {
        string_table = executable->dynamic_string_table.offset;
    }

    for (int i = 0; i < num_symbols; i++) {
        symbol    = ((elf_symbol_T *)&executable->buffer[symbol_table.offset])[i];

        if(strcmp(name, (const char *)&executable->buffer[string_table + symbol.name])) {
            log(LOGGING_INFO, "ELF [SymLookup] -> found symbol %s", name);
            return symbol;
        }
    }
    log(LOGGING_INFO, "ELF -> Symbol '%s' not found!", name);
    return (elf_symbol_T){0, 0, 0, 0, 0, 0};
}

void elf_resolve_libraries(elf_executable_T* executable) {
    size_t num_entries          = executable->dynamic->length / executable->dynamic->entry_size;
    executable->num_libraries   = 0;

    for (int i = 0; i < num_entries; i++) {
        elf_dynamic_T entry = ((elf_dynamic_T *) &executable->buffer[executable->dynamic->offset])[i];
        switch (entry.tag) {
            case ELF_DYN_NEEDED: {
                executable->num_libraries++;
            }
        }
    }
    executable->libraries = malloc(executable->num_libraries * sizeof(shared_library_T*));
    executable->num_libraries = 0;

    for (int i = 0; i < num_entries; i++) {
        elf_dynamic_T entry = ((elf_dynamic_T*)&executable->buffer[executable->dynamic->offset])[i];
        switch (entry.tag) {
            case ELF_DYN_NEEDED: {
                char* library_name = &executable->buffer[executable->dynamic_string_table.offset + entry.value];
                log(LOGGING_NONE, "            > Found needed library '%s'", library_name);

                shared_library_T* library = library_manager_get_lib_by_name(g_lib_manager, library_name);
                if (library == NULL) {
                    log(LOGGING_WARNING, "ELF -> Needed Library '%s' is not loaded!", library_name);
                } else {
                    executable->libraries[executable->num_libraries] = library;
                }
                executable->num_libraries++;
                break;
            }
        }
    }
}

void elf_set_got(elf_executable_T* executable) {
    uint64_t* got = (uint64_t*)&executable->buffer[executable->got_plt.offset];
    log(LOGGING_INFO, "ELF [GOT] -> located GOT at 0x%x", got);
    log(LOGGING_INFO, "        > size 0x%x", executable->got_plt.length);
    log(LOGGING_INFO, "        > entry_size 0x%x8", executable->got_plt.entry_size);

    got[1] = (uint64_t)got;
    got[2] = (uint64_t)dynamic_linker_resolve_symbol;

    for (int i = 0; i < (executable->got_plt.length / executable->got_plt.entry_size); i++) {
        log(LOGGING_INFO, "            > GOT[%d] = 0x%x", i, got[i]);
    }
}