//
// Created by antifallobst on 8/16/22.
//

#include <process/elf/Header.h>
#include <utils/Logger.h>

char *str_unspecified = "Unspecified";

char *elf_header_str_num_bits[] = {
    "Invalid",
    "32-bit",
    "64-bit"
};

char *elf_header_str_endianness[] = {
    "Invalid",
    "Little endian",
    "Big Endian"
};

char *elf_header_str_system_ABI[] = {
    "System V",
    "HP-UX",
    "NetBSD",
    "Linux",
    "GNU Hurd",
    "Solaris",
    "AIX",
    "IRIX",
    "FreeBSD",
    "Tru64",
    "Novell Modesto",
    "OpenBSD",
    "OpenVMS",
    "NonStop Kernel",
    "AROS",
    "FenixOS",
    "Nuxi CloudABI",
    "Stratus Technologies OpenVOS"
};

char *elf_header_str_object_type[] = {
    "None",
    "Relocatable Object",
    "Executable",
    "Shared Library",
    "Core Object"
};

char *elf_header_str_instruction_set[] = {
    "Undefined",
    "AT&T WE 32100",
    "SPARC",
    "x86_32",
    "Motorola 68000",
    "Motorola 88000",
    "Intel MCU",
    "Intel 80860",
    "MIPS",
    "IBM System/370",
    "MIPS RS3000 Little-Endian",
    "Reserved 0x0B",
    "Reserved 0x0C",
    "Reserved 0x0D",
    "HP PA-RISC",
    "Reserved 0x0F",
    "Unknown 0x10",
    "Unknown 0x11",
    "Unknown 0x12",
    "Intel 80960",
    "PowerPC",
    "PowerPC 64",
    "S390(x)",
    "IBM SPU/SPC",
    "Reserved 0x18",
    "Reserved 0x19",
    "Reserved 0x1A",
    "Reserved 0x1B",
    "Reserved 0x1C",
    "Reserved 0x1D",
    "Reserved 0x1E",
    "Reserved 0x1F",
    "Reserved 0x20",
    "Reserved 0x21",
    "Reserved 0x22",
    "Reserved 0x23",
    "NEC V800",
    "Fujitsu FR20",
    "TRW RH-32",
    "Motorola RCE",
    "ARM-32",
    "Digital Alpha",
    "SuperH",
    "SPARC v9",
    "Siemens TriCore",
    "Argonaut RISC",
    "Hitachi H8/300",
    "Hitachi H8/300H",
    "Hitachi H8S",
    "Hitachi H8/500",
    "IA-64",
    "Stanford MIPS-X",
    "Motorola ColdFire",
    "Motorola M68HC12",
    "Fujitsu MultiMedia Accelerator",
    "Siemens PCP",
    "Sony nCPU",
    "Denso NDR1",
    "Motorola Star*Core",
    "Toyota ME16",
    "STMicroelectronis ST100",
    "Advanced Logic TinyJ",
    "x86-64",
    "Sony DSP",
    "Digital Equipment PDP-10",
    "Digital Equipment PDP-11",
    "Siemens FX66",
    "STMicroelectronis ST9+",
    "STMicroelectronis ST7",
    "Motorola MC68HC16",
    "Motorola MC68HC11",
    "Motorola MC68HC08",
    "Motorola MC68HC05",
    "Silicon Graphics SVx",
    "STMicroelectronics ST19",
    "Digital VAX",
    "Axis Communications 32-bit",
    "Infineon Communications 32-bit",
    "Element 14 64-bit DSP",
//    "LSI Logic 16-bit DSP",
//    "TMS320C6000 Family",
//    "MCST Elbrus e2k",
//    "ARM-64",
//    "Zilog Z80",
//    "RISC-V",
//    "Berkeley Packet Filter",
//    "WDC 64C816"
};

void elf_log_header(elf_header_x64_T* header) {
    log(LOGGING_INFO, "Logging ELF header");
    log(LOGGING_INFO, "Bits: %d", header->identity[5]);
    log(LOGGING_INFO, "Endian: %d", header->identity[6]);
//    log(LOGGING_INFO, "System ABI: %d", header->identity);
    log(LOGGING_INFO, "ELF Type: %d", header->elf_type);
    log(LOGGING_INFO, "Instruction Set: %d", header->instruction_set);
    log(LOGGING_INFO, "Entry point: 0x%x", header->address_entry_point);
    log(LOGGING_INFO, "Offset Program Header: %d", header->offset_program_header);
    log(LOGGING_INFO, "Offset Section Header: %d", header->offset_section_header);
}