//
// Created by antifallobst on 8/18/22.
//

#include <process/Scheduler.h>
#include <memory/Heap.h>
#include <memory/Memory.h>
#include <utils/Math.h>
#include <utils/Logger.h>
#include <drivers/time/PIT.h>

scheduler_T                         G_scheduler;
scheduler_T*    g_scheduler     =  &G_scheduler;


void init_scheduler(scheduler_T* scheduler) {
    scheduler->processes            = malloc(MAX_PROCESSES * sizeof(process_T*));
    scheduler->threads              = malloc(MAX_THREADS * sizeof(thread_T*));
    scheduler->queue                = malloc(MAX_PROCESSES * sizeof(thread_T*));

    size_t threads_bitmap_size      = ceil_to(MAX_THREADS, 8) / 8;
    size_t processes_bitmap_size    = ceil_to(MAX_THREADS, 8) / 8;

    init_bitmap(&scheduler->processes_bitmap, malloc(processes_bitmap_size), processes_bitmap_size);
    init_bitmap(&scheduler->threads_bitmap, malloc(threads_bitmap_size), threads_bitmap_size);
    init_bitmap(&scheduler->running_threads_bitmap, malloc(threads_bitmap_size), threads_bitmap_size);

    scheduler->queue_index      = 0;
    scheduler->current_tid      = 0;
    scheduler->allow_switching  = false;
    scheduler->enabled          = false;
}

void enable_scheduler(scheduler_T* scheduler) {
    log_threads();

    log(LOGGING_SUCCESS, "Enabled Scheduler");
    asm("int $0x81");   // load current kernel state into g_kernel_state and schedule
    // when all threads are finished, the execution will jump here
    scheduler->enabled = false;
    log(LOGGING_WARNING, "Exited scheduler (no more threads left in queue)");
}

int new_tid(scheduler_T* scheduler) {
    for (int tid = 0; tid < MAX_THREADS; tid++) {
        if (get_bitmap_at_index(&scheduler->threads_bitmap, tid) == false) {
            set_bitmap_at_index(&scheduler->threads_bitmap, tid, true);
            return tid;
        }
    }
    return -1;
}

int new_pid(scheduler_T* scheduler) {
    for (int pid = 0; pid < MAX_PROCESSES; pid++) {
        if (get_bitmap_at_index(&scheduler->processes_bitmap, pid) == false) {
            set_bitmap_at_index(&scheduler->processes_bitmap, pid, true);
            return pid;
        }
    }
    return -1;
}

int register_process(scheduler_T* scheduler, process_T* process) {
    scheduler->allow_switching = false;
    int pid = new_pid(scheduler);
    if (pid == -1) {
        log(LOGGING_WARNING, "Scheduler -> Cannot register process '%s' maximum process limit reached", process->name);
        return -1;
    }

    process->pid = pid;

    scheduler->processes[pid] = malloc(sizeof(process_T));
    memcpy(scheduler->processes[pid], process, sizeof(process_T));

    log(LOGGING_INFO, "Scheduler -> Registered Process PID[%d] NAME[%s]", pid, scheduler->processes[pid]->name);
    calculate_queue(scheduler);
    return pid;
}


int register_thread(scheduler_T* scheduler, thread_T* thread) {
    int tid = new_tid(scheduler);
    if (tid == -1) {
        log(LOGGING_WARNING, "Scheduler -> Cannot register thread for process '%s' maximum thread limit reached", scheduler->processes[thread->pid]->name);
        return -1;
    }

    thread->priority = THREAD_PRIORITY_HIGH;
    thread->g_tid = tid;
    scheduler->threads[tid] = malloc(sizeof(thread_T));
    memcpy(scheduler->threads[tid], thread, sizeof(thread_T));

    log(LOGGING_INFO, "Scheduler -> Registered Thread  TID[%d]  PID[%d]  PROC_NAME[%s]", tid, thread->pid, scheduler->processes[thread->pid]->name);
    return tid;
}

void pause_thread(scheduler_T* scheduler, int tid) {
    if (!verify_tid(scheduler, tid)) { return; }

    scheduler->allow_switching = false;

    set_bitmap_at_index(&scheduler->running_threads_bitmap, tid, false);

    calculate_queue(scheduler);
}

void start_thread(scheduler_T* scheduler, int tid) {
    if (!verify_tid(scheduler, tid)) { return; }

    scheduler->allow_switching = false;

    set_bitmap_at_index(&scheduler->running_threads_bitmap, tid, true);

    calculate_queue(scheduler);
}

void start_process(scheduler_T* scheduler, int pid) {
    if (!verify_pid(scheduler, pid)) { return; }

    start_thread(scheduler, scheduler->processes[pid]->master_thread->g_tid);
}

void kill_thread(scheduler_T* scheduler, int tid) {
    if (!verify_tid(scheduler, tid)) { return; }

    scheduler->allow_switching = false;

    set_bitmap_at_index(&scheduler->threads_bitmap, tid, false);
    set_bitmap_at_index(&scheduler->running_threads_bitmap, tid, false);

    process_free_thread_lid(scheduler->processes[scheduler->threads[tid]->pid], scheduler->threads[tid]->l_tid);

    free(scheduler->threads[tid]);

    calculate_queue(scheduler);
}

void kill_process(scheduler_T* scheduler, int pid) {
    if (!verify_pid(scheduler, pid)) { return; }

    g_scheduler->allow_switching = false;

    process_T* process = scheduler->processes[pid];

    log(LOGGING_INFO, "Killing Process PID[%d]", process->pid);

    for (int tid = 0; tid < MAX_THREADS_PER_PROC; tid++) {
        thread_T* thread = process_get_thread(process, tid);
        if (thread != NULL) {
            log(LOGGING_INFO, "  * Killing slave thread  GLOBAL_TID[%d]  LOCAL_TID[%d]", thread->g_tid, thread->l_tid);
            kill_thread(g_scheduler, thread->g_tid);
        }
    }

    set_bitmap_at_index(&scheduler->processes_bitmap, pid, false);
    free(process->threads);
    free(process);

    g_scheduler->allow_switching = true;
}

bool verify_pid(scheduler_T* scheduler, int pid) {
    if (pid < 0 || pid > MAX_PROCESSES) {
        return false;
    }
    return get_bitmap_at_index(&scheduler->processes_bitmap, pid);
}

bool verify_tid(scheduler_T* scheduler, int tid) {
    if (tid < 0 || tid > MAX_THREADS) {
        return false;
    }
    return get_bitmap_at_index(&scheduler->threads_bitmap, tid);
}

void calculate_queue(scheduler_T* scheduler) {
    scheduler->allow_switching      = false;

    int index = 0;

    for (int i = 0; i < MAX_THREADS; i++) {
        if (get_bitmap_at_index(&scheduler->running_threads_bitmap, i)) {
            switch (scheduler->threads[i]->priority) {
                case THREAD_PRIORITY_HIGH:
                case THREAD_PRIORITY_LOW: {
                    scheduler->queue[index] = scheduler->threads[i];
                    index += 1;
                    break;
                }
            }
        }
    }

//    log(LOGGING_INFO, "Scheduler -> Calculated queue  ITEMS[%d]", index);

    scheduler->queue_length = index;

    scheduler->allow_switching  = true;
}

cpu_state_T* switch_thread(scheduler_T* scheduler, cpu_state_T* state) {
    if (!scheduler->enabled) {
        return NULL;
    }
    if (scheduler->queue_length == 0) {
        return &g_kernel_state;
    }
    if (!scheduler->allow_switching) {
        log(LOGGING_INFO, "Scheduler -> Switch denied  CURRENT_TID[%d](%s)  INT[0x%x8]",
            scheduler->current_tid, thread_type_strings[scheduler->threads[scheduler->current_tid]->type], state->interrupt_id);
        return NULL;
    }
    scheduler->allow_switching = false;

    if (scheduler->queue_index >= scheduler->queue_length) {
        scheduler->queue_index = 0;
    }

    thread_T* current_thread  = scheduler->threads    [scheduler->current_tid];
    thread_T* next_thread     = scheduler->queue      [scheduler->queue_index];

    if (next_thread->g_tid == current_thread->g_tid && !memcmp(state, &g_kernel_state, sizeof(cpu_state_T))) {
        next_thread->cpu_time                               += 1 / (double)pit_get_frequency();
        scheduler->processes[next_thread->pid]->cpu_time    += 1 / (double)pit_get_frequency();
        scheduler->queue_index                              += 1;
        scheduler->allow_switching                           = true;
        return state;
    }

    log(LOGGING_INFO, "Switching Thread  [%d -> %d]  CPU_TIME[%f]  PROC_NAME[%s]  INT[0x%x8]",
        current_thread->g_tid, next_thread->g_tid, next_thread->cpu_time, scheduler->processes[next_thread->pid]->name ,state->interrupt_id);

    if (current_thread->cpu_time != 0){
//        log(LOGGING_INFO, "Scheduler -> TID[%d]  (saved)RIP[0x%x]", current_thread->g_tid, state->rip);
        memcpy(&current_thread->cpu_state, state, sizeof(cpu_state_T));
    }
    else {
        next_thread->cpu_state.eflags = state->eflags;
    }

    next_thread->cpu_time                            += 1 / (double)pit_get_frequency();
    scheduler->processes[next_thread->pid]->cpu_time += 1 / (double)pit_get_frequency();

//    log(LOGGING_INFO, "Scheduler -> Next Thread: TID[%d]  RIP[0x%x]", next_thread->g_tid, next_thread->cpu_state.rip);

    scheduler->current_tid      = next_thread->g_tid;
    scheduler->queue_index     += 1;
    scheduler->allow_switching  = true;
    return &next_thread->cpu_state;
}
