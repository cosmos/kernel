//
// Created by antifallobst on 11/2/22.
//

#include <utils/Symbol.h>
#include <utils/StrUtil.h>

symbol_T* symbol_lookup(symbols_T* symbols, const char* name) {
    for (int i = 0; i < symbols->num_funcs; i++) {
        if (strcmp(symbols->funcs[i].name, name)) {
            return &symbols->funcs[i];
        }
    }
    for (int i = 0; i < symbols->num_vars; i++) {
        if (strcmp(symbols->vars[i].name, name)) {
            return &symbols->vars[i];
        }
    }
    return NULL;
}