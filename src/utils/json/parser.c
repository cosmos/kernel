//
// Created by antifallobst on 9/8/22.
//

#include <utils/json/parser.h>
#include <utils/json/tokenizer.h>
#include <utils/Logger.h>
#include <memory/Heap.h>

void json_parse_array(json_ast_node_T* array_master_node, json_scope_T* scope, int* index) {
    json_token_T token;

    while (*index < scope->size) {
        *index += 1;
        token   = scope->tokens[*index];
        switch (token.type) {
            case JSON_TOKEN_STRING: {
                json_ast_node_add_child(array_master_node, (json_ast_data_T){JSON_AST_NODE_STRING, token.length, token.string});
                break;
            }

            case JSON_TOKEN_BOOL: {
                json_ast_node_add_child(array_master_node, (json_ast_data_T){JSON_AST_NODE_BOOL, token.length, token.string});
                break;
            }

            case JSON_TOKEN_INTEGER: {
                json_ast_node_add_child(array_master_node, (json_ast_data_T){JSON_AST_NODE_INTEGER, token.length, token.string});
                break;
            }

            case JSON_TOKEN_NULL: {
                json_ast_node_add_child(array_master_node, (json_ast_data_T){JSON_AST_NODE_NULL, token.length, token.string});
                break;
            }

            case JSON_TOKEN_BRACKET_SQUARE_OPEN: {
                json_parse_array(json_ast_node_add_child(array_master_node, (json_ast_data_T){JSON_AST_NODE_ARRAY, 0, NULL}), scope, index);
                break;
            }

            case JSON_TOKEN_BRACKET_SQUARE_CLOSE: {
                return;
            }

            case JSON_TOKEN_COMMA: {
                break;
            }

            default: {
                log(LOGGING_ERROR, "JSON parser -> Misplaced token '%s' in Array", token.string);
                // TODO: Error node in ast
                return;
            }
        }
    }
}


void json_parse_scope (json_ast_node_T* scope_master_node, json_scope_T* scope, int* index) {
    json_token_T        token;
    json_ast_node_T*    node    = NULL;

    while (*index < scope->size - 1) {
        *index += 1;
        token   = scope->tokens[*index];
        switch (token.type) {
            case JSON_TOKEN_BRACKET_CURLY_CLOSE: {
                return;
            }
            case JSON_TOKEN_STRING: {
                node = json_ast_node_add_child(scope_master_node, (json_ast_data_T){JSON_AST_NODE_STRING, token.length, token.string});
                break;
            }

            case JSON_TOKEN_COLON: {
                *index += 1;
                token   = scope->tokens[*index];

                if (node == NULL) {
                    log(LOGGING_ERROR, "JSON Parser -> Misplaced token '%s' after nothing", token.string);
                    // TODO: Error node in ast
                    return;
                }

                switch (token.type) {
                    case JSON_TOKEN_BRACKET_CURLY_OPEN: {
                        json_parse_scope(json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_SCOPE, 0, NULL}), scope, index);
                        break;
                    }
                    case JSON_TOKEN_BRACKET_SQUARE_OPEN: {
                        json_parse_array(json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_ARRAY, 0, NULL}), scope, index);
                        break;
                    }

                    case JSON_TOKEN_BOOL: {
                        json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_BOOL, token.length, token.string});
                        break;
                    }
                    case JSON_TOKEN_STRING: {
                        json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_STRING, token.length, token.string});
                        break;
                    }

                    case JSON_TOKEN_INTEGER: {
                        json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_INTEGER, token.length, token.string});
                        break;
                    }

                    case JSON_TOKEN_NULL: {
                        json_ast_node_add_child(node, (json_ast_data_T){JSON_AST_NODE_NULL, token.length, token.string});
                        break;
                    }

                    default: {
                        log(LOGGING_ERROR, "JSON Parser -> Misplaced token '%s' after ':'", token.string);
                        // TODO: Error node in ast
                        return;
                    }
                }
                break;
            }

            case JSON_TOKEN_COMMA: {
                if (node == NULL) {
                    log(LOGGING_ERROR, "JSON Parser -> Misplaced token '%s' after nothing", token.string);
                    // TODO: Error node in ast
                    return;
                }
                node = NULL;
                break;
            }

            default: {
                log(LOGGING_ERROR, "JSON Parser -> Misplaced token '%s' before ':'", token.string);
                // TODO: Error node in ast
                return;
            }
        }
    }
}

char* token_type_names[10] = {
        "NULL        ",
        "INTEGER     ",
        "STRING      ",
        "CURLY_OPEN  ",
        "CURLY_CLOSE ",
        "SQUARE_OPEN ",
        "SQUARE_CLOSE",
        "COLON       ",
        "COMMA       ",
        "BOOL        "
};

json_ast_node_T json_parse_json_string(char* json_string, int json_string_size) {

    json_scope_T        json_tokenized  = json_tokenize_json(json_string, json_string_size);
    int                 index           = 0;
    json_ast_node_T     master_node     = json_ast_init(&json_tokenized);

    json_parse_scope(&master_node, &json_tokenized, &index);
    free(json_tokenized.tokens);

    return master_node;
}