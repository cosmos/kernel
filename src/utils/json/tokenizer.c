//
// Created by antifallobst on 9/8/22.
//

#include <utils/json/tokenizer.h>
#include <memory/Memory.h>
#include <memory/Heap.h>
#include <stdbool.h>

#include <utils/Logger.h>

bool is_char_number(char c) {
    if (c >= '0' && c <= '9') {
        return true;
    }
    return false;
}

void generate_token_data(json_token_T* token, json_token_types_E type, uint64_t len, char* string) {
    if (token != NULL) {
        token->length   = len;
        token->type     = type;
        token->string   = malloc(token->length);
        memcpy(token->string, string, token->length);
    }
}

void json_next_token(json_token_T* token, char* str, size_t str_size, int* index) {

    while (*index < (int)str_size) {
        bool no_return = false;

        switch (str[*index]) {
            case '{': {
                generate_token_data(token, JSON_TOKEN_BRACKET_CURLY_OPEN, 1, &str[*index]);
                break;
            }

            case '}': {
                generate_token_data(token, JSON_TOKEN_BRACKET_CURLY_CLOSE, 1, &str[*index]);
                break;
            }

            case ':': {
                generate_token_data(token, JSON_TOKEN_COLON, 1, &str[*index]);
                break;
            }

            case '[': {
                generate_token_data(token, JSON_TOKEN_BRACKET_SQUARE_OPEN, 1, &str[*index]);
                break;
            }

            case ']': {
                generate_token_data(token, JSON_TOKEN_BRACKET_SQUARE_CLOSE, 1, &str[*index]);
                break;
            }

            case ',': {
                generate_token_data(token, JSON_TOKEN_COMMA, 1, &str[*index]);
                break;
            }

            case '"': {
                int string_size = -1;
                do {
                    string_size += 1;
                    *index      += 1;
                } while (str[*index] != '"');

                generate_token_data(token, JSON_TOKEN_STRING, string_size, &str[*index - string_size]);
                break;
            }

            default: {
                if (memcmp("true", &str[*index], 4)) {
                    generate_token_data(token, JSON_TOKEN_BOOL, 4, &str[*index]);
                    break;
                }

                if (memcmp("false", &str[*index], 5)) {
                    generate_token_data(token, JSON_TOKEN_BOOL, 5, &str[*index]);
                    break;
                }

                if (memcmp("null", &str[*index], 4)) {
                    generate_token_data(token, JSON_TOKEN_NULL, 4, &str[*index]);
                    break;
                }

                if (is_char_number(str[*index])) {
                    int string_size = 0;
                    while (is_char_number(str[*index])) {
                        string_size += 1;
                        *index      += 1;
                    };

                    generate_token_data(token, JSON_TOKEN_INTEGER, string_size, &str[*index - string_size]);
                    *index -= 1;
                    break;
                }

                no_return = true;
                break;
            }
        }
        *index += 1;

        if (!no_return) {
            return;
        }
    }

}

json_scope_T json_tokenize_json(char* json_string, size_t json_string_size) {
    json_scope_T    scope;
    scope.size                  = 0;
    int             str_index   = 0;

    while (str_index < (int)json_string_size) {
        json_next_token(NULL, json_string, json_string_size, &str_index);
        scope.size++;
    }

    scope.tokens = malloc(scope.size * sizeof(json_token_T));

    int index = 0;
    str_index = 0;
    while (str_index < (int)json_string_size) {
        json_next_token(&scope.tokens[index], json_string, json_string_size, &str_index);
        index++;
    }

    return scope;
}