//
// Created by antifallobst on 9/8/22.
//

#include <utils/json/ast.h>
#include <utils/Math.h>
#include <memory/Heap.h>
#include <memory/Memory.h>

char* json_ast_type_names[6] = {
        "SCOPE",
        "ARRAY",
        "STRING",
        "INTEGER",
        "BOOL",
        "NULL"
};

json_ast_node_T json_ast_init(json_scope_T* scope) {
    json_ast_node_T master;

    master.parent           = (void*)scope;
    master.next             = NULL;
    master.prev             = NULL;
    master.childs           = NULL;
    master.data.length      = 5;
    master.data.string      = "root";

    return master;
}

json_ast_node_T* json_ast_node_add_child(json_ast_node_T* node, json_ast_data_T data) {
    json_ast_node_T*     child           = malloc(sizeof(json_ast_node_T));
    json_ast_node_T**    child_index     = &node->childs;

    child->prev     = NULL;
    child->next     = NULL;
    child->childs   = NULL;
    child->parent   = node;
    child->data     = data;

    while (*child_index != NULL) {
        child->prev = *child_index;
        child_index = &(*child_index)->next;
    }
    *child_index = child;

    return child;
}

json_ast_node_T* json_ast_node_get_child_at_index(json_ast_node_T* node, int index) {
    json_ast_node_T**   child_index     = &node->childs;
    int                 loop_index      = 0;

    while (*child_index != NULL) {
        if (loop_index == index) {
            return *child_index;
        }
        child_index = &(*child_index)->next;
        loop_index++;
    }

    return NULL;
}

uint64_t json_ast_node_num_childs(json_ast_node_T* node) {
    json_ast_node_T**   child_index     = &node->childs;
    int                 index           = 0;

    while (*child_index != NULL) {
        child_index = &(*child_index)->next;
        index++;
    }

    return index;
}

char* json_ast_node_get_assigned_string(json_ast_node_T* node) {
    json_ast_node_T* child = json_ast_node_get_child_at_index(node, 0);
    if (child->data.type == JSON_AST_NODE_STRING) {
        return child->data.string;
    }
    return NULL;
}

void json_ast_clear(json_ast_node_T* node) {
    json_ast_node_T** child = &node->childs;

    while (*child != NULL) {
        json_ast_clear(*child);
        child = &(*child)->next;
    }

    if (node->data.string != NULL && !memcmp(node->data.string, "root", min(node->data.length, 4))) {
        free(node->data.string);
    }
}