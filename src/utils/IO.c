//
// Created by antifallobst on 28/07/2022.
//

#include <utils/IO.h>

void io_out_x8(uint16_t port, uint8_t value) {
    asm volatile ("outb %0, %1" :  : "a"(value), "Nd"(port));
}

void io_out_x16(uint16_t port, uint16_t value) {
    asm volatile ("outw %0, %1" :  : "a"(value), "Nd"(port));
}

void io_out_x32(uint16_t port, uint32_t value) {
    asm volatile ("outl %0, %1" :  : "a"(value), "Nd"(port));
}

uint8_t io_in_x8(uint16_t port) {
    uint8_t return_value;
    asm volatile ("inb %1, %0" : "=a"(return_value) : "Nd"(port));
    return return_value;
}

uint16_t io_in_x16(uint16_t port) {
    uint16_t return_value;
    asm volatile ("inw %1, %0" : "=a"(return_value) : "Nd"(port));
    return return_value;
}

uint32_t io_in_x32(uint16_t port) {
    uint32_t return_value;
    asm volatile ("inl %1, %0" : "=a"(return_value) : "Nd"(port));
    return return_value;
}


void io_wait() {
    asm volatile ("outb %%al, $0x80" : : "a"(0));
}