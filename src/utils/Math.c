//
// Created by antifallobst on 8/16/22.
//

#include <utils/Math.h>


uint64_t octal_to_binary(uint8_t *str, int size) {
    uint64_t         binary  = 0;
    uint8_t*    chr     = str;

    for (int i = 0; i < size; i++) {
        binary  *= 8;
        binary  += *chr - '0';
        chr++;
    }

    return binary;
}

int64_t ceil_to(uint64_t n, int64_t factor) {
    return n + factor - (n % factor);
}

int64_t floor_to(uint64_t n, int64_t factor){
    return n - (n % factor);
}

int64_t min(int64_t val1, int64_t val2){
    if(val1 < val2) return val1;
    return val2;
}

int64_t max(int64_t val1, int64_t val2){
    if(val1 > val2) return val1;
    return val2;
}

double pow(int64_t base, int64_t exponent) {
    double result = 1;

    if (exponent >= 0) {
        for (int i = 0; i < exponent; i++) {
            result *= base;
        }
    } else {
        for (int i = 0; i < abs(exponent); i++) {
            result /= base;
        }
    }
    return result;
}

int64_t abs(int64_t n) {
    if (n < 0) {
        return n * -1;
    }
    return n;
}