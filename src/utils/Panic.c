//
// Created by antifallobst on 15/08/2022.
//

#include <stddef.h>
#include <utils/Panic.h>
#include <utils/Logger.h>
#include <memory/Stack.h>
#include <process/Scheduler.h>

void handle_page_fault(cpu_state_T* state) {
    log(LOGGING_NONE_PRINT, "\nPage Fault Information:");
    bool        bit_P   = ((state->error_code & (1 << 0)) > 0);
    bool        bit_W   = ((state->error_code & (1 << 1)) > 0);
    bool        bit_U   = ((state->error_code & (1 << 2)) > 0);
    bool        bit_R   = ((state->error_code & (1 << 3)) > 0);
    bool        bit_I   = ((state->error_code & (1 << 4)) > 0);
    bool        bit_PK  = ((state->error_code & (1 << 5)) > 0);
    bool        bit_SS  = ((state->error_code & (1 << 6)) > 0);
    bool        bit_SGX = ((state->error_code & (1 << 15)) > 0);
    uint64_t    cr2     = 0;
    uint64_t    cr3     = 0;

    asm volatile ("mov %%cr2, %0" : "=r" (cr2));
    asm volatile ("mov %%cr3, %0" : "=r" (cr3));

    log(LOGGING_NONE_PRINT, "    Loaded Page Table [CR3]: 0x%x", cr3);
    log(LOGGING_NONE_PRINT, "    Accessed Virtual Address [CR2]: 0x%x", cr2);

    log(LOGGING_NONE_PRINT, "\n   <Error Type>                  <Access Type>");
    log(LOGGING_NONE_PRINT, "    %s  |  %s",
        bit_P ? "Page Protection Violation" : "Page is not present      ",
        bit_W ? "Write" : "Read");

    if (bit_U || bit_R || bit_I || bit_PK || bit_SS || bit_SGX) {
        log(LOGGING_NONE_PRINT, "    Flags:");
        if (bit_U)   log(LOGGING_NONE_PRINT, "        U    - User (CPL=3)");
        if (bit_R)   log(LOGGING_NONE_PRINT, "        R    - Reserved Write");
        if (bit_I)   log(LOGGING_NONE_PRINT, "        I    - Instruction Fetch");
        else         log(LOGGING_NONE_PRINT, "       !I    - Data Access");
        if (bit_PK)  log(LOGGING_NONE_PRINT, "        PK   - Protection Key violation");
        if (bit_SS)  log(LOGGING_NONE_PRINT, "        SS   - Shadow Stack access");
        if (bit_SGX) log(LOGGING_NONE_PRINT, "        SGX  - SGX Violation");
    }
}

void panic(const char* message, cpu_state_T* state) {
    thread_T* thread = g_scheduler->threads[g_scheduler->current_tid];

    log(LOGGING_NONE_PRINT, "");
    log(LOGGING_ERROR, "!=====[ KERNEL PANIC ]=====!");
    log(LOGGING_NONE_PRINT, "Exception: %s", message);
    log(LOGGING_NONE_PRINT, "Process Information:  PID[%d]  NAME[%s]  LOCAL_THREAD[%d]",
        thread->pid, g_scheduler->processes[thread->pid]->name, thread->l_tid);

    if (state != NULL) {
        log(LOGGING_NONE_PRINT, "Interrupt: 0x%x8", state->interrupt_id);
        log(LOGGING_NONE_PRINT, "Error Code: 0x%x", state->error_code);

        if (state->interrupt_id == 0x0E) {
            handle_page_fault(state);
        }

        log(LOGGING_NONE_PRINT, "\nCPU EFLAGS:");
        if (((state->eflags & (1 << EFLAGS_CARRY)) > 0))                        log(LOGGING_NONE_PRINT, "    CF  - Carry");
        if (((state->eflags & (1 << EFLAGS_PARITY)) > 0))                       log(LOGGING_NONE_PRINT, "    PF  - Parity");
        if (((state->eflags & (1 << EFLAGS_AUXILIARY)) > 0))                    log(LOGGING_NONE_PRINT, "    AF  - Auxiliary");
        if (((state->eflags & (1 << EFLAGS_ZERO)) > 0))                         log(LOGGING_NONE_PRINT, "    ZF  - Zero");
        if (((state->eflags & (1 << EFLAGS_SIGN)) > 0))                         log(LOGGING_NONE_PRINT, "    SF  - Sign");
        if (((state->eflags & (1 << EFLAGS_TRAP)) > 0))                         log(LOGGING_NONE_PRINT, "    TF  - Trap");
        if (((state->eflags & (1 << EFLAGS_INTERRUPT_ENABLE)) > 0))             log(LOGGING_NONE_PRINT, "    IF  - Interrupt Enable");
        if (((state->eflags & (1 << EFLAGS_DIRECTION)) > 0))                    log(LOGGING_NONE_PRINT, "    DF  - Direction");
        if (((state->eflags & (1 << EFLAGS_OVERFLOW)) > 0))                     log(LOGGING_NONE_PRINT, "    OF  - Overflow");
        if (((state->eflags & (1 << EFLAGS_NESTED_TASK)) > 0))                  log(LOGGING_NONE_PRINT, "    NT  - Nested Task");
        if (((state->eflags & (1 << EFLAGS_RESUME)) > 0))                       log(LOGGING_NONE_PRINT, "    RF  - Resume");
        if (((state->eflags & (1 << EFLAGS_VIRTUAL_8086)) > 0))                 log(LOGGING_NONE_PRINT, "    VM  - Virtual 8086 Mode");
        if (((state->eflags & (1 << EFLAGS_ALIGNMENT_CHECK)) > 0))              log(LOGGING_NONE_PRINT, "    AC  - Alignemt Check");
        if (((state->eflags & (1 << EFLAGS_VIRTUAL_INTERRUPT)) > 0))            log(LOGGING_NONE_PRINT, "    VIF - Virtual Interrupt");
        if (((state->eflags & (1 << EFLAGS_VIRTUAL_INTERRUPT_PENDING)) > 0))    log(LOGGING_NONE_PRINT, "    VIP - Virtual Interrupt Pending");
        if (((state->eflags & (1 << EFLAGS_CPUID)) > 0))                        log(LOGGING_NONE_PRINT, "    ID  - CPUID available");

        log(LOGGING_NONE_PRINT, "\nCPU Registers:");
        log(LOGGING_NONE_PRINT, "    RAX: 0x%x  RBX: 0x%x  RCX: 0x%x", state->rax, state->rbx, state->rcx);
        log(LOGGING_NONE_PRINT, "    RDX: 0x%x  RSI: 0x%x  RDI: 0x%x", state->rdx, state->rsi, state->rdi);
        log(LOGGING_NONE_PRINT, "    RBP: 0x%x  RSP: 0x%x  RIP: 0x%x", state->rbp, state->rsp, state->rip);

        stack_backtrace(state);

    } else {
        log(LOGGING_WARNING, "No CPU_STATE provided -> cannot dump detailed information about panic source");
    }
    log(LOGGING_NONE_PRINT, "");

    log(LOGGING_WARNING, "!=====[ SYSTEM HALTED ]=====!");
    asm("cli");
    while (1) {
        asm("hlt");
    }
}