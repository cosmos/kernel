//
// Created by antifallobst on 28/07/2022.
//

#include <utils/Logger.h>
#include <utils/StrUtil.h>
#include <utils/IO.h>
#include <graphics/Renderer.h>
#include <graphics/Color.h>

void log_char(char c) {
    io_out_x8(LOGGER_QEMU_PORT, c);
}

static void log_string(const char* str, bool print_string, color_t color) {
    char* chr = (char*)str;
    while (*chr != 0) {
        log_char(*chr);
        chr++;
    }
    if (print_string) {
        printf("%C%s", str, color);
    }
}

void log(LoggingTypes_E type, const char* str, ...) {
    va_list args;
    va_start(args, str);
    // calculate size of formatted string
    size_t size = vformat(NULL, str, args);

    va_start(args, str);
    char buf[size + 1];
    vformat(buf, str, args);
    va_end(args);

    // terminate the string with a newline and then a zero
    buf[size]       = '\n';
    buf[size + 1]   = '\0';

    switch (type) {
        case LOGGING_SUCCESS: {
            log_string(LOGGING_PREFIX_SUCCESS, LOGGING_PRINT_SUCCESS, COLOR_PAL_GREEN_SIGNAL);
            log_string(buf, LOGGING_PRINT_SUCCESS, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_INFO: {
            log_string(LOGGING_PREFIX_INFO, LOGGING_PRINT_INFO, COLOR_STD_CYAN);
            log_string(buf, LOGGING_PRINT_INFO, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_DEBUG: {
            log_string(LOGGING_PREFIX_DEBUG, LOGGING_PRINT_DEBUG, COLOR_PAL_PINK);
            log_string(buf, LOGGING_PRINT_DEBUG, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_WARNING: {
            log_string(LOGGING_PREFIX_WARNING, LOGGING_PRINT_WARNING, COLOR_PAL_ORANGE);
            log_string(buf, LOGGING_PRINT_WARNING, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_ERROR: {
            log_string(LOGGING_PREFIX_ERROR, LOGGING_PRINT_ERROR, COLOR_STD_RED);
            log_string(buf, LOGGING_PRINT_ERROR, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_NONE: {
            log_string(buf, LOGGING_PRINT_NONE, COLOR_PAL_GREY_LIGHT);
            break;
        }
        case LOGGING_NONE_PRINT: {
            log_string(buf, true, COLOR_PAL_GREY_LIGHT);
            break;
        }
    }
}