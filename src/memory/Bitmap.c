//
// Created by antifallobst on 28/07/2022.
//

#include <memory/Bitmap.h>
#include <memory/Memory.h>

void init_bitmap(Bitmap_T* bitmap, void* buffer, size_t size) {
    bitmap->size    = size;
    bitmap->buffer  = (uint8_t*)buffer;

    memset(buffer, 0, size);
}

bool get_bitmap_at_index(Bitmap_T* bitmap, uint64_t index) {
    if (index > bitmap->size * 8) return false;

    uint64_t    byte_index  = index / 8;
    uint8_t     bit_index   = index % 8;
    uint8_t     bit_indexer = 0b10000000 >> bit_index;

    if ((bitmap->buffer[byte_index] & bit_indexer) > 0) {
        return true;
    }
    return false;
}

bool set_bitmap_at_index(Bitmap_T* bitmap, uint64_t index, bool value) {
    if (index > bitmap->size * 8) return false;

    uint64_t    byte_index  = index / 8;
    uint8_t     bit_index   = index % 8;
    uint8_t     bit_indexer = 0b10000000 >> bit_index;

    bitmap->buffer[byte_index] &= ~bit_indexer;

    if (value) {
        bitmap->buffer[byte_index] |= bit_indexer;
    }
    return true;
}