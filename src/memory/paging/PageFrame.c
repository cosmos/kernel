//
// Created by antifallobst on 28/07/2022.
//

#include <memory/paging/PageFrame.h>
#include <memory/Memory.h>
#include <memory/Bitmap.h>
#include <utils/Logger.h>

uint64_t    free_memory;
uint64_t    reserved_memory;
uint64_t    used_memory;
uint64_t    page_bitmap_index   = 0;
Bitmap_T    page_bitmap;
bool        initialized         = false;
bool        blocked             = false;

void reserve_page(void* address) {
//    while (blocked);
    blocked = true;

    uint64_t index = (uint64_t) address / 0x1000;
    if (get_bitmap_at_index(&page_bitmap, index)) {
        blocked = false;
        return;
    }
    if (set_bitmap_at_index(&page_bitmap, index, true)) {
        free_memory -= 0x1000;
        reserved_memory += 0x1000;
    }

    blocked = false;
}

void reserve_pages(void* address, uint64_t page_count) {
    for (int i = 0; i < page_count; i++) {
        reserve_page((void*)((uint64_t)address + (i * 0x1000)));
    }
}

void unreserve_page(void* address) {
//    while (blocked);
    blocked = true;

    uint64_t index = (uint64_t)address / 0x1000;
    if (!get_bitmap_at_index(&page_bitmap, index)) {
        blocked = false;
        return;
    }
    if (set_bitmap_at_index(&page_bitmap, index, false)) {
        free_memory += 0x1000;
        reserved_memory -= 0x1000;
        if (page_bitmap_index > index) page_bitmap_index = index;
    }

    blocked = false;
}

void unreserve_pages(void* address, uint64_t page_count) {
    for (int i = 0; i < page_count; i++) {
        unreserve_page((void*)((uint64_t)address + (i * 0x1000)));
    }
}

void free_page(void* address) {
//    while (blocked);
    blocked = true;

    uint64_t index = (uint64_t)address / 0x1000;
    if (!get_bitmap_at_index(&page_bitmap, index)) {
        blocked = false;
        return;
    }
    if (set_bitmap_at_index(&page_bitmap, index, false)) {
        free_memory += 0x1000;
        used_memory -= 0x1000;
        if (page_bitmap_index > index) page_bitmap_index = index;
    }

    blocked = false;
}

void free_pages(void* address, uint64_t page_count) {
    for (int i = 0; i < page_count; i++) {
        free_page((void*)((uint64_t)address + (i * 0x1000)));
    }
}

void lock_page(void* address) {
//    while (blocked);
    blocked = true;

    uint64_t index = (uint64_t)address / 0x1000;
    if (get_bitmap_at_index(&page_bitmap, index)) {
        blocked = false;
        return;
    }
    if (set_bitmap_at_index(&page_bitmap, index, true)) {
        free_memory -= 0x1000;
        used_memory += 0x1000;
        if (page_bitmap_index > index) page_bitmap_index = index;
    }

    blocked = false;
}

void lock_pages(void* address, uint64_t page_count) {
    for (int i = 0; i < page_count; i++) {
        lock_page((void*)((uint64_t)address + (i * 0x1000)));
    }
}

void* request_page() {
//    while (blocked);
    blocked = true;

    for (; page_bitmap_index < page_bitmap.size * 8; page_bitmap_index++) {
        if (get_bitmap_at_index(&page_bitmap, page_bitmap_index)) { continue; }
        lock_page((void *)(page_bitmap_index * 0x1000));

        blocked = false;
        return (void *) (page_bitmap_index * 0x1000);
    }

    blocked = false;
    return NULL;
}

uint64_t get_free_RAM() {
    return free_memory;
}

uint64_t get_used_RAM() {
    return used_memory;
}

uint64_t get_reserved_RAM() {
    return reserved_memory;
}

void read_EFI_memory_map(EFI_MEMORY_DESCRIPTOR_T* memory_map, size_t memory_map_size, size_t memory_map_descriptor_size) {
    if (initialized) return;

    uint64_t memory_map_entries                 = memory_map_size / memory_map_descriptor_size;
    void* largest_free_memory_segment           = NULL;
    size_t largest_free_memory_segment_size     = 0;

    for (int i = 0; i < memory_map_entries; i++) {
        EFI_MEMORY_DESCRIPTOR_T* descriptor = (EFI_MEMORY_DESCRIPTOR_T*)((uint64_t)memory_map + (i * memory_map_descriptor_size));
        if (descriptor->type == EMT_CONVENTIONAL) {
            if (descriptor->num_pages * 0x1000 > largest_free_memory_segment_size) {
                largest_free_memory_segment         = descriptor->physical_address;
                largest_free_memory_segment_size    = descriptor->num_pages * 0x1000;
            }
        }
    }

    uint64_t memory_size    = get_memory_size(memory_map, memory_map_entries, memory_map_descriptor_size);
    free_memory             = memory_size;
    uint64_t bitmap_size    = memory_size / 0x1000 / 8 + 1;

    init_bitmap(&page_bitmap, largest_free_memory_segment, bitmap_size);

    reserve_pages(0, memory_size / 0x1000 + 1);
    log(LOGGING_INFO, "Reserved ALL pages in Memory");

    for (int i = 0; i < memory_map_entries; i++) {
        EFI_MEMORY_DESCRIPTOR_T* descriptor = (EFI_MEMORY_DESCRIPTOR_T*)((uint64_t)memory_map + (i * memory_map_descriptor_size));
        if (descriptor->type == EMT_CONVENTIONAL) {
            unreserve_pages(descriptor->physical_address, descriptor->num_pages);
        }
    }
    log(LOGGING_INFO, "Unreserved pages of type 7 [ EfiConventionalMemory ]");

    reserve_pages(0, 0x100);

    lock_pages(page_bitmap.buffer, page_bitmap.size / 0x1000 + 1);

    initialized = true;
}