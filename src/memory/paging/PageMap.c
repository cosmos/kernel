//
// Created by antifallobst on 28/07/2022.
//

#include <memory/paging/PageMap.h>
#include <memory/paging/PageMapIndexer.h>
#include <memory/paging/PageFrame.h>
#include <memory/Memory.h>

#include <utils/Logger.h>
#include <utils/Math.h>

void map_memory(paging_page_table_T* page_map, uint64_t flags, void* virtual_memory, void* physical_memory) {

    virtual_memory = (void*)floor_to((uint64_t)virtual_memory, PAGE_SIZE);

    PageMapIndexer_T    indexer                 = new_page_map_indexer((uint64_t)virtual_memory);
    uint64_t            page_directory_entry    = page_map->entries[indexer.page_directory_page_index];

    if (page_map != g_page_map) {
        log(LOGGING_INFO, "Mapping [%d/%d/%d/%d]",
            indexer.page_directory_page_index, indexer.page_directory_index, indexer.page_table_index,
            indexer.page_index);
    }

    paging_page_table_T*        page_directory_page;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        page_directory_page = (paging_page_table_T*)request_page();
        memset(page_directory_page, 0, 0x1000);

        SetAddress(&page_directory_entry, (uint64_t)page_directory_page >> 12);
        page_directory_entry |= PTF_PRESENT | PTF_READ_WRITE;

        page_map->entries[indexer.page_directory_page_index] = page_directory_entry;
    } else {
        page_directory_page = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_directory_entry = page_directory_page->entries[indexer.page_directory_index];
    paging_page_table_T* page_directory;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        page_directory = (paging_page_table_T*)request_page();
        memset(page_directory, 0, 0x1000);

        SetAddress(&page_directory_entry, (uint64_t)page_directory >> 12);
        page_directory_entry |= PTF_PRESENT | PTF_READ_WRITE;

        page_directory_page->entries[indexer.page_directory_index] = page_directory_entry;
    } else {
        page_directory = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_directory_entry = page_directory->entries[indexer.page_table_index];
    paging_page_table_T* page_table;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        page_table = (paging_page_table_T*)request_page();
        memset(page_table, 0, 0x1000);

        SetAddress(&page_directory_entry, (uint64_t)page_table >> 12);
        page_directory_entry |= PTF_PRESENT | PTF_READ_WRITE;

        page_directory->entries[indexer.page_table_index] = page_directory_entry;
    } else {
        page_table = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_directory_entry = page_table->entries[indexer.page_index];

    SetAddress(&page_directory_entry, (uint64_t)physical_memory >> 12);
    page_directory_entry |= PTF_PRESENT | flags;
    page_table->entries[indexer.page_index] = page_directory_entry;
}

void unmap_memory(paging_page_table_T* page_map, void* virtual_memory) {
    PageMapIndexer_T    indexer                 = new_page_map_indexer((uint64_t)virtual_memory);
    uint64_t            page_directory_entry    = page_map->entries[indexer.page_directory_page_index];

    paging_page_table_T*        page_directory_page;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        return;
    } else {
        page_directory_page = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_directory_entry = page_directory_page->entries[indexer.page_directory_index];
    paging_page_table_T* page_directory;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        return;
    } else {
        page_directory = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_directory_entry = page_directory->entries[indexer.page_table_index];
    paging_page_table_T* page_table;
    if (!GetFlag(&page_directory_entry, PTF_PRESENT)){
        return;
    } else {
        page_table = (paging_page_table_T*)((uint64_t)GetAddress(&page_directory_entry) << 12);
    }

    page_table->entries[indexer.page_index] = 0;
}

void load_page_map(paging_page_table_T* page_map) {
//    uint64_t physical_address = get_physical_address(page_map);
    uint64_t physical_address = (uint64_t)page_map;

    asm volatile ("mov %0, %%cr3" : : "r" (physical_address));
}

paging_page_table_T* new_page_map() {
    paging_page_table_T* map = request_page();
    return map;
}

//void clone_page_map(paging_page_table_T* dest, paging_page_table_T* src) {
//    for (int PML4_index = 0; PML4_index < 512; PML4_index++) {
//        if (!GetFlag(&src->entries[PML4_index], PTF_PRESENT)) {
//            continue;
//        }
//        log(LOGGING_INFO, "PML4E  [%d]", PML4_index);
//
//        paging_page_table_T* src_PML4_entry  = (paging_page_table_T*)src->entries [PML4_index];
//        paging_page_table_T* dest_PML4_entry = (paging_page_table_T*)dest->entries[PML4_index];
//
//        dest->entries[PML4_index] = request_page();
//        memset(&dest->entries[PML4_index], 0, 0x1000);
//
////        SetAddress(dest_PDE, *src_PDE >> 12);
////        *dest_PDE |= PTF_PRESENT | PTF_READ_WRITE;
//
//        for (int PML4E_index = 0; PML4E_index < 512; PML4E_index++) {
//            if (!GetFlag(&src_PML4_entry->entries[PML4E_index], PTF_PRESENT)) {
//                continue;
//            }
//            log(LOGGING_INFO, "    PDPT  [%d]", PML4E_index);
//            paging_page_table_T** src_PDPT  = &src_PML4_entry->entries [PML4E_index];
//            paging_page_table_T** dest_PDPT = &dest_PML4_entry->entries[PML4E_index];
//
//            *dest_PDPT = request_page();
//            memset(*dest_PDPT, 0, 0x1000);
//
////            SetAddress(dest_PDP, *src_PDP >> 12);
////            *dest_PDP |= PTF_PRESENT | PTF_READ_WRITE;
//
//            for (int PDPT_index = 0; PDPT_index < 512; PDPT_index++) {
//                if (!GetFlag(&((uint64_t*)*src_PDPT)[PDPT_index], PTF_PRESENT)) {
//                    continue;
//                }
//                log(LOGGING_INFO, "        PDPTE  [%d]", PDPT_index);
//
//                paging_page_table_T** src_PDPT_entry  = &(*src_PDPT)->entries [PDPT_index];
//                paging_page_table_T** dest_PDPT_entry = &(*dest_PDPT)->entries[PDPT_index];
//
//                *dest_PDPT_entry = request_page();
//                memset(*dest_PDPT_entry, 0, 0x1000);
//
////                SetAddress(dest_PD, *src_PD >> 12);
////                *dest_PD |= PTF_PRESENT | PTF_READ_WRITE;
//
//                for (int PD = 0; PD < 512; PD++) {
//                    if (!GetFlag(&(*src_PDPT_entry)->entries[PD], PTF_PRESENT)) {
//                        continue;
//                    }
//
//                    log(LOGGING_INFO, "            PD  [%d]", PD);
//
//                    paging_page_table_T** src_PD  = &(*src_PDPT_entry)->entries [PD];
//                    paging_page_table_T** dest_PD = &(*dest_PDPT_entry)->entries[PD];
//
//                    *dest_PD = request_page();
//                    memset(*dest_PD, 0, 0x1000);
//
////                    SetAddress(dest_PT, *src_PT >> 12);
////                    *dest_PT |= PTF_PRESENT | PTF_READ_WRITE;
//
//                    for (int PDE = 0; PDE < 512; PDE++) {
//                        if (!GetFlag(&(*src_PD)->entries[PDE], PTF_PRESENT)) {
//                            continue;
//                        }
//
//                        paging_page_table_T** src_PDE  = &(*src_PD)->entries [PDE];
//                        paging_page_table_T** dest_PDE = &(*dest_PD)->entries[PDE];
//
//                        *dest_PDE = *src_PDE;
//                        log(LOGGING_INFO, "                PDE  [%d]", PDE);
//                    }
//                }
//            }
//        }
//    }
//}