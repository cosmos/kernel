//
// Created by antifallobst on 28/07/2022.
//

#include <memory/paging/Paging.h>
#include <interrupts/Interrupts.h>

bool GetFlag (uint64_t* value, page_table_flags_E flag) {
    return ((*value & flag) > 0);  // return (*value & bit_selector) > 0 ? true : false;
}

void SetAddress (uint64_t* value, uint64_t address) {
    address &= 0x000000ffffffffff;
    *value &= 0xfff0000000000fff;
    *value |= (address << 12);
}

uint64_t GetAddress (uint64_t* value) {
    return (*value & 0x000ffffffffff000) >> 12;
}