//
// Created by antifallobst on 28/07/2022.
//

#include <memory/Memory.h>
#include <memory/paging/Paging.h>
#include <utils/Logger.h>
#include <utils/Math.h>
#include <utils/StrUtil.h>

uint64_t get_memory_size(EFI_MEMORY_DESCRIPTOR_T* memory_map, uint64_t memory_map_entries, uint64_t memory_map_descriptor_size) {
    static uint64_t memory_size_bytes = 0;
    if (memory_size_bytes > 0) return memory_size_bytes;

    for (uint64_t i = 0; i < memory_map_entries; i++) {
        EFI_MEMORY_DESCRIPTOR_T* descriptor      = (EFI_MEMORY_DESCRIPTOR_T*)((uint64_t)memory_map + (i * memory_map_descriptor_size));
        memory_size_bytes                       += descriptor->num_pages * PAGE_SIZE;
    }
    log(LOGGING_INFO, "Calculated Memory Size [B]: %d", memory_size_bytes);
    return memory_size_bytes;
}

void memcpy(void *dest, void* src, uint64_t len) {
    uint8_t*    dest_8      = (uint8_t *) dest;
    uint8_t*    src_8       = (uint8_t *) src;

    for (uint64_t i = 0; i < len; i++) {
        dest_8[i] = src_8[i];
    }
}

// Reversed memcpy - copies memory from src+len downwards
void rmemcpy(void* dest, void* src, uint64_t len) {
    uint8_t*    dest_8      = (uint8_t *) dest;
    uint8_t*    src_8       = (uint8_t *) src;

    for (uint64_t i = 0; i < len; i++) {
        dest_8[len - i] = src_8[len - i];
    }
}

void memset(void *area, uint8_t value, size_t len) {
    uint8_t* area_8 = (uint8_t *) area;

    for (uint64_t i = 0; i < len; i++) {
        area_8[i] = value;
    }
}

bool memcmp(void* a, void* b, uint64_t num) {
    uint8_t* a_ptr = (uint8_t*)a;
    uint8_t* b_ptr = (uint8_t*)b;

    for (uint64_t i = 0; i < num; i++) {
        if (*(uint8_t*)a_ptr != *(uint8_t*)b_ptr) return false;
        a_ptr++;
        b_ptr++;
    }
    return true;
}

void hexdump(void* buffer, uint64_t num) {
    log(LOGGING_NONE, "\n<===================================================[ HEXDUMP START ]==================================================>");
    log(LOGGING_NONE, "[ 0x%x  -->  0x%x ]----< %d Bytes > ", buffer, buffer + num, num);

    uint8_t*    ptr     = buffer;
    uint16_t    lines   = floor_to(num, 16) / 16 + 1;
    bool        skipped = false;

    for (int y = 0; y < lines; y++) {
        char        hex_block   [50];
        char        chr_block   [34];
        uint64_t    hex_pos    = 0;
        uint64_t    chr_pos    = 0;
        uint64_t    length     = 16;
        bool        not_zero   = false;

        if (((y + 1) * 16) > num) {
            length = num - (y * 16);
        }

        for (int x = 0; x < length; x++) {
            if (ptr[x] > 0) {
                not_zero = true;
            }
        }

        if (not_zero) {
            if (skipped) {
                log(LOGGING_NONE, "      . . .");
            }
            skipped  = false;
        } else {
            skipped  = true;
            ptr     += 16;
            continue;
        }

        for (int x = 0; x < length; x++) {
            if (hex_pos == 24) {
                hex_block[hex_pos]      = ' ';
                hex_block[hex_pos + 1]  = ' ';
                hex_pos                += 2;

                chr_block[chr_pos]      = ' ';
                chr_block[chr_pos + 1]  = ' ';
                chr_pos                += 2;
            }
            hex_to_string_x8(ptr[x], &hex_block[hex_pos]);
            hex_block[hex_pos + 2]   = ' ';
            hex_pos                 += 3;

            chr_block[chr_pos]      = '.';
            chr_block[chr_pos + 1]  = ' ';
            if ((ptr[x] >= 32 && ptr[x] <= 126)) {
                chr_block[chr_pos] = (char)ptr[x];
            }
            chr_pos += 2;
        }
        memset(&hex_block[hex_pos], ' ', 50 - hex_pos);
        memset(&chr_block[chr_pos], ' ', 34 - chr_pos);
        hex_block[49] = '\0';
        chr_block[33] = '\0';
        ptr          += 16;

        log(LOGGING_NONE, "    0x%x  |  0x%x16  |  %s  |  [%s]", ptr - 16, y, hex_block, chr_block);
    }

    log(LOGGING_NONE, "<=====================================================[ HEXDUMP END ]==================================================>\n");
}