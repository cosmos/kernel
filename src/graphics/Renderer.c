//
// Created by antifallobst on 28/07/2022.
//

#include <stddef.h>
#include <stdint.h>
#include <graphics/Renderer.h>
#include <graphics/Font.h>
#include <utils/StrUtil.h>
#include <memory/Memory.h>

bool        renderer_free       = true;
point_T     cursor_position;
color_t     fg_color            = COLOR_STD_WHITE;
color_t     bg_color            = COLOR_STD_BLACK;

void put_pixel(point_T coords, color_t clr) {
    *(uint32_t*)((uint64_t)g_framebuffer->base_address + (coords.x * 4) + (coords.y * g_framebuffer->pixels_per_scan_line * 4)) = clr;
}

uint32_t get_pixel(point_T coords) {
    return *(uint32_t*)((uint64_t)g_framebuffer->base_address + (coords.x * 4) + (coords.y * g_framebuffer->pixels_per_scan_line * 4));
}


void put_char(char chr, color_t clr) {
    switch (chr) {
        case '\r': {
            cursor_position.x = 0;
            break;
        }
        case '\n': {
            if (cursor_position.y + CHAR_HEIGHT >= g_framebuffer->height - CHAR_HEIGHT * 2) {
                scroll();
            }
            cursor_position.y += CHAR_HEIGHT;
            put_char('\r', clr);
            break;
        }
        default: {
            char *font_ptr = (char *) g_font->glyph_buffer + (chr * g_font->psf1_header->char_size);

            for (uint64_t y = cursor_position.y; y < cursor_position.y + CHAR_HEIGHT; y++) {
                for (uint64_t x = cursor_position.x; x < cursor_position.x + CHAR_WIDTH; x++) {
                    if ((*font_ptr & (0b10000000 >> (x - cursor_position.x))) > 0) {
                        put_pixel((point_T) {x, y}, clr);
                    } else {
                        put_pixel((point_T) {x, y}, bg_color);
                    }
                }
                font_ptr++;
            }

            cursor_position.x += CHAR_WIDTH;
            if (cursor_position.x >= g_framebuffer->width) {
                put_char('\n', clr);
            }
            break;
        }
    }
}

void printf(const char* str, ...) {
    while (!renderer_free) { asm ("hlt"); }
    renderer_free = false;

    if (str == NULL) {
        renderer_free = true;
        printf("< NULL Reference Warning >");
        return;
    }

    va_list args;
    va_start(args, str);
    size_t size = vformat(NULL, str, args);

    va_start(args, str);
    char buf[size];
    vformat(buf, str, args);

    char* pattern = buf;

    while (*pattern != 0) {
        if (*pattern == '%') {
            pattern++;
            switch (*pattern) {
                case 'C':
                    fg_color = va_arg(args, uint32_t);
                    break;
                default:
                    pattern--;
                    put_char(*pattern, fg_color);
                    break;
            }
        } else {
            put_char(*pattern, fg_color);
        }
        pattern++;
    }
    va_end(args);

    renderer_free = true;
}

void scroll() {
    size_t line = g_framebuffer->pixels_per_scan_line * CHAR_HEIGHT * 4;
    memcpy(g_framebuffer->base_address, (void*)((uint64_t)g_framebuffer->base_address + line), g_framebuffer->buffer_size - line);

    cursor_position.y -= CHAR_HEIGHT;
}