OSNAME = cosmos

# Directories
GNUEFI = ../bootloader/gnu-efi
BINARIES = ../binaries
TOOLS = ../tools
INCLUDEDIR = inc
SRCDIR := src
OBJDIR := ../build/kernel/lib

# Files
BOOTEFI := $(GNUEFI)/x86_64/bootloader/main.efi
LDS = kernel.ld

# Toolchain
CC = gcc
ASM = nasm
LD = ld

# Flags
CFLAGS = -fno-stack-protector -fno-exceptions -mno-red-zone -ffreestanding -fshort-wchar -I$(INCLUDEDIR) -Wall -Wextra -march=$(ARCHITECTURE)
EMUFLAGS = -no-reboot -machine q35 -vga cirrus -drive file=../build/$(OSNAME).img -m 256M -cpu qemu64 -drive if=pflash,format=raw,unit=0,file="$(BINARIES)/OVMF/OVMF_CODE-pure-efi.fd",readonly=on -drive if=pflash,format=raw,unit=1,file="$(BINARIES)/OVMF/OVMF_VARS-pure-efi.fd",readonly=on -net none -chardev stdio,id=log,logfile=serial.log -serial chardev:log
ASMFLAGS =
LDFLAGS = -T $(LDS) -static -Bsymbolic -nostdlib

ARCHITECTURE = x86-64

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

SRC = $(call rwildcard,$(SRCDIR),*.c)
ASMSRC = $(call rwildcard,$(SRCDIR),*.asm)
OBJS = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRC))
OBJS += $(patsubst $(SRCDIR)/%.asm, $(OBJDIR)/%.o, $(ASMSRC))
DIRS = $(wildcard $(SRCDIR)/*)

kernel: setup $(OBJS) link

$(OBJDIR)/interrupts/Interrupts.o: $(SRCDIR)/interrupts/Interrupts.c
	@ echo !==== COMPILING $^
	@ mkdir -p $(@D)
	$(CC) -mgeneral-regs-only $(CFLAGS) -c $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@ echo !==== COMPILING $^
	@ mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.asm
	@ echo !==== ASSEMBLING $^
	@ mkdir -p $(@D)
	$(ASM) $(ASMFLAGS) $^ -f elf64 -o $@

link:
	@ echo !===== LINKING
	$(LD) $(LDFLAGS) -o ../build/kernel/kernel.elf $(OBJS)

setup:
	mkdir -p ../build/
	mkdir -p ../build/kernel
	mkdir -p $(OBJDIR)

clean:
	rm -rf ../build/kernel

buildimg:
	python3 $(TOOLS)/gensym.py ../build/kernel/kernel.elf
	mv parsed.sym $(BINARIES)/ramdisk/kernel_symbols.map
	make ramdisk

	@ echo !===== BUILDING IMAGE
	dd if=/dev/zero of=../build/$(OSNAME).img bs=512 count=93750
	mformat -i ../build/$(OSNAME).img -f 1440 ::
	mmd -i ../build/$(OSNAME).img ::/EFI
	mmd -i ../build/$(OSNAME).img ::/EFI/BOOT
	mcopy -i ../build/$(OSNAME).img $(BOOTEFI) ::/EFI/BOOT
	mcopy -i ../build/$(OSNAME).img startup.nsh ::
	mcopy -i ../build/$(OSNAME).img ../build/kernel/kernel.elf ::
	mcopy -i ../build/$(OSNAME).img $(BINARIES)/zap-light16.psf ::
	mcopy -i ../build/$(OSNAME).img ../build/init.rd ::

run:
	qemu-system-x86_64 $(EMUFLAGS)

ramdisk: setup
	tar -C $(BINARIES)/ramdisk/ -cvf ../build/init.rd .